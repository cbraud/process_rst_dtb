#!/bin/bash

# TODO change the extension of the conllu/ files (ie train/test/dev files in the conllu format, use.conllu rather than .parse)

# Exit immediately if a command exits with a non-zero status.
set -e

# Directory for the code
src=code/rst_DTB/
# Log file
log=log_preprocess_corpora_051119.txt


# ----------------------------------------------------------------------------------
### BASQUE
data=data
ud_model=basque-ud-1.2-160523.udpipe

original=${data}/original

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/

echo Processing Basque corpus - ${original}

echo ${dmrg}
mkdir -p ${dmrg}
python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping >${log}

## dmrg=/home/cbraud/sandbox/basque-pardi/data/original-basque-rstdt/

echo ${const}
mkdir -p ${const}
python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/ >>${log}

echo ${dep}
mkdir -p ${dep}
python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/ >>${log}

echo ${raw}
mkdir -p ${raw}
python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/ >>${log}

echo ${parse}
mkdir -p ${parse}
bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${ud_model} >>${log}

echo ${conll}
mkdir -p ${conll}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/ #>>${log}

bash generate_discourse_data.sh ${data}/ ${conll}
