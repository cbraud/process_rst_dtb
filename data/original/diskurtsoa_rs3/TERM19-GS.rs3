﻿<rst>
  <header>
    <relations>
      <rel name="elaborazioa" type="rst" />
      <rel name="prestatzea" type="rst" />
      <rel name="kausa" type="rst" />
      <rel name="ondorioa" type="rst" />
      <rel name="testuingurua" type="rst" />
      <rel name="zirkunstantzia" type="rst" />
      <rel name="kontzesioa" type="rst" />
      <rel name="baldintza" type="rst" />
      <rel name="helburua" type="rst" />
      <rel name="metodoa" type="rst" />
      <rel name="ebaluazioa" type="rst" />
      <rel name="ebidentzia" type="rst" />
      <rel name="interpretazioa" type="rst" />
      <rel name="justifikazioa" type="rst" />
      <rel name="motibazioa" type="rst" />
      <rel name="birformulazioa" type="rst" />
      <rel name="antitesia" type="rst" />
      <rel name="ahalbideratzea" type="rst" />
      <rel name="aukera" type="rst" />
      <rel name="alderantzizko-baldintza" type="rst" />
      <rel name="ez-baldintzatzailea" type="rst" />
      <rel name="arazo-soluzioa" type="rst" />
      <rel name="laburpena" type="rst" />
      <rel name="definitugabeko-erlazioa" type="rst" />
      <rel name="kontrastea" type="multinuc" />
      <rel name="lista" type="multinuc" />
      <rel name="sekuentzia" type="multinuc" />
      <rel name="konjuntzioa" type="multinuc" />
      <rel name="disjuntzioa" type="multinuc" />
      <rel name="bateratzea" type="multinuc" />
      <rel name="same-unit" type="multinuc" />
    </relations>
  </header>
  <body>
    <segment id="1" parent="55" relname="prestatzea">Terminologia normalizatzeak gizartean duen eragina</segment>
    <segment id="2" parent="33" relname="span"> Egungo gizartean bi joera nabarmentzen dira, eta kontrajarriak, itxura batean.</segment>
    <segment id="3" parent="31" relname="span"> Alde batetik, gero eta indartsuagoa da nazioarteko harmonizazioa lortu beharra,</segment>
    <segment id="4" parent="3" relname="kausa"> ekonomian, politikan eta kultura eta gizarte gaietan etenik gabe sortzen ari diren loturak eta elkarren arteko trukaketak eraginda;</segment>
    <segment id="5" parent="30" relname="span"> eta bestetik, aniztasuna onartu beharra, eta, ondorioz, norbanakoen izaeraren berrespena,</segment>
    <segment id="6" parent="5" relname="zirkunstantzia"> giza bizitzako arlo guztietan agertzen ari den moduan.</segment>
    <segment id="7" parent="34" relname="same-unit"> Terminologiak berak ere,</segment>
    <segment id="8" parent="34" relname="kausa"> gizartearekin lotuta dagoen jarduera denez,</segment>
    <segment id="9" parent="34" relname="same-unit"> uztartu egin behar ditu joera orokor horiek, eransten zaizkien beste batzuekin batera, hala nola: teknologien aurrerakuntza zorabiagarria, zientziak diziplinartekotasunera eta hiperespezializaziora daramatzan bilakaera, eta informazioa elkarren artean berehala trukatu beharra.</segment>
    <segment id="10" parent="37" relname="same-unit"> Alderdi horiek guztiek,</segment>
    <segment id="11" parent="38" relname="lista"> espezialitateko terminologiaren gehikuntza kuantitatiboa eragiteaz gain,</segment>
    <segment id="12" parent="37" relname="same-unit"> terminologia lanen ikuspegia ere zabaldu egin dute;</segment>
    <segment id="13" parent="39" relname="same-unit"> eta,</segment>
    <segment id="14" parent="39" relname="kontzesioa"> egia bada ere ikuspegi berri horrek terminologia aberastu egin duela esatea,</segment>
    <segment id="15" parent="39" relname="same-unit"> zalantzan jarri ditu terminologiaren oinarrizko zenbait kontzeptu: kontzeptu-izendapen bikotearen adierabakartasuna, espezialitateko eremuen kontzeptua, eta normalizazioak terminologian duen eginbeharra.</segment>
    <segment id="16" parent="43" relname="span"> Nahiz eta gaur egun normalizazioko oinarrizko printzipioek balio osoa gorde komunikazio espezialduaren bermearen bidez </segment>
    <segment id="17" parent="16" relname="elaborazioa">(eta elkarrekin zerikusia duten gizarteko sektoreen arteko adostasuna da printzipio horietako bat)</segment>
    <segment id="18" parent="44" relname="span">, terminologiako lan praktikoan, batzuetan, ahaztuxe uzten da normalizazioaren eta gizartearen artean egon behar den lotura estua.</segment>
    <segment id="19" parent="45" relname="same-unit"> Horrek dakarren arriskua zera da: adostasuna lortzeko ezinbestekoa den arbitrarietatea, maila baterainokoa behintzat,</segment>
    <segment id="20" parent="45" relname="kontzesioa"> eta adituek terminologiari ematen dioten erabilera erreala orekatu behar izan arren,</segment>
    <segment id="21" parent="45" relname="same-unit"> eten egin daitekeela.</segment>
    <segment id="22" parent="48" relname="lista"> Kasu horretan, normalizazioa ez bakarrik ez litzateke eraginkorra izango,</segment>
    <segment id="23" parent="48" relname="lista"> are gehiago, bete gabe utziko lituzke bere helburuak.</segment>
    <segment id="24" parent="51" relname="same-unit"> Komunikazio honetan,</segment>
    <segment id="25" parent="51" relname="zirkunstantzia"> katalanerako terminologia normalizatzeko lanetan izandako eskarmentutik hasita,</segment>
    <segment id="26" parent="51" relname="same-unit"> batetik, gizarteak terminologia normaltzeko duen beharra aurkeztuko dugu,</segment>
    <segment id="27" parent="53" relname="sekuentzia"> hurrengo, horretarako dauden zenbait zailtasun aipatuko ditugu,</segment>
    <segment id="28" parent="53" relname="sekuentzia"> eta, amaitu orduko, ideia batzuk plazaratuko ditugu</segment>
    <segment id="29" parent="53" relname="helburua"> hori guztia egungo gizartean bideratu ahal izateko.</segment>
<group id="30" type="span" parent="32" relname="lista" />
<group id="31" type="span" parent="32" relname="lista" />
<group id="32" type="multinuc" parent="2" relname="elaborazioa" />
<group id="33" type="span" parent="35" relname="testuingurua" />
<group id="34" type="multinuc" parent="35" relname="span" />
<group id="35" type="span" parent="36" relname="span" />
<group id="36" type="span" parent="42" relname="span" />
<group id="37" type="multinuc" parent="38" relname="lista" />
<group id="38" type="multinuc" parent="41" relname="kontrastea" />
<group id="39" type="multinuc" parent="40" relname="span" />
<group id="40" type="span" parent="41" relname="kontrastea" />
<group id="41" type="multinuc" parent="36" relname="ondorioa" />
<group id="42" type="span" parent="50" relname="span" />
<group id="43" type="span" parent="18" relname="kontzesioa" />
<group id="44" type="span" parent="49" relname="span" />
<group id="45" type="multinuc" parent="46" relname="span" />
<group id="46" type="span" parent="47" relname="span" />
<group id="48" type="multinuc" parent="46" relname="interpretazioa" />
<group id="47" type="span" parent="44" relname="ondorioa" />
<group id="49" type="span" parent="42" relname="elaborazioa" />
<group id="50" type="span" parent="54" relname="testuingurua" />
<group id="51" type="multinuc" parent="52" relname="span" />
<group id="52" type="span" parent="53" relname="sekuentzia" />
<group id="53" type="multinuc" parent="54" relname="span" />
<group id="54" type="span" parent="55" relname="span" />
<group id="55" type="span" />
  </body>
</rst>
