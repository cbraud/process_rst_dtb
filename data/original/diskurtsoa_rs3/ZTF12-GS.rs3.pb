﻿<rst>
  <header>
    <relations>
      <rel name="elaborazioa" type="rst" />
      <rel name="prestatzea" type="rst" />
      <rel name="kausa" type="rst" />
      <rel name="ondorioa" type="rst" />
      <rel name="testuingurua" type="rst" />
      <rel name="zirkunstantzia" type="rst" />
      <rel name="kontzesioa" type="rst" />
      <rel name="baldintza" type="rst" />
      <rel name="helburua" type="rst" />
      <rel name="metodoa" type="rst" />
      <rel name="ebaluazioa" type="rst" />
      <rel name="ebidentzia" type="rst" />
      <rel name="interpretazioa" type="rst" />
      <rel name="justifikazioa" type="rst" />
      <rel name="motibazioa" type="rst" />
      <rel name="birformulazioa" type="rst" />
      <rel name="antitesia" type="rst" />
      <rel name="ahalbideratzea" type="rst" />
      <rel name="aukera" type="rst" />
      <rel name="alderantzizko-baldintza" type="rst" />
      <rel name="ez-baldintzatzailea" type="rst" />
      <rel name="arazo-soluzioa" type="rst" />
      <rel name="laburpena" type="rst" />
      <rel name="definitugabeko-erlazioa" type="rst" />
      <rel name="kontrastea" type="multinuc" />
      <rel name="lista" type="multinuc" />
      <rel name="sekuentzia" type="multinuc" />
      <rel name="konjuntzioa" type="multinuc" />
      <rel name="disjuntzioa" type="multinuc" />
      <rel name="bateratzea" type="multinuc" />
      <rel name="same-unit" type="multinuc" />
    </relations>
  </header>
  <body>
    <segment id="1" parent="94" relname="prestatzea">Micro eta nanoposizionamentu kontrola adimen materialekin.</segment>
    <segment id="2" parent="51" relname="span"> Material adimenduak (SMART MATER IALS ) kanpo–kinada fisiko edo kimiko batzuen aurrean era itzulgarrian eta kontrolatuan erantzuteko gai diren materialak dira,</segment>
    <segment id="3" parent="2" relname="elaborazioa"> bere propietateren bat aldatuz, luzeera, adibidez.</segment>
    <segment id="4" parent="49" relname="span"> Forma–memoria duten aleazioak (SMA) eta forma–memoria ferromagnetiko duten aleazioak (FSMA), mota hauetako material batzuk dira, jatorrizko forma gogoratu eta berreskuratzeko gai direnak,</segment>
    <segment id="5" parent="4" relname="kontzesioa"> nahiz eta deformazio–prozesu bat jasan izan.</segment>
    <segment id="6" parent="49" relname="kausa"> Deformazio–prozesu hau, eremu magnetikoa edota tenperatura aldaketaren ondorioz gerta daiteke.</segment>
    <segment id="7" parent="54" relname="kausa"> Material hauen sentikortasuna dela eta,</segment>
    <segment id="8" parent="53" relname="same-unit"> sentsoreak eta eragingailuak</segment>
    <segment id="9" parent="53" relname="helburua"> diseinatzeko</segment>
    <segment id="10" parent="53" relname="same-unit"> erabil daitezke,</segment>
    <segment id="11" parent="59" relname="span"> baita adimendun sistemak eta estrukturak konfiguratzeko ere,</segment>
    <segment id="12" parent="11" relname="zirkunstantzia"> egokiak edo hoberenak diren baldintzetara moldatuz.</segment>
    <segment id="13" parent="57" relname="kausa"> Propietate hau dela eta,</segment>
    <segment id="14" parent="56" relname="span"> eragingailua SMA elementu bakar batez osa daiteke,</segment>
    <segment id="15" parent="14" relname="ebaluazioa"> eta horrek, abantaila garrantzitsua suposatzen du</segment>
    <segment id="16" parent="56" relname="zirkunstantzia"> mekatronika teknologian eta robotikan dagoen miniaturizazio joera kontutan izanik.</segment>
    <segment id="17" parent="62" relname="same-unit"> Hala ere,</segment>
    <segment id="18" parent="62" relname="kausa"> material hauen fase–transizioa histeretikoa denez,</segment>
    <segment id="19" parent="62" relname="same-unit"> zehaztasun txikiko mugimenduak kontrolatu ahal izateko kontrol seinalearen esfortzu handiak beharrezkoak izaten dira. </segment>
    <segment id="20" parent="77" relname="span"> Ikerketa lan–ildo honetan, material hauen uzkurdura eta erlaxazioa zehaztasunez kontrolatzeko bideak proposatu ditugu.</segment>
    <segment id="21" parent="75" relname="span"> Kontrol esperimentu ugari egin dira garatutako zenbait gailu esperimentaletan,</segment>
    <segment id="22" parent="21" relname="metodoa"> mugimendu zorrotza sor dezakeen SMA materialak eragingailu bezala erabiltzeko benetako aukerak ikertuz.</segment>
    <segment id="23" parent="72" relname="span"> Lortutako emaitza esperimentalek SMA materialaren kasuan, mikra bateko zehaztazunezko higidura lor daitekeela erakusten dute,</segment>
    <segment id="24" parent="23" relname="baldintza"> beti ere posizio sentsore egoki bat erabiliz gero, LVDT a esaterako.</segment>
    <segment id="25" parent="73" relname="kontrastea"> FSMA materialarekin ordea, zehaztasun nanometrikoa lortu da.</segment>
    <segment id="26" parent="71" relname="span"> Hala ere, lehenago esan den bezala, material hauek duten histeresiak kontrola zailtzen du.</segment>
    <segment id="27" parent="28" relname="baldintza"> Histeresiaren efektuak kontrol eskeman sartuz,</segment>
    <segment id="28" parent="68" relname="span"> sistema kontrolatzeko egin beharreko esfortzuak nabarmenki gutxitzen dira.</segment>
    <segment id="29" parent="66" relname="span"> Sare Neuronal bat trebatu da</segment>
    <segment id="30" parent="29" relname="helburua"> alderantzizko histeresia inplementatzeko asmoz,</segment>
    <segment id="31" parent="66" relname="helburua"> sistema osoa era linealean jokatu dezan.</segment>
    <segment id="32" parent="69" relname="ondorioa"> Esfortzuen hobekuntzak posizionatze–gailuen fidagarritasuna eta iraunkortasuna handiagotzea inplikatzen du.</segment>
    <segment id="33" parent="91" relname="prestatzea"> Lan–ildo berriak bi dira bereziki.</segment>
    <segment id="34" parent="88" relname="same-unit"> Alde batetik,</segment>
    <segment id="35" parent="87" relname="sekuentzia"> SMA materialak mugimendu zorrotza sor dezakeen eragingailu bezala erabiltzeko benetako aukerak ikertu ondoren,</segment>
    <segment id="36" parent="87" relname="sekuentzia"> eta eragingailu bat diseinatu ondoren,</segment>
    <segment id="37" parent="88" relname="same-unit"> materiala bera sentsore bezala erabiltzea aukera bat da,</segment>
    <segment id="38" parent="39" relname="metodoa"> hau da, hariaren erresistenzia neurtuz </segment>
    <segment id="39" parent="85" relname="span">uzkurdura ondorioztatu,</segment>
    <segment id="40" parent="85" relname="helburua"> beste sentsore baten beharrik ez izateko.</segment>
    <segment id="41" parent="81" relname="same-unit"> Eta bestetik,</segment>
    <segment id="42" parent="78" relname="same-unit"> eragingailu hauek</segment>
    <segment id="43" parent="78" relname="helburua"> elementu hauskorrak hartzeko</segment>
    <segment id="44" parent="78" relname="same-unit"> erabil daitezkela kontutan izanik,</segment>
    <segment id="45" parent="80" relname="konjuntzioa"> eta mikromekatronika arloaren aplikazio batzuentzako posizioa soilik kontrolatzea nahikoa ez denez,</segment>
    <segment id="46" parent="81" relname="zirkunstantzia"> elementua hartzean</segment>
    <segment id="47" parent="81" relname="same-unit"> eragingailuak egindako indarra kontrolatzea ere derrigorrezkoa izaten da.</segment>
    <segment id="48" parent="83" relname="justifikazioa"> Horrexegatik, lan–ildo berri bat eragingailuetan indar–kontrola egitean datza.</segment>
<group id="49" type="span" parent="50" relname="span" />
<group id="50" type="span" parent="51" relname="elaborazioa" />
<group id="51" type="span" parent="52" relname="span" />
<group id="52" type="span" parent="65" relname="span" />
<group id="53" type="multinuc" parent="54" relname="span" />
<group id="54" type="span" parent="55" relname="span" />
<group id="55" type="span" parent="60" relname="konjuntzioa" />
<group id="56" type="span" parent="57" relname="span" />
<group id="57" type="span" parent="58" relname="span" />
<group id="58" type="span" parent="60" relname="elaborazioa" />
<group id="59" type="span" parent="60" relname="konjuntzioa" />
<group id="60" type="multinuc" parent="61" relname="span" />
<group id="61" type="span" parent="64" relname="span" />
<group id="62" type="multinuc" parent="63" relname="span" />
<group id="63" type="span" parent="61" relname="kontzesioa" />
<group id="64" type="span" parent="52" relname="elaborazioa" />
<group id="65" type="span" parent="93" relname="testuingurua" />
<group id="66" type="span" parent="67" relname="span" />
<group id="67" type="span" parent="68" relname="metodoa" />
<group id="68" type="span" parent="69" relname="span" />
<group id="69" type="span" parent="70" relname="span" />
<group id="70" type="span" parent="26" relname="elaborazioa" />
<group id="71" type="span" parent="73" relname="kontzesioa" />
<group id="72" type="span" parent="73" relname="kontrastea" />
<group id="73" type="multinuc" parent="74" relname="span" />
<group id="74" type="span" parent="75" relname="ondorioa" />
<group id="75" type="span" parent="76" relname="span" />
<group id="76" type="span" parent="20" relname="elaborazioa" />
<group id="77" type="span" parent="93" relname="span" />
<group id="78" type="multinuc" parent="79" relname="span" />
<group id="79" type="span" parent="80" relname="konjuntzioa" />
<group id="80" type="multinuc" parent="82" relname="zirkunstantzia" />
<group id="81" type="multinuc" parent="82" relname="span" />
<group id="82" type="span" parent="83" relname="span" />
<group id="83" type="span" parent="84" relname="span" />
<group id="84" type="span" parent="91" relname="sekuentzia" />
<group id="85" type="span" parent="86" relname="span" />
<group id="86" type="span" parent="89" relname="birformulazioa" />
<group id="87" type="multinuc" parent="88" relname="metodoa" />
<group id="88" type="multinuc" parent="89" relname="span" />
<group id="89" type="span" parent="90" relname="span" />
<group id="90" type="span" parent="91" relname="sekuentzia" />
<group id="91" type="multinuc" parent="92" relname="span" />
<group id="92" type="span" parent="77" relname="elaborazioa" />
<group id="93" type="span" parent="94" relname="span" />
<group id="94" type="span" />
  </body>
</rst>
