
data=$1
parse=$2
folder=$1

src=code/process_tbk

echo $folder $data
mkdir -p $folder


## process_pdtb.py uses dep corpus to assign heads to const corpus, then transforms const corpus in new format
## arguments:
## 1. tbk (generate tbk file) or raw (keep only tokens)
## 2. constituent corpus (ptb style)
## 3. dep corpus (conll)
## output on stdout

## for train and dev datasets: generate tbk format and raw format
c=dev
echo processing DEV
## header of file must be the list of attributes for each token
echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.tbk
python3 ${src}/process_rst.py tbk ${data}constituent_format/${c}.brackets ${data}dependency_format/${c}.conll ${parse}/${c}.parse >> ${folder}/${c}.tbk
echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
python3 ${src}/process_rst.py raw ${data}constituent_format/${c}.brackets ${parse}/${c}.parse >> ${folder}/${c}.raw

## for test generate only raw
c=test
echo processing TEST 
echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
python3 ${src}/process_rst.py raw ${data}constituent_format/${c}.brackets ${parse}/${c}.parse >> ${folder}/${c}.raw
