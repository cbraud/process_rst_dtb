#! /usr/bin/env python

import sys
import getopt
from fastptbparser import *
from heads import *
import re

"""
This is a quick made script for processing penn treebank wall street journal format and converting it to the parser input format
This kind of script is restricted to English data.
I made the choice to ignore the functional tags + empty traces
"""

"""
chloe: changes made for discourse representation:
    - add prefix (first words in a dicourse units, could contain connectives, temporal expressions...)
"""

"""
This defines the order of the token variables in the generated file
"""
global_ptb_attributes = ['word','tag','prefw1','prefw2','prefw3','prefp1','prefp2','prefp3', 'suffw1', 'suffp1', 'len', 'headw1', 'headw2', 'headw3', 'headp1', 'headp2', 'headp3','head', 'position', 'numb', 'perc', 'money', 'date']
# global_ptb_attributes = ['word','tag']

doc_vars = {
      'word':'the raw wordform',\
      'tag': 'the CC tag of the word',\
      'prefw1':'the 1st word of the DU',\
      'prefw2':'the 2nd word of the DU',\
      'prefw3':'the 3rd word of the DU',\
      'prefp1':'the POS of the 1st word of the DU',\
      'prefp2':'the POS of the 2nd word of the DU',\
      'prefp3':'the POS of the 3rd word of the DU',\
      'suffw1':'the last word of the DU',\
      'suffp1':'the POS of the last word of the DU',\
      'len':'lenght of the EDU, ie number of tokens',\
      'headw1':'first head word in the headset',\
      'headw2':'second head word in the headset',\
      'headw3':'third head word in the headset',\
      'headp1':'first head pos in the headset',\
      'headp2':'second head pos in the headset',\
      'headp3':'third head pos in the headset',\
      'head':'head word if inside the EDU, else OUTHEAD tag',\
      'position':'relative position of the EDU in the document',\
      'numb':'if the EDU contains a number that s not a date, a percent or an amount of money',\
      'perc':'if the EDU contains a percentage',\
      'money':'if the EDU contains an amount of money',\
      'date':'if the EDU contains a date',\
      #'ponct':'last ponct if the DU ends with a ponct'
      }



def generate_doc():
    """
    generates a markdown doc of the dataset
    """

    doc_head = """
    The data sets are mainly structured as R datatables:

    * The train files include XML style markup across word lines to indicate bracketing. This turns out to be similar to the IMS CWB format.
    The train files are convertible to R datatables if the markup is removed.
    * The raw test files can be converted to R datatables provided the whitespaces are suppressed
    * The reference test files are still PTB style files without whatever morphology included (required by evalb).

    Standard split:

    * The train section is made of PTB (wall street journal) sections 2-21
    * The dev section is made of PTB (wall street journal) section 22
    * The test section is made of PTB (wall street journal) section 23

    The variables encoded in the Files are (from left to right, with <NA> value when the value is not provided):
    """
    doc_head+='\n'
    for att in global_ptb_attributes:
        if att in doc_vars:
            doc_head += ' '*10 +"* %s : %s"%(att,doc_vars[att]) + '\n'
    print( doc_head, file=sys.stderr )
    return doc_head




'''
Other features used for the PDTB:
    - Semantic category of each verb (<General Inquirer)
    - Polarity, taking into account the presence of a negative word reversing it
    (<MPQA)
    - Modality ie presence of modals can, should ... (requires a list of modals or
    that the POS tagger identifies them, eg MD in the PTB)
    - Connective (<connective lexicon)
    - syntactic features: lexicalized production rules (don t understand why it works,
    maybe we could keep the POS of the first 3 words, to have an idea of the type
    of proposition ?
    + for each verb in the EDU:
    - Levin class (more precisely, the numb of verbs in 2 EDU belonging to the same class)
    - Mean lenght of the VPs
    - POS of the verb (includes a temporal indication in the PTB annotation)

Most important things we miss:
    - Adverbs
    - Modals
    - Temporality (suffix of the verbs, auxiliaries ...)
    - Negation
    Add words around the head if verbal? Add suffix of the head if verbal?
    --> pb: could be not useful for other languages..
'''




def expand_numb( tokens, avm, mode="heur" ):
    '''
    Number features (inspired by PDTB studies)
    mode:
    - ner: use the NER information from the parse if any (not with UD)
    - heuristic based detection, TODO need a translation for the name of the month for any languages
    '''
    percent, money, numb, date = False, False, False, False
    if mode == "ner":
        for i,t in enumerate( tokens ):
            if t['ner'].lower() == 'date':
                date = True
            elif t['ner'].lower() == 'duration':
                date = True
            elif t['ner'].lower() == 'money':
                money = True
            elif t['ner'].lower() == 'percent':
                percent = True
            else:
                if is_number( t['form'] ):#t['pos'] == 'CD'
                    numb = True#or look at the NUMBER ner ?
    else:
        text = ' '.join( [str(t['form']) for t in tokens] )+' '
        date = has_date( text )
        money = has_money( text )
        percent = has_percent( text )
        numb = has_numb( text )

    if numb:
        avm['numb'] = 'numb'
    if percent:
        avm['perc'] = 'perc'
    if date:
        avm['date'] = 'date'
    if money:
        avm['money'] = 'money'

def has_numb( text ):
    numb = re.findall( r'\d+', text )
    return len( numb ) != 0

def has_money( text ):
    '''
    Adding one|two ... |billions of|thousand dollars ? four cents ? seven pounds ? # 11.53 ?
    '''
    money =[]
    money.extend( re.findall(r'(\$\s(\d*\.\d+|\d+|million|billion))', text ) )
    money.extend( re.findall(r'(\d*\.\d+|\d+)\s(pence|cents)', text ) )
    money.extend( re.findall(r'(\d*\.\d+|\d+)\s(million|billion|trillion)', text ) )
    money.extend( re.findall(r'(\d*\.\d+|\d+)\s(yen|marks|euros|dollars|Swiss franc|Canadian dollars|Australian cents|pounds)', text ) )
    money.extend( re.findall(r'(million|billion|trillion)\s(yen|marks|euros|dollars|Swiss franc|Canadian dollars|Australian cents|pounds)', text ) )
    return len( money ) != 0

def has_percent( text ):
    '''
    add percent? percentage point ?
    '''
    percent = []
    percent.extend( re.findall(r'(\d*\.\d+|\d+)\s%', text ) )
    return (len( percent ) != 0)

def has_date( text ):
    dates = []
    dates.extend( re.findall(r'[1-2][0-9][0-9][0-9]\s', text ) )
    dates.extend( re.findall(r'[1-3]?[0-9]\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep?t|Oct|Nov|Dec)\.?', text ) )
    dates.extend( re.findall(r'(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep?t|Oct|Nov|Dec)\.?\s[1-3]?[0-9]', text ) )
    dates.extend( re.findall(r'(January|February|March|April|May|June|July|August|September|October|November|December)\s', text ) )
    return len( dates ) != 0
# -----


# The last ponct is the most interesting, we already have it in the last wd feature
# def expand_ponct( tokens, avm ):
#     '''
#     Only look at the last token and keep it if it is a ponctuation, could be
#     informative for inferring the structure (+ !?)]} could inform on specific relations)
#     '''
#     last_token = tokens[-1]
#     if is_ponct( last_token['form'] ) or is_ponct( last_token['pos'] ):
#         avm['ponct'] = last_token['form']
#     else:
#         avm['ponct'] = '<NA>'


def expand_begwords( wordsforms, avm ):
    '''
    Expands a wordform by extracting first words
    A Discourse Unit is formed by tokens separated by __, we use prefix as the first
    tokens in a DU.
    @return a dict
    '''
    N = len(wordsforms)
    if N > 1:
        avm['prefw1'] = str(wordsforms[0])
    if N > 2:
        avm['prefw2'] = str(wordsforms[1])
    if N > 3:
        avm['prefw3'] = str(wordsforms[2])

def expand_endwords( wordsforms, avm ):
    '''
    Could indicate a parenthetic, some adverbs at the end, presence of a point also
    a good indice.
    '''
    avm['suffw1'] = str(wordsforms[-1])

def expand_begpos( wordspos, avm ):
    '''
    Especially for connectives
    '''
    N = len(wordspos)
    if N > 1:
        avm['prefp1'] = str(wordspos[0])
    if N > 2:
        avm['prefp2'] = str(wordspos[1])
    if N > 3:
        avm['prefp3'] = str(wordspos[2])

def expand_endpos( wordspos, avm ):
    '''
    Especially for connectives
    '''
    avm['suffp1'] = str(wordspos[-1])

def expand_len( wordsforms, avm ):
    '''
    On the RST training data, lenght:
        mean=9.21611253197 max=53 min=1
    '''
    N = len(wordsforms)
    if N > 25:
        avm['len'] = 'verylong'
    elif N > 15:
        avm['len'] = 'long'
    elif N > 5:
        avm['len'] = 'short'
    else:
        avm['len'] = 'veryshort'

def expand_headset( deps, avm ):
    '''
    Head set as proposed  first in Sagae 09.
    Seems that previous authors use raw form, better to use lemma? both? what about pref- suffixe (ie -ing, -ed)
    99% of the example for English have at most 3 heads, we only keep the first 3 heads then
    '''
    head_set = []
    id_in_edu = [t['tok'] for t in deps]
    for token in deps:
        if not token['head'] in id_in_edu:
            # wds whose parent is not within the EDU or is root (ie id parent = 0)
            if not is_ponct(str(token['form'])) and not is_ponct(str(token['pos'])):
                head_set.append( token )
    N = len( head_set )
    if N == 0:
        avm['headw1'], avm['headw2'], avm['headw3'] = '<NA>', '<NA>', '<NA>'
        avm['headp1'], avm['headp2'], avm['headp3'] = '<NA>', '<NA>', '<NA>'
    elif N == 1:
        avm['headw1'] = str( head_set[0]['form'] )
        avm['headw2'], avm['headw3'] = '<NA>', '<NA>'
        avm['headp1'] = str( head_set[0]['pos'] )
        avm['headp2'], avm['headp3'] = '<NA>', '<NA>'
    elif N ==2:
        avm['headw1'] = str( head_set[0]['form'] )
        avm['headw2'] = str( head_set[1]['form'] )
        avm['headw3'] = '<NA>'
        avm['headp1'] = str( head_set[0]['pos'] )
        avm['headp2'] = str( head_set[1]['pos'] )
        avm['headp3'] = '<NA>'
    else:
        avm['headw1'] = str( head_set[0]['form'] )
        avm['headw2'] = str( head_set[1]['form'] )
        avm['headw3'] = str( head_set[2]['form'] )
        avm['headp1'] = str( head_set[0]['pos'] )
        avm['headp2'] = str( head_set[1]['pos'] )
        avm['headp3'] = str( head_set[2]['pos'] )

def expand_head( deps, avm ):
    '''
    If the ROOT of the sentence is in the EDU or not.
    '''
    avm['head'] = 'OUT-HEAD'
    for token in deps:
        if token['fct'].lower() == 'root':

            avm['head'] = 'IN-HEAD'



# def expand_headinfo( deps, avm ):
#     '''
#     Add words in a window of n words around the head: should contain auxiliaries,
#     modals, some adverbes, negation.. at least in English
#     '''
#     window=3
#     window_words = []
#     for i,token in enumerate( deps ):
#         if token['fct'].lower() == 'root':
#             if token['pos'].lower() == "verb":#UD postag
#                 # search dependent that are not noun, pron ...
#                 head_deps = search_deps( deps )
#
#                 print( token["form"], [str(t["form"])+"-"+str(t["pos"]) for t in deps[i-window:i] if not t['pos'] in ['NOUN','PRON','PUNCT','DET','PROPN', 'NUM']], file = sys.stderr )
#             window_words.extend( deps[i-window:i] )
#             break
# #             window_words.extend( deps[i:i+window] ) #useful?
# #     avm["w"]


def expand_position( doc2edu2tokens, document_id, edu_id, avm ):
    '''
    Traditional feature =
    - distance between the EDUs in the stack and in the queue
    - distance between the EDU and the beginning of the doc
    - distance between the EDU and the end of the document
    We give as feature a number representing where is the EDU in the doc
    (round 2)
    e.g. if the EDU is the 3rd in a doc with 10 EDUs, feat = 0.3
         if the EDU is the 4th in a doc with 10 EDUs, feat = 0.4
         if the EDU is the 6th in a doc with 20 EDUs, feat = 0.3
         if the EDU is the 11th in a doc with 20 EDUs, feat = 0.55
    '''
    len_document = len( doc2edu2tokens[document_id] )+0.0

#     sc = edu_id/len_document

    if edu_id == 1:
        avm['position'] = "first"
    elif edu_id == len_document:
        avm['position'] = "last"
    else:
        sc = edu_id/len_document
        if sc < 0.25:
            avm['position'] = "beginning"
        elif sc < 0.5:
            avm['position'] = "first-middle"
        elif sc < 0.75:
            avm['position'] = "second-middle"
        else:
            avm['position'] = "end"
#     print( edu_id, len_document, sc, avm['position'], file=sys.stderr )
#     len_document = len( doc2edu2tokens[document_id] )+0.0
#     avm['position'] = str( round( edu_id/len_document, 2) )


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        try:
            float(s[0])
            return True
        except ValueError:
            return False

def is_ponct(s):
    if str(s) in '.?!,;()[]""\'':
        return True
    return False

def _get_fct( ind, deps ):
    for t in deps:
        if t['tok'] == ind:
            return t['fct']#TODO replace with deprel at some point..
    return None


def ifelse(avm,attribute):
    """
    Values an attribute
    """
    if attribute in avm:
        return avm[attribute]
    else:
        return '<NA>'

def transform_tree(tree_root):
    """
    Manages stripping of functions and lemmatas
    """
    if tree_root.is_tag():
        #print(len(tree_root.children))
        #print(tree_root.children[0])
        #print(tree_root)
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        #tree_root.lemma = tree_root.children[0].label.split('^')[1]
        #tree_root.lemma =
        tree_root.children[0].label = tree_root.children[0].label.split('^')[0]
    else:
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        for child in tree_root.children:
            transform_tree(child)

def print_tree_token(tree_root, all_edus, document_id, doc2edu2tokens, depth):
    avm = {}
    word = tree_root.children[0].label
    avm['tag'] = tree_root.label.replace("##head=true##", "-head").replace("##head=false##", "")
    avm['word'] = tree_root.children[0].label

    all_edus.append( word )
    edu_id = len( all_edus )#EDU ids begin at 1
    # tokens + parse info extracted from an automatic parse, use this tokenisation rather than the first one

    # Retrive tokens, possibly retriveing the translated forms if any (no column translation for the English corpus)
    # We simply replace the 'form' in the dict by the translated version

    tokens = [t for t in sorted( doc2edu2tokens[document_id][edu_id], key=lambda x:x['tok'] )]
#
    expand_begpos( [t['pos'] for t in tokens], avm )
    expand_endpos( [t['pos'] for t in tokens], avm )
    expand_len( [t['form'] for t in tokens], avm )
    expand_head( tokens, avm )
    #     expand_headinfo( tokens, avm )
    expand_position( doc2edu2tokens, document_id, edu_id, avm )
#     expand_ponct( tokens, avm )
    expand_numb( tokens, avm )
#
#
    # -- Produce word features: rm when using embeddings
    expand_headset( tokens, avm )
    expand_begwords( [t['form'] for t in tokens], avm )#TODO use rather lemma?
    expand_endwords( [t['form'] for t in tokens], avm )
    # --


    fvals = [ ifelse(avm,att) for att in global_ptb_attributes]
    print(' '*(depth*2) +'\t'.join(fvals))

def print_tree_token_trueraw(tag, all_edus, document_id, edu_id, doc2edu2tokens, depth):
#     print( doc2edu2tokens[document_id][edu_id] )

    avm = {}
    word ='__'.join( [str(d['form']) for d in doc2edu2tokens[document_id][edu_id]] )

    avm['tag'] = tag
    avm['word'] = word

    all_edus.append( word )
    edu_id = len( all_edus )#EDU ids begin at 1
    # tokens + parse info extracted from an automatic parse, use this tokenisation rather than the first one

    # Retrive tokens
    tokens = [t for t in sorted( doc2edu2tokens[document_id][edu_id], key=lambda x:x['tok'] )]
#
    expand_begpos( [t['pos'] for t in tokens], avm )
    expand_endpos( [t['pos'] for t in tokens], avm )
    expand_len( [t['form'] for t in tokens], avm )
    expand_head( tokens, avm )
    #     expand_headinfo( tokens, avm )
    expand_position( doc2edu2tokens, document_id, edu_id, avm )
#     expand_ponct( tokens, avm )
    expand_numb( tokens, avm )
#
#
    # -- Produce word features: rm when using embeddings
    expand_headset( tokens, avm )
    expand_begwords( [t['form'] for t in tokens], avm )#TODO use rather lemma?
    expand_endwords( [t['form'] for t in tokens], avm )

    fvals = [ ifelse(avm,att) for att in global_ptb_attributes]
    print(' '*(depth*2) +'\t'.join(fvals))


def print_parse_tree(tree_root,all_edus,document_id,doc2edu2tokens,depth=0):
    if tree_root.is_tag():
        print_tree_token(tree_root,all_edus,document_id,doc2edu2tokens,depth+1)
    else:
        if tree_root.label=='':#trashes the ptb root node
            for child in tree_root.children:
                print_parse_tree(child,all_edus,document_id,doc2edu2tokens,depth)
        else:
             lbl = tree_root.label
             print (' '*(depth*2)+'<'+lbl+'>')
             for child in tree_root.children:
                 print_parse_tree(child,all_edus,document_id,doc2edu2tokens,depth+1)
             print (' '*(depth*2)+'</'+lbl+'>')

def print_raw_sent(tree_root, document_id,doc2edu2tokens):
    """
    prints words only
    """
    toklist = tree_root.tag_yield()
    all_edus = []
    for tag,tok in toklist:
        print_tree_token(tag,all_edus,document_id,doc2edu2tokens,0)
    print()

def print_trueraw_sent(document_id,doc2edu2tokens):
    all_edus = []
    for edu_id in doc2edu2tokens[document_id]:
        print_tree_token_trueraw('EDU', all_edus, document_id, edu_id, doc2edu2tokens,0)
    print()


def read_parse( inparse ):
    doc2edu2tokens = {}
    doc = 0
    doc2edu2tokens[doc] = {}
    bfr = inparse.readline()
    cats = bfr.strip().split('\t')
    bfr = inparse.readline()
    while bfr !='':
        #print( doc, file=sys.stderr )
        if bfr == '\n':
            doc += 1
            doc2edu2tokens[doc] = {}
        else:
            #print( bfr, '\n', cats, file=sys.stderr )
            bfr = bfr.strip()
            vals = bfr.split('\t')
            d = {}
            for i,c in enumerate(cats):
                try:
                    d[c] = int(vals[i])
                except:
                    d[c] = vals[i].replace( '#', 'SHARP') # TO TEST! ERROR when tryong to parse # in fold 8 for Basque
            # replace the field 'form' by a translation if any
            if 'transForm' in cats:#no such column for English data
                if d['transForm'] != 'NONE':
                    d['form'] = d['transForm']
                elif d['transLemma'] != 'NONE':
                    d['form'] = d['transLemma']
                elif d['transStem'] != 'NONE':
                    d['form'] = d['transStem']
                #else:#if no translation found, keep the word

            if d['edu'] in doc2edu2tokens[doc]:
                doc2edu2tokens[doc][d['edu']].append( d )
            else:
                doc2edu2tokens[doc][d['edu']] = [d]
        bfr = inparse.readline()
        
    #print( doc2edu2tokens.keys(), file=sys.stderr )
    return doc2edu2tokens

def process_train(inFile,doc2edu2tokens):
    document_id = 0
    bfr = inFile.readline()
    while bfr !='':
        t = parse_line(bfr)
        #transform_tree(t)
        #expand_trigrams(t)
        t = add_dummy_root(t, "ROOT")
        all_edus = []#EDUS of the current document
        print_parse_tree(t,all_edus,document_id,doc2edu2tokens)
        document_id += 1
        bfr = inFile.readline()

def process_raw(inFile, doc2edu2tokens):
    document_id = 0
    bfr = inFile.readline()
    while bfr !='':
        t = parse_line(bfr)
        #print( t, file=sys.stderr )
        #transform_tree(t)
        #expand_trigrams(t)
        t = add_dummy_root(t)
        print_raw_sent(t, document_id,doc2edu2tokens)
        bfr = inFile.readline()
        document_id += 1

def process_trueraw(doc2edu2tokens):
    document_id = 0
    for doc in doc2edu2tokens:
        print_trueraw_sent(document_id,doc2edu2tokens)
        document_id += 1

def process_ref(inFile):

    bfr = inFile.readline()
    while bfr !='':
        t = parse_line(bfr)
        #transform_tree(t)
        t = add_dummy_root(t, "ROOT")
        print(t.do_flat_string())
        bfr = inFile.readline()



def main():
    import sys

    if sys.argv[1] == "tbk" :
        ptbfile = sys.argv[2]
        conllfile = sys.argv[3]
        R = get_knn_dataset(open(ptbfile), open(conllfile) )

        featfile = sys.argv[4]# each EDU of each doc is associated with some info

        ptbf = open(ptbfile)
        conllf = open(conllfile)
        with open("tmp.mrg", "w") as out :
            head_annotate(ptbf, conllf, out, R, 5)

        # build an association between each EDU and its parse info doc : edu : list of tokens
        with open(featfile, "r", encoding="utf8") as inparse:
            doc2edu2tokens = read_parse( inparse )

        with open("tmp.mrg", "r", encoding="utf8") as instream:
            process_train(instream, doc2edu2tokens)

    elif sys.argv[1] == "raw" :

        featfile = sys.argv[3]# each EDU of each doc is associated with some info
        # build an association between each EDU and is parse info doc : edu : list of tokens
        with open(featfile, "r", encoding="utf8") as inparse:
            doc2edu2tokens = read_parse( inparse )

        process_raw(open(sys.argv[2], "r"), doc2edu2tokens)

    elif sys.argv[1] == "trueraw" : #only the parse file, no gold tree

        featfile = sys.argv[2]# each EDU of each doc is associated with some info
        # build an association between each EDU and is parse info doc : edu : list of tokens
        with open(featfile, "r", encoding="utf8") as inparse:
            doc2edu2tokens = read_parse( inparse )

        process_trueraw(doc2edu2tokens)

main()




"""
options, remainder = getopt.getopt(sys.argv[1:], 'trdg', ['--train',
                                                         '--raw',
                                                         '--ref',
                                                         '--doc'])
generate_type = ''
for opt, arg in options:
    if opt in ('-t', '--train'):
        process_train(sys.stdin)
    elif opt in ('-r', '--raw'):
        process_raw(sys.stdin)
    elif opt in ('-g', '--ref'):
        process_ref(sys.stdin)
    elif opt in ('-d', '--doc'):
       print  generate_doc()

"""
