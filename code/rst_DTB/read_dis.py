#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
- Generate tree files (nltk Tree) from the RST like corpora
- For the RST DT: use code from discourse-parsing-master to make corrections in the data ie:
    - modify the names of some files
    - TODO: modify the text of some EDUs
        - some pb can t be solved, see discourse-parsing-master warnings:
        file1.edus, file5.edus, wsj_0678.out.edus, and wsj_2343.out.edus.
        Multiple warnings 'not enough syntax trees' will be produced
        because the RSTDTB has footers that are not in the PTB (e.g.,
        indicating where a story is written). Also, there are some loose
        match warnings because of differences in formatting between
        treebanks
    - use code from DPLP to generate trees, make use of different mappings of the relations
    - TODO: use the code from discourse-parsing-master to align PTB and RST DT trees and generate JSON files?
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs

import data

def main( ):
    parser = argparse.ArgumentParser(
            description='Discourse corpus (for now .dis or .rs3).')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            default="/Users/chloe/navi/discourse/data/rstdt_samples/",
            help='Input file/directory to read (RST .dis files, RST .rs3 files)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            default="/Users/chloe/navi/discourse/data/rstdt_samples/",
            help='Output directory (.bracketed, .edus and .ps files)')
    parser.add_argument('--mapping',
            dest='mapping',
            action='store',
            default=None,
            help='Label mapping (original_relation class)')
    parser.add_argument('--draw',
            dest='draw',
            action='store',
            default=True,
            help='Draw a ps file for each tree (Default=True)')
    args = parser.parse_args()

    readDis( vars(args)['treebank'], vars(args)['outpath'], vars(args)['mapping'], vars(args)['draw'] )


def readDis( tbpath, outpath, mappingFile, draw ):
    if os.path.isdir( os.path.join( tbpath, 'TRAINING' ) ):
        for dataset in ['TRAINING', 'TEST']:
            corpus = data.Corpus(
                    os.path.join( tbpath, dataset ),
                    mappingFile,
                    datatype="dis",
                    draw=draw )
            print( corpus.__str__(), file=sys.stderr )
            corpus.read( )
    #         corpus.mapRelations( )
            outpath_ = os.path.join( outpath, dataset )
            if not os.path.isdir( outpath_ ):
                os.mkdir( outpath_ )
            corpus.write( outpath_ )
    else:
        corpus = data.Corpus(
                tbpath,
                mappingFile,
                datatype="dis",
                draw=draw )
        print( corpus.__str__(), file=sys.stderr )
        corpus.read( )
    #         corpus.mapRelations( )
        outpath_ = outpath
        if not os.path.isdir( outpath_ ):
            os.mkdir( outpath_ )
        corpus.write( outpath_ )


if __name__ == '__main__':
    main()



