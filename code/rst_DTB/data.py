#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import os, sys, codecs
from nltk import Tree

from common import *
from utils_rs3 import *
from utils_dis import *
from utils_chn import *
from relationSet import *


'''
TODO:
    - for now, read the entire corpus before writing, do both at the same time
    - Chinese corpus
    c = codecs.open('/Users/chloe/Downloads/rechinesecorpusannotatedatthediscourelevel/Chinese_discourse_corpus-4747.txt', encoding='gb2312' )
'''


#Copy the files for which we were not able to build a tree in this dir
PBPATH='data/experiments/eacl-16-multiling-rst/pb-data/'

class Corpus:
    def __init__( self, tbpath, mappingFile, datatype="dis", draw=True ):
        self.path = tbpath
        self.mappingFile = mappingFile
        self.datatype = datatype
        self.draw = draw #draw a ps file for each tree
        self.files = []
        self.edufiles = []
        self.documents = []
        self.validDocuments = []#document for which a valid tree has been built, hopefuly will be rm later

        self.outputExt = ".bracketed" #Extension of the output files, modified if mapping used
        self.labelsMapping= None
        if os.path.isfile( self.mappingFile ):
            self.labelsMapping, self.outputExt, self.nbclasses = getLabelMapping( self.mappingFile, self.outputExt ) #Get label mapping
        self.originLabels, self.finalLabels = set(), set() #Keep track of the original/final relation set
        self.nuclearity_relations = {}

        self.pb_files = []#keep the names of files for which we couldnt build a tree

        self.statistics = {} #Save some information about the data

    def read( self ):
        if self.datatype == "rs3":#Read .rs3 annotations
            self.files = getRS3Files( self.path )
            for fd in self.files:
                print( "Reading: "+fd )
                doc = Rs3Document( fd )
                doc.read()
                addLabels( doc.tree, self.originLabels )
                if self.mappingFile:#TODO moche faire autrement
                    doc.mapRelation( self.mappingFile )
                    addLabels( doc.tree, self.finalLabels )
                self.documents.append( doc )

                self.updateNucRel( doc ) #Keep trace of the rel set defined in each file
                self.updateStatistics( doc )

            #self.printNucRel() #Relation set, each relationa associated with the possible nuclearity

        elif self.datatype == "dis":#Read .dis annotations
            self.files, self.edufiles = getDisFiles( self.path )
            for fd in self.files:
                print( "Reading: "+fd )
                doc = DisDocument( fd )
                doc.read( self.edufiles ) #TODO retrieve the correspondant edu file before

                addLabels( doc.tree, self.originLabels )
                if self.mappingFile:#TODO moche faire autrement
                    doc.mapRelation( self.mappingFile )
                    addLabels( doc.tree, self.finalLabels )

                self.documents.append( doc )
                #self.addLabels( doc.tree ) #TODO

#                 self.updateNucRel( doc ) #TODO Keep trace of the rel set defined in each file
                self.updateStatistics( doc )

        elif self.datatype == "chn":#chinese rstdt
            rel2count = {}
            self.files = getCHNFiles( self.path )
            print( "Reading: ",self.files, file=sys.stderr)
            # one file/tree (original document previously split and re write in utf8)
            for fd in self.files:
                print( "Reading: "+fd, )
                doc = ChnDocument( fd )
                doc.read( )
                addLabels( doc.tree, self.originLabels )
                if self.mappingFile:#TODO moche faire autrement
                    doc.mapRelation( self.mappingFile )
                    addLabels( doc.tree, self.finalLabels )

                self.documents.append( doc )
#                 self.updateStatistics( doc )
                countLabels( doc.tree, rel2count )

#             for r in rel2count:
#                 print( r, rel2count[r] )
        elif self.datatype == "thiago":#Read .dis annotations
            self.files = getRS3Files( self.path, ext='.txt.lisp.thiago' )
            for fd in self.files:
#                 print( os.path.join( "Sandbox", os.path.basename(fd)+".bracketed" ) )
#                 sys.exit()
                print( "="*30, "\nReading: "+fd, "\n", "="*30 )
                doc = ThiagoDocument( fd )
                doc.read()

                addLabels( doc.tree, self.originLabels )
                if self.mappingFile:#TODO moche faire autrement
                    doc.mapRelation( self.mappingFile )
                    addLabels( doc.tree, self.finalLabels )

                self.documents.append( doc )
                #self.addLabels( doc.tree ) #TODO

#                 self.updateNucRel( doc ) #TODO Keep trace of the rel set defined in each file
                self.updateStatistics( doc )

        else:
            sys.exit( "Unknown data type "+self.datatype )
        ###self.validDocuments = [d for d in self.documents if d.tree != None]
        self.validDocuments = self.documents
        self.pb_files = [d.path for d in self.documents if d.tree == None]
        # nb of doc read / nb of doc for which a tree has been built
        print( "#Files read:", len( self.files ), "#Doc read/#Tree built:", len( self.validDocuments ) )

        self.printLabels()
#         self.printStatistics( )


    def write( self, outpath ):
        if not os.path.isdir( outpath ):
            os.mkdir( outpath )
        docno = 0 #ued for corpora with only one file containing all the doc
        for doc in self.validDocuments:
            #print( "Writing: "+doc.path )
#             print( outpath, doc.datatype, self.outputExt )
            doc.datatype = 'rs3'#'rs3' #TEMP TODO rm
#             try:
            doc.writeTree( outpath, '.'+doc.datatype, self.outputExt, docno=docno ) #TODO datatype not found..
            doc.writeEdu( outpath, docno=docno )
# #             except:
# #                 print( "Problem in writing", doc.path )




            if self.draw:#create a picture representing the tree
                if self.labelsMapping == None:
                    doc.drawTree( outpath, '.'+doc.datatype, '.ps', docno=docno )
                else:
                    doc.drawTree( outpath, '.'+doc.datatype, '.map'+str( self.nbclasses )+'.ps', docno=docno )
            docno += 1

        # Write the list of documents for which we couldnt build a tree
        if len( self.pb_files ) != 0:
            with open( os.path.join( outpath, "pb_files" ), 'w' ) as f:
                f.write( '\n'.join( self.pb_files ) )



#     def mapRelations( self ):# perform mapping if file mapping
#         for doc in self.validDocuments:
#             print( "Mapping", doc.path )
#             #originLabels = addLabels( doc.tree, self.originLabels )
#             #addLabels( doc.tree, self.finalLabels )
# #             mapLabels( doc.tree, self.labelsMapping ) # Modify the tree in place
#             doc.mapRelation( self.mappingFile )
#             addLabels( doc.tree, self.finalLabels )

#     def addLabels( self, tree ):
#         """
#         Fill the label set, used to check which relations exactly are used in the corpus
#         """
#         if tree != None:
#             for st in tree.subtrees():
#                 label = st.label()
#                 if not label.lower() == "edu":
#                     relation = getRelation( label )
#                     self.originLabels.add( relation )
#                     if "span" in relation[0].lower():
#                         sys.exit( "Still a span relation??")



    # -- UTILS
    def updateNucRel( self, doc ): #Keep trace of the rel set defined in each file
        for r in doc.nuclearity_relations:
            if r in self.nuclearity_relations:
                for n in doc.nuclearity_relations[r]:
                    self.nuclearity_relations[r].add( n )
            else:
                self.nuclearity_relations[r] = set( doc.nuclearity_relations[r] )

    def updateStatistics( self, doc ):
        for k in doc.statistics:
            if k in self.statistics:
                try: #int value
                    self.statistics[k] += doc.statistics[k]
                except: #or dict
                    for k2 in doc.statistics[k]:
                        if k2 in self.statistics[k]:
                            self.statistics[k][k2] += doc.statistics[k][k2]
                        else:
                            self.statistics[k][k2] = doc.statistics[k][k2]
            else:
                self.statistics[k] = doc.statistics[k]

    def printLabels( self ):
        ''' The label sets record tuples (relation, nuclearity)  '''
        # -- Originaly
        labels = np.unique( [l for (l,n) in self.originLabels] )
        print( "#Original Labels:"+str(len( labels) ) )
        print( ', '.join( sorted( labels ) ) )
#         print( '\n'.join( ['u'+'\''+l+'\''+':'+l+',' for l in sorted( labels ) ] ) )
        # -- Finaly
        labels = np.unique( [l for (l,n) in self.finalLabels] )
        print( "#Final Labels:"+str(len( labels) ) )
        print( ', '.join( sorted( labels ) ) )


    def printNucRel( self ):
        '''Relation set, each relationa associated with the possible nuclearity'''
        for r in sorted( self.nuclearity_relations ):
            print( r+'\t'+' '.join( self.nuclearity_relations[r] ))

    def printStatistics( self ):
        for k in self.statistics:
            if not k.startswith( '#' ):#Test isADict ?
                print( "\nSTATS", k )
                for k2 in self.statistics[k]:
                    print( k2, self.statistics[k][k2] )
            else:
                print( k, self.statistics[k] )

    def __str__( self ):
        return ' '.join( [self.path, "Type:", self.datatype] )


class Document:
    def __init__( self, dpath ):#? parse=parse, raw=raw
        self.path = dpath
        self.datatype = None
        self.tree = None
        self.tokendict = None # Token dict: id token in the document -> token form
        self.eduIds = []
        self.edudict = None # EDU dict: id EDU -> list of id tokens
        self.outbasename = os.path.basename( self.path ) #Name of the output file, can be modified for the RST DT
        #self.relapairs = None
        self.statistics = {} # statistics for one document

    def read( self ):
        raise NotImplementedError

    #def writeTree( tree, forigin, ext, outExt, pathout ):
    def writeTree( self, outpath, ext, outExt, docno=-1 ):
        '''Write the bracketed tree into a file'''
        if not self.outbasename.endswith( ext ):#For modified file names
            fileout = os.path.join( outpath, self.outbasename+outExt )
        else:
            fileout = os.path.join( outpath, self.outbasename.replace( ext, outExt ) )
        with open( fileout, 'w' ) as fout:
            fout.write( self.tree.__str__().strip() )

    def drawTree(self, outpath, ext, outExt, docno=-1 ):
        '''Draw RST tree into a file'''
        if not self.outbasename.endswith( ext ):#For modified file names
            fileout = os.path.join( outpath, self.outbasename+outExt )
        else:
            fileout = os.path.join( outpath, self.outbasename.replace( ext, outExt ) )
        cf = CanvasFrame()
        tc = TreeWidget(cf.canvas(), self.tree)
        cf.add_widget(tc,10,10) # (10,10) offsets
        cf.print_to_file( fileout )
        cf.destroy()

    def mapRelation( self, mappingRel ):#TODO not use file anymore (or keep the possiblity?), use directly the lists
        if self.tree == None:
            return
        if os.path.isfile( mappingRel ):
            sys.exit( "Mapping RS3 from file not implemented yet" )
        else:
            if mappingRel == 'basque_labels':
                performMapping( self.tree, basque_labels )
            elif mappingRel == 'brazilianCst_labels':
                performMapping( self.tree, brazilianCst_labels )
            elif mappingRel == 'brazilianSum_labels':
                performMapping( self.tree, brazilianSum_labels )
            elif mappingRel == 'germanPcc_labels':
                performMapping( self.tree, germanPcc_labels )
            elif mappingRel == 'multiLing_labels':
                performMapping( self.tree, multiLing_labels )
            elif mappingRel == 'spanish_labels':
                performMapping( self.tree, spanish_labels )
            elif mappingRel == 'rstdt_mapping18':
                performMapping( self.tree, rstdt_mapping18 )
            elif mappingRel == 'chinese_labels':
                performMapping( self.tree, chinese_labels )
            elif mappingRel == 'dutch_labels':
                performMapping( self.tree, dutch_labels )
            elif mappingRel == 'brazilianTCC_labels':
                performMapping( self.tree, brazilianTCC_labels )
            elif mappingRel == 'mapping':#use the common mapping to 18 classes
                performMapping( self.tree, mapping )
            else:
                print( "Unknown mapping: "+mappingRel )
#                 sys.exit( "Unknown mapping: "+mappingRel )


class Rs3Document( Document ):
    '''
    Class for a document encoded in rs3 format.
    - XML format
    - the relation list in the header gives the nuclearity of the relations
    - EDU id are not always continuous: EDU are renamed
    - EDU text is tokenized
    - For some corpora/languages, the binarization using right branching is not enough,
    a more general strategy is used
    - An EDU file is created
    '''
    def __init( self, dpath ):
        Document.__init__(self, dpath)
        self.datatype = "rs3"
        self.nuclearity_relations = {}

    def read( self ):
        '''
        Create an NLTK Tree, self.tree, from the rs3 file + preprocessing (binarize, check)
        Fill self.tokendict and self.edudict
        '''
        doc_root, rs3_xml_tree = parseXML( self.path )
        self.nuclearity_relations = getRelationsType( rs3_xml_tree ) #Retrieve the relations in the header (used to find multinuc rel)
        eduList, groupList, root = readRS3Annotation( doc_root ) #Get info for each node
        tree = buildNodes( eduList, groupList, root, self.nuclearity_relations ) #Build nodes, rename DU, tree=SpanNode instance
        eduIds = [e["id"] for e in eduList] #Can t be retrieved from the tree for now, some EDU have children
        orderSpanList( tree, eduIds ) #Order span list for each node

        computeStatsRs3( tree, self, eduList, self.nuclearity_relations ) #Compute stats for the doc

        cleanTree( tree, eduIds, self.nuclearity_relations, self )#Clean the tree: deal with DU with only one child + same unit cases
        self.tokendict, self.edudict = retrieveEdu( tree, eduIds )#Retrieve info about the text of the EDUs

        non_bin_tree = tree

# #         # If you want to see a picture of the tree before binarization, keep this line, comment the rest
#         self.tree = Tree.fromstring( getParseNobin(tree, "") )
#         Tree.draw( self.tree )
#
        binarizeTreeGeneral(tree, self, nucRelations=self.nuclearity_relations) # Binarize the tree
#
# #         printBinTree( tree )
#
        tree = backprop(tree, self) # Backprop info
        self.tree = Tree.fromstring( parse(tree) )# Build an nltk tree
#
#         Tree.draw( self.tree )
#
        validTree = checkTree( self.tree, non_bin_tree, self )
        if not validTree:
            self.tree = None
#             shutil.copy( self.path, PBPATH )



    def writeEdu( self, outpath, docno=-1 ):
        writeEdus( self, ".rs3", outpath ) #Write edu files similar to the ones used in the RST DT



class DisDocument( Document ):
    def __init__( self, dpath ):
        Document.__init__(self, dpath)
        self.datatype = "dis"
        self.eduPath = None

    def read( self, eduFiles ):
#         basename = os.path.basename( self.path ).split('.')[0] # split not possible for Instructional corpus TODO
        basename = os.path.basename( self.path ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '')
        if basename in file_mapping:#Modify the name of some specific files in the RST DT
            self.outbasename = file_mapping[basename]
        self.eduPath = findFile( eduFiles, basename )# Retrieve EDUs file
        if self.eduPath == None:
            sys.exit( "Edus file not found: "+basename )
        tree, self.eduIds = buildTree(open( self.path ).read()) # Build RST as annotation

#         print( "self.eduIds", self.eduIds )

        computeStats( tree, self )

        tree = binarizeTreeRight(tree)# Binarize it
        doc = readEduDoc( self.eduPath, self )# Retrieve info on EDUs
        tree = backprop(tree, self)
        str_tree = parse(tree)#Get nltk tree
        self.tree = Tree.fromstring( str_tree )


    def writeEdu( self, outpath, docno=-1 ):
        # copy the EDU file, possibly rename it using the file mapping
#         print( "\nWRITE EDU DIS", self.outbasename, os.path.basename( self.path.split('.')[0] ), file=sys.stderr )#For modified files
        if self.outbasename != os.path.basename( self.path.split('.')[0] ):
            shutil.copy( self.eduPath, os.path.join( outpath, self.outbasename+'.edus' ) )
        else:
            shutil.copy( self.eduPath, outpath )


class ThiagoDocument( Document ):
    def __init__( self, dpath ):
        Document.__init__(self, dpath)
        self.datatype = "thiago"
        self.eduPath = None


    def read( self ):
        tree, self.eduIds, allnodes, self.edudict = buildTreeThiago(open( self.path, encoding="windows-1252" ).read())
        tree = bTree( allnodes, self.path )

#         printThiagoList( tree )

        tree = binarizeTreeRightThiago(tree)
#         tree = binarizeTreeGeneral( tree, self )
#         printThiago( tree )
#         self.edudict = retrieveEduThiago( open( self.path, encoding="windows-1252" ).read() )

#         printThiago( tree )
        tree = backprop(tree, self) # Backprop info
#         print( tree )
        str_tree = parse(tree)
#         print( str_tree )
        self.tree = Tree.fromstring( parse(tree) )
#         Tree.draw( self.tree )
#         sys.exit()

    def writeEdu( self, outpath, docno=-1 ):
        writeEdusChn( self, ".thiago", outpath )

class ChnDocument( Document ):
    def __init__( self, dpath ):
        '''
        No token dict here? Don't know how to split the string of each EDU
        self.edudict : directly the text of each EDU
        '''
        Document.__init__(self, dpath)
        self.datatype = "chn"
        self.eduPath = None

    def read( self ):
        self.edudict, relations = self.get_annotations(  )

        self.eduIds = sorted( self.edudict.keys() )
#
        self.tree = self.buildTree( relations )

    def buildTree( self, relations ):

        tree = None
        # find top node
        try:
            top_node, a1, a2 = self.find_top_node( relations )
            tree = Tree( top_node.label(), [self._buildTree( a1, relations ), self._buildTree( a2, relations )] )
        except:
            # pb for trees with no node covering all the EDUs, cross dep? multiple parents ?
            tree = None

#         sys.exit()
        return tree

    def _buildTree( self, arg, relations ):
        if len( arg ) == 1:
            # EDU : need to add a node with the label EDU with the label of the EDU as child
            return Tree( "EDU", [arg[0]] )
        else:
            # find the relation covering the EDUS
            tree, a1, a2 = self._find_node( arg, relations )
            return Tree( tree.label(), [ self._buildTree( a1, relations ), self._buildTree( a2, relations ) ] )

    def _find_node( self, arg, relations ):
        for (rel, a1, a2 ) in relations:
            edus = [a for a in a1]
            edus.extend( a2 )
            if sorted(edus) == sorted( arg ):
                return Tree( rel, [] ), a1, a2
        return None

    def find_top_node( self, relations ):
        for (rel, a1, a2 ) in relations:
            edus = [a for a in a1]
            edus.extend( a2 )
#             print( edus, self.eduIds )
            if sorted(edus) == self.eduIds:
                return Tree( rel, [] ), a1, a2
        return None


    def get_annotations( self ):
        '''
        1 tree per sentence
        {ID} the ID of the EDU
        [REL, ARG1, ARG2] relation name and arguments (ie ID, with ID1-ID2 for CDU)
        in between the text of the EDU
        '''
        line = open( self.path, encoding="utf8" ).read().strip()
        line = self.correct_text( line )

        pos = 0
        relations = []
        edus = {}

        end_index = len( line )
        cur_edu_text = ''
        cur_edu_id = -1
        while pos != end_index:
            c = line[pos]
            if c == '{':#Id of the current EDU, {ID} text
#                 print( cur_edu_id, cur_edu_text )
                if cur_edu_id > 0:
                    edus[cur_edu_id] = cur_edu_text.replace( ']', '' ).strip()#RB from the rel annot
                # Get the ID of the current EDU
                j = pos
                cur_edu = ''
                while line[j] != '}':
                    cur_edu += line[j]
                    j += 1
                pos = j+1
                cur_edu_id = int( cur_edu.replace('{','').replace('}','') )
                cur_edu_text = '' # Begin a new EDU
            if c == '[':
                # Get the relation annotations
                j = pos
                cur_rel = ''
                while line[j] != ']':
                    cur_rel += line[j]
                    j += 1
                pos = j
                rel,arg1,arg2 = cur_rel.replace('[','').replace(']', '').split(',')
                if '-' in arg1:
                    arg1 = [int(a) for a in arg1.split('-')]
                    arg1 = list( range( arg1[0], arg1[-1]+1 ) )
                else:
                    arg1 = [int(arg1)]
                if '-' in arg2:
                    arg2 = [int(a) for a in arg2.split('-')]
                    arg2 = list( range( arg2[0], arg2[-1]+1 ) )
                else:
                    arg2 = [int(arg2)]
                relations.append( tuple([rel, arg1, arg2]) )
            cur_edu_text += line[pos]
            pos += 1
        if cur_edu_id > 0:
            edus[cur_edu_id] = cur_edu_text.replace( ']', '' ).strip()#RB from the rel annot
#         print( "\nEDUS:   ", edus )
#         print( "\nRELA:   ", relations )
        return edus, relations


    def correct_text( self, line ):
        '''
        TODO move to common/utils
        The [] are used to indicate a relation and its argument, but the first
        sentence contains brackets around ome text: we replace these brackets
        using the LSB/RSB encoding
        '''
        if '}[' in line:
            while '}[' in line:
                new_line = self._correct_text( line )
                line = new_line
        return line

    def _correct_text( self, line ):
        new_line = []
        pos = 0
        pos_beg = 0
        found_end = False
        while pos != len( line ):
            if line[pos] == '}' and line[pos+1] == '[':
                pos_beg = pos+1
                break
            pos += 1
        for i in range( 0, len(line) ):
            if i == pos_beg:
                new_line.append( u'-LSB-' )
            elif i > pos_beg and line[i] == ']' and found_end == False:
                new_line.append( u'-RSB-' )
                found_end = True
            else:
                new_line.append( line[i] )
        return ''.join( new_line )


    # shouldn be specific fct here, just distinguih on the datatype
    def writeEdu( self, outpath, docno=-1 ):
        writeEdusChn( self, ".chn", outpath, docno=docno ) #Write edu files similar to the ones used in the RST DT

    def writeTree( self, outpath, ext, outExt, docno=-1 ):
        '''Write the bracketed tree into a file'''
        fileout = os.path.join( outpath,
                os.path.basename( self.path ).split('.')[0]+outExt )
        with open( fileout, 'w' ) as fout:
            try:
                fout.write( self.tree.__str__().strip() )
            except:
                sys.exit()

    def drawTree(self, outpath, ext, outExt, docno=-1 ):
        '''Draw RST tree into a file'''
        fileout = os.path.join( outpath,
                os.path.basename( self.path ).split('.')[0]+outExt )
        try:
            cf = CanvasFrame()
            tc = TreeWidget(cf.canvas(), self.tree)
            cf.add_widget(tc,10,10) # (10,10) offsets
            cf.print_to_file( fileout )
            cf.destroy()
        except:
            print( 'unable to draw the tree' )




# DATA STRUCTURE classes (<DPLP)
# ----------------------------------------------------------------------------------

class SpanNode:
    """ RST tree node
    """
    def __init__(self, prop):
        """
        Initialization of SpanNode

        :type text: string
        :param text: text of this span
        """
        self.text, self.relation = None, None # Text of this span / Discourse relation
        self.eduspan, self.nucspan = None, None # EDU span / Nucleus span (begin, end) index id EDU
        self.nucedu = None # Nucleus single EDU (itself id for an EDU)s
        self.prop = prop # Property: Nucleus/Satellite/Roots
        self.lnode, self.rnode = None, None # Children nodes (for binary RST tree only)
        self.pnode = None # Parent node
        self.nodelist = [] # Node list (for general RST tree only)
        self.form = None # Relation form: NN, NS, SN
        self.eduCovered = [] # Id of the EDUS covered by a CDU (CHLOE Added)
        self._id = None # Id (int) of a DU, only from rs3 files (CHLOE Added)

    def __str__( self ):
        return self._info()+"\n"+"\n".join( "\t"+n._info() for n in self.nodelist )

    def _info( self ):#TODO rm?
        return "eduspan: "+str(self.eduspan)
