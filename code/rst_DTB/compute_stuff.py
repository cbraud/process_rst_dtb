#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse
import os, sys, shutil, codecs
import numpy as np
from nltk import Tree
from nltk.tokenize import treebank


TOKENIZER = treebank.TreebankWordTokenizer()

rstdt_mapping18 = {
    'analogy':'Comparison',
    'antithesis':'Contrast',
    'attribution':'Attribution',
    'attribution-negative':'Attribution',
    'background':'Background',
    'cause':'Cause',
    'cause-result':'Cause',
    'circumstance':'Background',
    'comment':'Evaluation',
    'comment-topic':'Topic-Comment',
    'comparison':'Comparison',
    'concession':'Contrast',
    'conclusion':'Evaluation',
    'condition':'Condition',
    'consequence':'Cause',
    'contingency':'Condition',
    'contrast':'Contrast',
    'definition':'Elaboration',
    'disjunction':'Joint',
    'elaboration-additional':'Elaboration',
    'elaboration-general-specific':'Elaboration',
    'elaboration-object-attribute':'Elaboration',
    'elaboration-part-whole':'Elaboration',
    'elaboration-process-step':'Elaboration',
    'elaboration-set-member':'Elaboration',
    'enablement':'Enablement',
    'evaluation':'Evaluation',
    'evidence':'Explanation',
    'example':'Elaboration',
    'explanation-argumentative':'Explanation',
    'hypothetical':'Condition',
    'interpretation':'Evaluation',
    'inverted-sequence':'Temporal',
    'list':'Joint',
    'manner':'Manner-Means',
    'means':'Manner-Means',
    'otherwise':'Condition',
    'preference':'Comparison',
    'problem-solution':'Topic-Comment',
    'proportion':'Comparison',
    'purpose':'Enablement',
    'question-answer':'Topic-Comment',
    'reason':'Explanation',
    'restatement':'Summary',
    'result':'Cause',
    'rhetorical-question':'Topic-Comment',
    'same-unit':'Same-unit',
    'sequence':'Temporal',
    'statement-response':'Topic-Comment',
    'summary':'Summary',
    'temporal-after':'Temporal',
    'temporal-before':'Temporal',
    'temporal-same-time':'Temporal',
    'textual-organization':'Textual-organization',
    'textualorganization':'Textual-organization',
    'topic-comment':'Topic-Comment',
    'topic-drift':'Topic-Change',
    'topic-shift':'Topic-Change',
    }

mapping = {#The most similar to the RST
        # Brazilian
    u'antithesis':u'Contrast',
    u'attribution':u'Attribution',
    u'background':u'Background',
    u'circumstance':u'Background',
    u'comparison':u'Comparison',
    u'concession':u'Contrast',
    u'conclusion':u'Evaluation',
    u'condition':u'Condition',
    u'contrast':u'Contrast',
    u'elaboration':u'Elaboration',
    u'enablement':u'Enablement',
    u'evaluation':u'Evaluation',
    u'evidence':u'Explanation',
    u'explanation':u'Explanation',
    u'interpretation':u'Evaluation',
    u'joint':u'Joint',
    u'justify':u'Explanation', #TODO
    u'list':u'Joint',
    u'means':u'Manner-Means',
    u'motivation':u'Explanation', #TODO
    u'non-volitional-cause':u'Cause',# Not in the RST DT (bu in the theoretical framework)
    u'non-volitional-result':u'Cause',#Map these relations to cause and result? see the # of inst
    u'otherwise':u'Condition',
    u'parenthetical':u'Elaboration', #TODO
    u'purpose':u'Enablement',
    u'restatement':u'Summary',
    u'same-unit':u'Same-unit',
    u'sequence':u'Temporal',
    u'solutionhood':u'Topic-Comment',#TODO
    u'volitional-cause':u'Cause',
    u'volitional-result':u'Cause',
    # BASQUE
    u'ahalbideratzea':u'Enablement',
    u'alderantzizko-baldintza':u'Condition',#not used in the rst dt #TODO
    u'antitesia':u'Contrast',
    u'arazo-soluzioa':u'Topic-Comment',#not used in the rst dt, solution-hood changed to 'solutionhood' used in the other corpora, map to problem-solution (<rst dt) ? #TODO
    u'aukera':u'Condition',
    u'baldintza':u'Condition',
    u'bateratzea':u'Joint',#Join on the website but Joint is a most common name, note however that it is the group name for List and disjunction, not a relation is the original set, check the meaning of this relation
    u'birformulazioa':u'Summary',
    u'definitu-gabeko-erlazioa':u'Summary',#not used in the rst dt
    u'disjuntzioa':u'Joint',
    u'ebaluazioa':u'Evaluation',
    u'ebidentzia':u'Explanation',
    u'elaborazioa':u'Elaboration',
    u'ez-baldintzatzailea':u'Condition',#not used in the rst dt#TODO
    u'helburua':u'Enablement',
    u'interpretazioa':u'Evaluation',
    u'justifikazioa':u'Explanation',#not used in the rst dt#TODO
    u'kausa':u'Cause',
    u'konjuntzioa':u'Joint',#not used in the rst dt, replace by List? Joint?#TODO
    u'kontrastea':u'Contrast',
    u'kontzesioa':u'Contrast',
    u'laburpena':u'Summary',
    u'lista':u'Joint',
    u'metodoa':u'Manner-Means',
    u'motibazioa':u'Explanation',#not used in the rst dt#TODO
    u'ondorioa':u'Cause',
    u'prestatzea':u'Background',#Preparation not used in the rst dt#TODO
    u'same-unit':u'Same-unit',
    u'sekuentzia':u'Temporal',
    u'testuingurua':u'Background',
    u'zirkunstantzia':u'Background',
    # multiling
    u'antithesis':u'Contrast',
    u'antítesis':u'Contrast',
    u'background':u'Background',
    u'causa':u'Cause',
    u'cause':u'Cause',
    u'circumstance':u'Background',
    u'circunstancia':u'Background',
    u'concesión':u'Contrast',
    u'concession':u'Contrast',
    u'condición':u'Condition',
    u'condition':u'Condition',
    u'conjunction':u'Joint',#TODO
    u'contrast':u'Contrast',
    u'contraste':u'Contrast',
    u'disjunction':u'Joint',
    u'elaboración':u'Elaboration',
    u'elaboration':u'Elaboration',
    u'evaluación':u'Evaluation',
    u'evaluation':u'Evaluation',
    u'evidence':u'Explanation',
    u'evidencia':u'Explanation',
    u'fondo':u'Background',
    u'interpretación':u'Evaluation',
    u'interpretation':u'Evaluation',
    u'justificación':u'Explanation',#TODO
    u'justify':u'Explanation',#TODO
    u'list':u'Joint',
    u'lista':u'Joint',
    u'means':u'Manner-Means',
    u'medio':u'Manner-Means',
    u'motivación':u'Explanation',#TODO
    u'motivation':u'Explanation',#TODO
    u'preparación':u'Background',#TODO
    u'preparation':u'Background',#TODO
    u'propósito':u'Enablement',
    u'purpose':u'Enablement',
    u'reformulación':u'Summary',
    u'restatement':u'Summary',
    u'result':u'Cause',
    u'resultado':u'Cause',
    u'resumen':u'Summary',
    u'same-unit':u'Same-unit',
    u'secuencia':u'Temporal',
    u'sequence':u'Temporal',
    u'solución':u'Topic-Comment',#TODO
    u'solutionhood':u'Topic-Comment',#TODO
    u'summary':u'Summary',
    u'unless':u'Condition',#TODO
    # SPANISH
    u'alternativa':u'Condition',#TODO
    u'antítesis':u'Contrast',
    u'capacitación':u'Enablement',
    u'causa':u'Cause',
    u'circunstancia':u'Background',
    u'concesión':u'Contrast',
    u'condición':u'Condition',
    u'condición-inversa':u'Condition',#TODO
    u'conjunción':u'Joint',#TODO
    u'contraste':u'Contrast',
    u'disyunción':u'Joint',
    u'elaboración':u'Elaboration',
    u'evaluación':u'Evaluation',
    u'evidencia':u'Explanation',
    u'fondo':u'Background',
    u'interpretación':u'Evaluation',
    u'justificación':u'Explanation',#TODO
    u'lista':u'Joint',
    u'medio':u'Manner-Means',
    u'motivación':u'Explanation',#TODO
    u'preparación':u'Background',#TODO
    u'propósito':u'Enablement',
    u'reformulación':u'Summary',
    u'resultado':u'Cause',
    u'resumen':u'Summary',
    u'same-unit':u'Same-unit',
    u'secuencia':u'Temporal',
    u'solución':u'Topic-Comment',#TODO
    u'unión':u'Joint',#never seen this relation!#TODO
    u'unless':u'Condition',#TODO,
    # DUTCH
    u'antithesis':u'Contrast',
    u'background':u'Background',
    u'circumstance':u'Background',
    u'concession':u'Contrast',
    u'condition':u'Condition',
    u'conjunction':u'Joint',#TODO
    u'contrast':u'Contrast',
    u'disjunction':u'Joint',
    u'elaboration':u'Elaboration',
    u'enablement':u'Enablement',
    u'evaluation':u'Evaluation',
    u'evidence':u'Explanation',
    u'interpretation':u'Evaluation',
    u'joint':u'Joint',
    u'justify':u'Explanation',#TODO
    u'list':u'Joint',
    u'means':u'Manner-Means',
    u'motivation':u'Explanation',#TODO
    u'nonvolitional-cause':u'Cause',
    u'nonvolitional-result':u'Cause',
    u'otherwise':u'Condition',
    u'preparation':u'Background',#TODO
    u'purpose':u'Enablement',
    u'restatement':u'Summary',
    u'restatement-mn':u'Summary',# multinuclear version: we do not need to keep this info at the end
    u'sequence':u'Temporal',
    u'solutionhood':u'Topic-Comment',#TODO
    u'summary':u'Summary',
    u'unconditional':u'Condition',#TODO
    u'unless':u'Condition',#TODO
    u'volitional-cause':u'Cause',
    u'volitional-result':u'Cause',
    # GERMAN
    u'antithesis':u'Contrast',
    u'background':u'Background',
    u'cause':u'Cause',
    u'circumstance':u'Background',
    u'concession':u'Contrast',
    u'condition':u'Condition',
    u'conjunction':u'Joint',#TODO
    u'contrast':u'Contrast',
    u'disjunction':u'Joint',
    u'e-elaboration':u'Elaboration',#Not a RST relation, can be mapped ?
    u'elaboration':u'Elaboration',
    u'enablement':u'Enablement',
    u'evaluation':u'Evaluation',
    u'evidence':u'Explanation',
    u'interpretation':u'Evaluation',
    u'joint':u'Joint',
    u'justify':u'Explanation',#TODO
    u'list':u'Joint',
    u'means':u'Manner-Means',
    u'motivation':u'Explanation',#TODO
    u'otherwise':u'Condition',
    u'preparation':u'Background',#TODO
    u'purpose':u'Enablement',
    u'reason':u'Explanation',#Map to Explanation?
    u'restatement':u'Summary',
    u'result':u'Cause',
    u'sequence':u'Temporal',
    u'solutionhood':u'Topic-Comment',#TODO
    u'summary':u'Summary',
    u'unless':u'Condition',#TODO
        # ENGLISH
    'analogy':'Comparison',
    'antithesis':'Contrast',
    'attribution':'Attribution',
    'attribution-negative':'Attribution',
    'background':'Background',
    'cause':'Cause',
    'cause-result':'Cause',
    'circumstance':'Background',
    'comment':'Evaluation',
    'comment-topic':'Topic-Comment',
    'comparison':'Comparison',
    'concession':'Contrast',
    'conclusion':'Evaluation',
    'condition':'Condition',
    'consequence':'Cause',
    'contingency':'Condition',
    'contrast':'Contrast',
    'definition':'Elaboration',
    'disjunction':'Joint',
    'elaboration-additional':'Elaboration',
    'elaboration-general-specific':'Elaboration',
    'elaboration-object-attribute':'Elaboration',
    'elaboration-part-whole':'Elaboration',
    'elaboration-process-step':'Elaboration',
    'elaboration-set-member':'Elaboration',
    'enablement':'Enablement',
    'evaluation':'Evaluation',
    'evidence':'Explanation',
    'example':'Elaboration',
    'explanation-argumentative':'Explanation',
    'hypothetical':'Condition',
    'interpretation':'Evaluation',
    'inverted-sequence':'Temporal',
    'list':'Joint',
    'manner':'Manner-Means',
    'means':'Manner-Means',
    'otherwise':'Condition',
    'preference':'Comparison',
    'problem-solution':'Topic-Comment',
    'proportion':'Comparison',
    'purpose':'Enablement',
    'question-answer':'Topic-Comment',
    'reason':'Explanation',
    'restatement':'Summary',
    'result':'Cause',
    'rhetorical-question':'Topic-Comment',
    'same-unit':'Same-unit',
    'sequence':'Temporal',
    'statement-response':'Topic-Comment',
    'summary':'Summary',
    'temporal-after':'Temporal',
    'temporal-before':'Temporal',
    'temporal-same-time':'Temporal',
    'textual-organization':'Textual-organization',
    'textualorganization':'Textual-organization',
    'topic-comment':'Topic-Comment',
    'topic-drift':'Topic-Change',
    'topic-shift':'Topic-Change'

}



def main( ):
    parser = argparse.ArgumentParser(
            description='Discourse corpus.')
    parser.add_argument('--corpus',
            dest='corpus',
            action='store',
            help='Input file/directory to read (bracketed and edus files)')
#     parser.add_argument('--corpus',
#             dest='corpus',
#             action='store',
#             help='Input file/directory to read (RST .dis files, RST .rs3 files)')
#     parser.add_argument('--treebank',
#             dest='treebank',
#             action='store',
#             default="/Users/chloe/navi/discourse/data/rstdt_samples/",
#             help='Input file/directory to read (RST .dis files, RST .rs3 files)')
    args = parser.parse_args()

    _compute_stuff( vars(args)['corpus'] )

#     compute_stuff( vars(args)['corpus'], vars(args)['treebank'] )


def compute_stuff( corpus ):
#
#     mapp = {}
#     mapp = {d:v for (d,v) in mapping.items() if not d in mapp}
#     print( mapp )
#     print( len( mapp.keys() ) )
#     print( sorted(list(set(mapp.values()))), '\n', len( set(mapp.values()) ) )
#
#     print( "\n", sorted(list(set(rstdt_mapping18.values()))), '\n', len( set(rstdt_mapping18.values()) ) )
#
#
#     original_relations = set(mapp.keys())
#     print( len(original_relations) )
#
#     for r in sorted( list( original_relations ) ):
#         print( 'u\''+r+'\':'+'\''+mapp[r]+'\',' )
#
    data2relset = {}
    for _dir in os.listdir( corpus ):
        data = os.path.join( corpus, _dir, "dmrg" )
        if os.path.isdir( data ):
#             print( "\nREADING:", data )
            relcount = _compute_stuff( data )
            data2relset[data] = relcount
#
    studyRelSet( data2relset )



def _compute_stuff( data ):
    print( "\n% READING:", data )
    edu_files = get_files( data, ext='.edus' )
    tree_files = get_files( data, ext='.bracketed' )
    print( '\#DOCUMENTS', len( tree_files ) )
    edu_files = [os.path.join( data, f ) for f in os.listdir( data ) if f.endswith('.edus' )]
    tree_files = [os.path.join( data, f ) for f in os.listdir( data ) if f.endswith('.bracketed' )]

    relationCount = countRelation( tree_files )
    print( "\#Relations:", len( relationCount.keys() ) )
    for r in sorted(relationCount):
        print( "u\'"+r+"\'"+":"+"u\'"+r+"\'"+"," )
    printRelationNuc( relationCount )

#     for r in rstdt_mapping18.keys():
#         if not r in relationCount:
#             print( "REL", r )

    labelCount = countLabel( tree_files )#Relation+nuc
    print( "\#Labels:", len( labelCount.keys() ) )

    # COUNT ANNOTATIONS AND EDU AND WORDS
    count_annot, doc2count = countRelationAnnotated( tree_files )
    print( "\#Relations annotated", count_annot )
#     print( [d for d in doc2count.keys() if doc2count[d] == max( doc2count.values() )] )
    print( "Max/Min/avg", max( doc2count.values() ),'/', min( doc2count.values() ),'/', np.mean(  list(doc2count.values() )) )
    print( "Mean per doc", count_annot/( len( tree_files )+0 ) )
#
#     printRelation( relationCount )
#
    count_edu, edus, doc2edu = count_number_edu( edu_files, tree_files )
    print( "\#EDUS:", count_edu )
    print( "Max/min/Avg", str( max(doc2edu.values()) )+'/'+str(min(doc2edu.values()))+'/'+str( round( np.mean(list(doc2edu.values())),1 ) ) )
#
    words = count_word( edus )
    print( "\#Words:", len( words ), len( set( words ) ) )
    return relationCount





def studyRelSet( data2relset ):
    common_rel = []

    all_rel = set( [r for d in data2relset for r in data2relset[d]] )
    print( "Total:", len( all_rel ) )

    # - Common to all relation sets
    common = findCommonSet( data2relset, all_rel )
    print( "\nCommon:", len(common) )
#     print( " ".join( ["\\rel{"+r+"}," for r in common ] ) )
#     for r in sorted( common ):
#         print( r )

    # - Only in one corpus
    diff_sp = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/sp-rst/dmrg' )
#     print( "\nOnly Spanish", len( diff_sp ), diff_sp )
    diff_ba = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/ba-rstdt/dmrg' )
#     print( "\nOnly Basque", len( diff_ba ), diff_ba )
    diff_br = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/br-cst+summit/dmrg' )
#     print( "\nOnly Brazilian", len( diff_br ), diff_br )
    diff_du = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/du-mto/dmrg' )
#     print( "\nOnly Dutch", len( diff_du ), diff_du )
    diff_en = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/en-rstdt/dmrg' )
#     print( "\nOnly English", len( diff_en ) )
    diff_ge = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/ge-pcc/dmrg' )
#     print( "\nOnly German", len( diff_ge ), diff_ge )

    all_dif = [r for r in diff_sp]
    all_dif.extend( diff_ba )
    all_dif.extend( diff_br )
    all_dif.extend( diff_du )
    all_dif.extend( diff_ge )

    print( "\nRelations in only 1 corpus:", "en-rstdt:", len( diff_en ), " ; others:", len( all_dif ) )
    for r in all_dif:
        print( r )



    print( "\nCommon+diff", len(common)+len( diff_en )+len( all_dif ) )

    # Reste les relations qui sont dans qques corpus mais pas tous:

    rem = []
    for r in all_rel:
        if not r in common and not r in diff_en and not r in all_dif:
            rem.append( r )
    print( "\nRelations existing in several corpora", len( rem ) )
    for r in rem:
        print( r )

    # among these remaining relation, how many are not in the RST DT
    notEng = []
    for r in rem:
        if not r in data2relset['data/discourse-rst/multilingualCorpora/en-rstdt/dmrg']:
            notEng.append( r )
    print( "\nRemaining relations not in the Eng RST DT", len( notEng ) )
    for r in notEng:
        print( r )

    print( "\noverall", len(common)+len( diff_en )+len( all_dif )+len( rem ) )


#     eng_relset = {k:0 for k in rstdt_mapping18.keys()}
# #     for r in sorted( rstdt_mapping18):
# #         print( r+"\t&\t"+rstdt_mapping18[r]+'\\\\' )
#     data2relset["b/b/orig-eng"] = eng_relset


#
#     print( "\nDifferences" )
#     for r in sorted(all_rel):
#         if not r in common and not r in data2relset['data/discourse-rst/multilingualCorpora/en-rstdt/dmrg']:
#             print( r )

#     # compute number of rel only in the RST DT
#     diff = compareDiff( data2relset,'data/discourse-rst/multilingualCorpora/sp-rst/dmrg' )
#     print( "\nRel only in the sp", len( diff ), diff )


#     comparison = {}
#     for data1 in sorted( data2relset.keys() ):
#         for data2 in sorted( data2relset.keys() ):
# #             print( data1, data2 )
#             d1 = data1.split('/')[-2].replace('_', '\_')
#             d2 = data2.split('/')[-2].replace('_', '\_')
# #             print( "DATA 1", d1, "DATA 2", d2 )
#             common = compareRelSet( data2relset[data1], data2relset[data2] )
# #             print( len( common ) )
#             if d1 in comparison:
#                 comparison[d1][d2] = len( common )
#             else:
#                 comparison[d1] = {d2:len( common )}
# #
#     datas = sorted( comparison.keys() )
#     print( "\t&"+"\t&\t".join( datas )+'\\\\' )
#     for d1 in datas:
#         l = d1
#         for d2 in datas:
#             l += "\t&\t"+str(comparison[d1][d2])+"\t"
# #             print( d1, d2, comparison[d1][d2] )
#         print( l+'\\\\' )

def findCommonSet( data2relset, all_rel ):
    common = []
    for r in all_rel:
        if iscommon( data2relset, r ):
            common.append( r )
    return common


def iscommon( data2relset, r ):
    for d in data2relset:
#         if not 'en-rstdt' in d:
        if not r in data2relset[d]:
            return False
    return True


def compareDiff( data2relset, excluded ):
    diff = []
#     print( [d for d in data2relset] )
    allrel = [r for d in data2relset for r in data2relset[d] if d != excluded]
    for r in data2relset[excluded]:
        if not r in allrel:
            diff.append(r )
    return diff

def compareRelSet( relSet1, relSet2 ):
    common = []
    for r in relSet1:
#         print( r )
        if r in relSet2:
#             print( "\tok" )
            common.append( r.lower() )
    return common


def count_word( edus ):
    words = []
    for e in edus:
        tokens = TOKENIZER.tokenize( e )
        words.extend( tokens )
    return words

def count_number_edu( edu_files, tree_files ):
    count1, edus, doc2edu = count_eduf( edu_files )
    count2 = count_edut( tree_files )
#     if count1 != count2:
#         sys.exit( "From edu files:"+str(count1)+" ; From tree files:"+str(count2 ))
    return count1, edus, doc2edu

def count_eduf( edu_files ):
    count_edu = 0
    doc2edu = {}
    edus = []
    for f in edu_files:
        with open( f, encoding='utf8' ) as myfile:
            lines = [l for l in myfile.readlines() if l.strip() != '']
            edus.extend( lines )
            count_edu += len( lines )
            doc2edu[f] = len( lines )

    return count_edu, edus, doc2edu

def count_edut( tree_files ):
    count_edu = 0
    for f in tree_files:
        with open( f, encoding='utf8' ) as myfile:
            tree = Tree.fromstring( myfile.read() )
            queue = [tree]#seems to happen in the spanish RST DT
            while queue:
                n= queue.pop(0)
                label = n.label()
                if n.label() == "EDU":
                    count_edu += 1
                else:
                    queue.extend( [m for m in n] )
    return count_edu

def countLabel( tree_files ):
    labelCount = {}
    for f in tree_files:
        with open( f, encoding='utf8' ) as _f:
            tree = Tree.fromstring( _f.read() )
            queue = [tree]#seems to happen in the spanish RST DT
            while queue:
                n= queue.pop(0)
                label = n.label()
                if n.label() != "EDU":
                    rel = label
                    if rel.endswith( '-n-e' ) or rel.endswith( '-s-e' ):
                        rel = rel[:-4]
                    if rel.endswith( '-e' ) or rel.endswith( '-n' ) or rel.endswith( '-s' ):
#                         print( "REL CHANGED", rel )
                        rel = rel[:-2]
                    label = rel.lower()

                    if label in labelCount:

                        labelCount[label] += 1
                    else:
                        labelCount[label] = 1
                    queue.extend( [m for m in n] )
    return labelCount

def countRelation( tree_files ):
    relationCount = {}
    for f in tree_files:
        with open( f, encoding='utf8' ) as _f:
            tree = Tree.fromstring( _f.read() )
            queue = [tree]#seems to happen in the spanish RST DT
            while queue:
                n= queue.pop(0)
                label = n.label()
                if n.label() != "EDU":
                    rel = label
                    if label[2] == '-':
                        l = label.split('-')
                        nuc = l[0]
                        rel = '-'.join( l[1:] )
#                     else:
#                         sys.exit( "No nuclearity indication?" )
                    temp_rel = rel
                    if rel.endswith( '-n-e' ) or rel.endswith( '-s-e' ):
                        rel = rel[:-4]
                    if rel.endswith( '-e' ) or rel.endswith( '-n' ) or rel.endswith( '-s' ):
#                         print( "REL CHANGED", rel )
                        rel = rel[:-2]
                    rel = rel.lower()
#                         print( "\t", rel )
#                     if rel in relationCount:
#                         relationCount[rel] += 1
#                     else:
#                         relationCount[rel] = 1
                    if rel in relationCount:
                        if nuc in relationCount[rel]:
                            relationCount[rel][nuc] += 1
                        else:
                            relationCount[rel][nuc] = 1
                    else:
                        relationCount[rel] = {nuc:1}
                    queue.extend( [m for m in n] )
#     print( relationCount.keys() )
    return relationCount

def countRelationAnnotated( tree_files ):
    count = 0
    doc2count = {}
    for f in tree_files:
        c = 0
        with open( f, encoding='utf8' ) as _f:
            tree = Tree.fromstring( _f.read() )
            queue = [tree]#seems to happen in the spanish RST DT
            while queue:
                n= queue.pop(0)
                label = n.label()
                if n.label() != "EDU":
#                     if 'enablement' in label:
#                         print( f )
#                         Tree.draw( tree )
                    count += 1
                    c += 1
                    queue.extend( [m for m in n] )
        doc2count[f] = c
    return count, doc2count


def printRelationNuc( relationCount ):
    all_nuc = sorted( list( set( [n for r in relationCount for n in relationCount[r]] ) ) )
    print( "\n%# Relations:", len( relationCount ) )

    header = ["Relation"]
    header.extend( all_nuc )
    header.append( "Total" )

    table = "\\begin{table}\n\\begin{tabular}{"+"l"*len(header)+"}\n\\toprule\n"
    table += "\t&\t".join( header )+"\t\\\\\n\\midrule\n"

    for relation in sorted( relationCount ):
#         print( relation )
        table += relation
        for nuc in all_nuc: #sorted( relationCount[relation] ):
            if nuc in relationCount[relation]:
                table += "\t&\t"+str(relationCount[relation][nuc])
            else:
                table += "\t&\t0"
#         print( sum( [ relationCount[relation][n] for n in relationCount[relation]] ) )
        table += "\t&\t"+str(sum( [ relationCount[relation][n] for n in relationCount[relation]] ))+"\t\\\\\n"
    table += "\\bottomrule\n\\end{tabular}\n\\caption{Relation counts.}\n\\end{table}"
    print( "\n", table, "\n" )
#             print( "\t", nuc, relationCount[relation][nuc] )
#             print( sum( [ relationCount[relation][n] for n in relationCount[relation]] ) )

def printRelation( relationCount ):
    print( "\n# Relations:", len( relationCount ) )
    for relation in sorted( relationCount ):
        print( relation, sum( [ relationCount[relation][n] for n in relationCount[relation]] )  )


def get_files( tbpath, ext='.bracketed' ):
    ext_files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                ext_files.append( os.path.join( p, file ) )
    return ext_files


#
# def compute_stuff( corpus, treebank ):
#     #spanish wtf test
# #     rs3files_a = getRS3Files( corpus, ext='a.rs3' )
# #     rs3files_b = getRS3Files( corpus, ext='b.rs3' )
# #
# #     print( "#Documents A", len( rs3files_a ) )
# #     print( "#Documents B", len( rs3files_b ) )
# #
# #     fa = [os.path.basename(f).split('.')[0][:-1] for f in rs3files_a]
# #     fb = [os.path.basename(f).split('.')[0][:-1] for f in rs3files_b]
# #
# #     for f in fa:
# #         if not f in fb:
# #             print( f )
#
#     rs3files = getRS3Files( corpus, ext='.dis' )
#     print( "#Documents", len( rs3files ) )
#
#     bracketedfiles = getRS3Files( treebank, ext='.bracketed' )
#     print( "#Trees", len( bracketedfiles ) )
#
#     computePerRel( treebank, ext='.bracketed' ) # english .map18
#
# def computePerRel( treebank, ext='.bracketed' ):
#     label2count = {}
#     total = 0.
#     total_edu = 0.
# #     for d in ["train", "test", "dev"]:#for english rst dt
# #         _treebank = os.path.join( treebank, d )
#     for f in [f_ for f_ in os.listdir( treebank ) if f_.endswith( ext )]:
#
#         #tree = Tree.fromstring( open( os.path.join( treebank, f ) ).read() )
#
#         c = open( os.path.join( treebank, f ) )
#         lines = c.readlines()
#         for l in lines:
#
#             l = l.strip()
#             tree = Tree.fromstring( l )
#
#             for st in tree.subtrees():
#                 if st.label().lower() == 'edu':
#                     total_edu += 1.0
#                 else:
#                     if st.label() != 'S':
#                         relation = st.label()[2:]
#                         print( relation, st.label() )
#                         #relation = '-'.join( st.label().split('-')[1:] ) #0 is the nuclearity
#                         if relation in label2count:
#                             label2count[relation] += 1.0
#                         else:
#                             label2count[relation] = 1.0
#                         total += 1.0
#     print( "\n#EDUs", total_edu )
#     print( "#Relations", total, "\n" )
#     print( "#Relation Set", len(label2count), "\n" )
#     rel = []
#     count = []
#     for r,c in sorted( label2count.items(), lambda x,y:cmp(x[0],y[0] ) ):
#         print( '\t&\t'.join( [r, str(c), str( round( (c*100)/total, 2) ) ])+'\\\\' )
#         rel.append( r )
#         count.append( c )
#     print( rel )
#     print( count )
#
# def getRS3Files( tbpath, ext=".rs3" ):
#     rs3files = []
#     for p, dirs, files in os.walk( tbpath ):
#         subdir = os.path.basename(p)
# #         print( "Reading subdir: ", p, file=sys.stderr )
#         dirs[:] = [d for d in dirs if not d[0] == '.']
#         for file in files:
#             if not file.startswith('.') and file.endswith(ext) and not 'ANNOTATOR_B' in file:
#
#                 rs3files.append( os.path.join( p, file ) )
#     return rs3files




if __name__ == '__main__':
    main()

