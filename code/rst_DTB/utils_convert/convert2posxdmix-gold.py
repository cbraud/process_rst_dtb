#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Generate he word+pos Mix format from the gold data for Pos and Word
'''


import argparse, os, sys


def main( ):
    parser = argparse.ArgumentParser(
            description='Generate he word+pos Mix format from the gold data for Pos and Word.')
    parser.add_argument('--word',
            dest='word',
            action='store',
            help='Word data in seg format (gold tokenisation)')
    parser.add_argument('--pos',
            dest='pos',
            action='store',
            help='Pos data in seg format (gold tokenisation and postagging)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for the segmenter')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )

    convert( args.word, args.pos, args.outpath )


def convert( word, pos, outpath ):
    for dset in [d for d in os.listdir( word ) if not d.startswith( '.' )]:
        if not os.path.isdir( os.path.join( outpath, dset ) ):
            os.mkdir(  os.path.join( outpath, dset ) )
        for _file in [f for f in os.listdir( os.path.join( word, dset ) ) if not f.startswith( '.')]:
            pos_file = _find_pos_file( pos, dset, _file )
            if pos_file == None:
                sys.exit( "Pos file not found for word file: "+_file )
            outfile = open( os.path.join( outpath, dset, _file ), 'w' )
            with open( os.path.join( word, dset, _file ) ) as wf:
                with open( os.path.join( pos, dset, pos_file ) ) as pf:
                    pf_lines = pf.readlines()
                    for i,l in enumerate( wf.readlines() ):
                        outfile.write( l.strip()+"\n" )
                        outfile.write( pf_lines[i].strip().split('\t')[0].strip()+'\t'+'I\n' )
            outfile.close()


def _find_pos_file( pos, dset, wdfile ):
    if not os.path.isdir( os.path.join( pos, dset ) ):
        sys.exit( "dataset not found: "+os.path.join( pos, dset ) )
    for _file in [f for f in os.listdir( os.path.join( pos, dset ) ) if not f.startswith( '.')]:
        if _file == wdfile:
            return _file
    return None


if __name__ == '__main__':
    main()
