#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Adding EDU info to conll files

When using PTB pos tags obtained with the Charniak Johnwon parser, use first check-ptb-parse-outputs.py to change the will --> wo and can n't --> can 't

'''

# TODO: redo the parse of the files using directly the .brackets files
# TODO redo the parse file without the previously done tokenization

from __future__ import print_function
import argparse, os, sys, shutil, codecs, numpy, collections
from nltk import Tree
import numpy as np


def main( ):
    parser = argparse.ArgumentParser(
            description='Add pos and parse info.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Input directory (contains .edu files).')
    parser.add_argument('--parse',
            dest='parse',
            action='store',
            help='Directory containing parsing info for the data.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory (same as in the input if not given).')
    parser.add_argument('--split',
            dest='split',
            action='store',
            help='List of files to make train/dev/test')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.treebank, args.outpath, args.parse, args.split )



# Write 1 file per input file
def convert( treebank, outpath, parse, split_files ):
    file2set, set2files = getSplit( split_files )
    eduFiles = getFiles( treebank, ext=".edus" )

    for dset in set2files:
        if not os.path.isdir( os.path.join( outpath, dset ) ):
            os.mkdir( os.path.join( outpath, dset ) )

        for i,bf in enumerate( set2files[dset] ):
            print( "Reading", bf )
            outFile = os.path.join( outpath, dset, bf+".parse" )
            ef = findFile( bf, eduFiles )
            if ef == None:
                print( "File not found: "+bf )
                continue
            pf = os.path.join( parse, dset, bf+'.conllu' )
            if not os.path.isfile( pf ):
                pf = os.path.join( parse, dset, bf+'.out.conllu' )
            if not os.path.isfile( pf ):
                pf = os.path.join( parse, dset, bf+'.bllip.conllu' )
            if not os.path.isfile( pf ):
                pf = os.path.join( parse, bf+'.bllip.conllu' )
            if not os.path.isfile( pf ):
                pf = os.path.join( parse, bf.replace( '.txt.xml', '.txt.conllu' ) )
            if not os.path.isfile( pf ):
                print( "Parsed file not found", bf )
                continue
            feats = readConllu( pf )

            edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]
            edu_tokens = retrieve_edu( edus, feats, bf )

            # One file per document, keep i = 0
            write_edus_ud( edu_tokens, outFile, 0 )

# Write 1 file per test/dev/train set
def convertDset( treebank, outpath, parse, split_files ):
    file2set, set2files = getSplit( split_files )

    for f in file2set.values():
        c = open( os.path.join( outpath, f+".parse" ), 'w' )# TODO rm
        c.close()

    writeFilesParse( treebank, outpath, parse, file2set, set2files )

def writeFilesParse( treebank, outpath, parse, file2set, set2files ):
    treeFiles = getFiles( treebank, ext=".bracketed" )
    eduFiles = getFiles( treebank, ext=".edus" )

    for dset in set2files:
        outFile = os.path.join( outpath, dset+".parse" )

        print( "Writing", outFile )

        for i,bf in enumerate( set2files[dset] ):
            print( "Reading", bf )
#             if bf == 'li00042':#'as00001':
            tf = findFile( bf, treeFiles )
            ef = findFile( bf, eduFiles )
            if ef == None:
                print( "File not found: "+bf )
                continue
            ##if tf == None:
            ##    print( "File not found: "+bf )
            ##    continue

            pf = os.path.join( parse, dset, bf+'.conllu' )
            feats = readConllu( pf )

            edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]
            edu_tokens = retrieve_edu( edus, feats, bf )
#                 sys.exit()

            # One file per document, keep i = 0
            write_edus_ud( edu_tokens, outFile, 0 )

def retrieve_edu( edus, feats, bf ):
    '''
    Asociate each EDU with a list of tokens from the parse
    '''
    edus = [e.split( ) for e in edus]
    edus_tokens = [] #final list
    all_tokens = get_all_tokens( feats ) #the tokens in the doc according to the parser
    pos_token = 0 #position of the current token in all_tokens_doc
    pos_char = 0
    indx_edu = 0
    e = edus[indx_edu]
    #for e in edus:
    correcte = correctEdu( e )
    cur_edu = ''.join( [c for c in correcte] ).lower()
    pos_char_edu = 0
    edu_tokens = []
    temp_edu = ''
    pb = False
    while pos_token < len( all_tokens ):
        curt = all_tokens[pos_token][0].lower()
        #print( '\n', bf )
        #print( '-> CURT', curt, curt[pos_char], pos_char )
        #print( ">", cur_edu )
        #print( '-> CURE', cur_edu[pos_char_edu], pos_char_edu )
        curc = curt[pos_char]

        if cur_edu[pos_char_edu] == curt[pos_char]:
#             print( '>' )
            temp_edu += curt[pos_char]
            pos_char += 1
        elif curt[pos_char] == "\"" and cur_edu[pos_char_edu] == "`" and pos_char_edu+1 != len( cur_edu ) and cur_edu[pos_char_edu+1] == "`":
#             print( '> pb guillemet' )
            temp_edu += curt[pos_char]
            pos_char += 1
            pb = True
        elif curt[pos_char] == "\"" and cur_edu[pos_char_edu] == "\'" and pos_char_edu+1 != len( cur_edu ) and cur_edu[pos_char_edu+1] == "\'":
#             print( '> pb guillemet' )
            temp_edu += curt[pos_char]
            pos_char += 1
            pb = True
        elif curt[pos_char] == "\'" and cur_edu[pos_char_edu] == "`":# just a per token difference, but depend on the data so hard to make a correction a priori
            temp_edu += curt[pos_char]
            pos_char += 1

        #elif curt[pos_char] == 'i' and curt == 'will' and cur_edu[pos_char_edu] == 'o' and pos_char_edu>1 and cur_edu[pos_char_edu-1] == 'w':
        #    print( "Will/wo problem" )
        #    all_tokens[pos_token][0] = "wo"
        #    curt = "wo"
        #    curc = curt[pos_char]
        #    print( all_tokens[pos_token][0], pos_char, len(curt ) )
        #    pos_char += 1
        else:
            print( "PB, unrecognized character, EDU begins with:", cur_edu[pos_char_edu], "\nFull EDU:",cur_edu )
            print( "Cur caracter:", curt[pos_char], ' -- cur token:', curt )
            sys.exit()

        if pos_char == len(curt):
#             print( "OK", curt)
            edu_tokens.append( all_tokens[pos_token] )
            pos_token += 1
            pos_char_edu += 1
            pos_char = 0
            if pb:
                pos_char_edu += 1
                pb = False
        else:
            pos_char_edu += 1
            if pb:
                pos_char_edu += 1
                pb = False
#             print( cur_edu )
#             print( temp_edu )
        if temp_edu == cur_edu or pos_char_edu == len( cur_edu ):
#             print( "End EDU", pos_char_edu, len( cur_edu ) )
            edus_tokens.append( edu_tokens )

#             print( "\nCUREDU", ' '.join( edus[indx_edu] ) )
# #             print( "TEMPEDU", temp_edu )
#             print( "TEMPEDU", ' '.join( [t[0] for t in edu_tokens] ) )

            if indx_edu == len( edus )-1:
                break

            edu_tokens = []
            indx_edu += 1
            e = edus[indx_edu]
            correcte = correctEdu( e )
            cur_edu = ''.join( [c for c in correcte] ).lower()
            pos_char_edu = 0
            edu_tokens = []
            temp_edu = ''
#             print( pos_token, all_tokens )


    if len( edus_tokens ) != len( edus ):
        sys.exit( "Not all EDUs found: found "+str(len( edus_tokens ))+" out of "+str(len( edus ) ) )

    return edus_tokens


def readConllu( pf ):
    '''
    The parse includes lines for composed tokens
    '''
    feats = {} #token 2 info
    sent = 0
    feats[sent] = {}
    keep_form = None
    ids = []
    with open( pf, encoding='utf8' ) as myfile:
        lines = myfile.readlines()
        i = 0
        while i < len( lines ):
            l = lines[i].strip()
#             print( i, len(lines) )#, '\''+lines[i]+'\'', i == len( lines )-1, l == '\n' )
            if l == '' or l == '\n':
                if i == len( lines )-1:
                    break
                sent += 1
                feats[sent] = {}
            else:
                #1	A	a	ADP	prp|<sam->	AdpType=Prep	5	case	_	SpaceAfter=No
                id_tok, form, lemma, pos, xpos, morph, head, deprel, deps, misc = l.split('\t')
                #print( id_tok, form )
                form, lemma = correct_form_ud( form, lemma )

                if '-' in id_tok:#strange case in German, see test/maz-18160.conll and Spanish, composed tokens
                    ids = [str(i) for i in range( int(id_tok.split('-')[0]), int(id_tok.split('-')[-1])+1 )]
#                     print( "TO IGNORE", ids )
                    keep_form = form
                    i += 1
                    continue

                if len( ids ) != 0:
                    if id_tok == ids[0] and keep_form != None:#keep_form != None:
#                         print( "COMPOSED", form, ids )
                        form = keep_form
                        keep_form = None
                        ids.pop(0)

#                         print( "FIRST RM", ids, form, lemma, pos, xpos, morph, head, deprel, deps, misc )
                    else:
                        # ignored the token
#                         print( "IGNORED", form, ids )
                        ids.pop(0)
                        i += 1
                        continue

                if form == None:
                    print( "No form found, dealing with file ", pf, "line", i )
                    sys.exit()
#                 print( ">> form added", form )

                feats[sent][int(id_tok)] = [form, lemma, pos, xpos, morph, head, deprel, deps, misc, sent, int(id_tok)]

            i += 1
    correctId( feats )
    return feats


def correctId( feats ):
    '''
    Possibly non following ID, ie if we have:
    20-21 del
    20 de
    21 el
    We keep "del" as id 20 and ignore 21
    '''

    for sent in feats:
        r = list( range( 1, len( feats[sent] )+1 ) )
        ids = feats[sent].keys()
        if r != ids:
            new_sent = {}
            for u,i in enumerate( sorted( ids ) ):
#                 print( i, u)
                if i != r[u]:
                    form, lemma, pos, xpos, morph, head, deprel, deps, misc, sent, id_tok = feats[sent][i]
                    new_sent[r[u]] = [form, lemma, pos, xpos, morph, head, deprel, deps, misc, sent, r[u]]
                else:
                    new_sent[r[u]] = feats[sent][i]
            feats[sent] = new_sent

def correctEdu( e ):
    correcte = []
    for t in e:
        newt = ''
        for form in t:
            if form == '(':
                form = '-LRB-'
            elif form == ')':
                form = '-RRB-'
            elif form == '[':
                form = '-LSB-'
            elif form == ']':
                form = '-RSB-'
            elif form == '{':
                form = '-LCB-'
            elif form == '}':
                form = '-RCB-'
            newt +=  form
        correcte.append( newt )
    if len(e) != len(correcte):
        print( "Pb when correcting en EDU:" )
        print( "Original EDU:", ' '.join( e ) )
        print( "Corrected EDU:", ' '.join(correcte ) )
        sys.exit()
    return correcte

def correct_form_ud( form, lemma ):
    form = form.replace( u'\xa0', u'' )
    lemma = lemma.replace( u'\xa0', u'' )
    if form == u'``':
        form = u'"'
        lemma = u'"'
    if form == u'\'\'':
        form = u'"'
        lemma = u'"'
    # only useful for some data, can break the code for others ..
    #if form == u'---':
    #    form = u'--'
    #    lemma = u'--'
    if form == u'`':
        form = u'\''
        lemma = u'\''
    if '`' in form:
        form = form.replace(u'`',u'\'')
        lemma = lemma.replace(u'`',u'\'')
    if form == '(':
        form = '-LRB-'
    if form == ')':
        form = '-RRB-'
    if form == '[':
        form = '-LSB-'
    if form == ']':
        form = '-RSB-'
    if form == '{':
        form = '-LCB-'
    if form == '}':
        form = '-RCB-'
    if form.endswith( '}' ):
        form = form[:-1]+'-RCB-' #tokenisation error..
    if form.endswith( ']' ):
        form = form[:-1]+'-RSB-' #tokenisation error..
    if form.endswith( ')' ):
        form = form[:-1]+'-RRB-' #tokenisation error..
    if form.startswith( '(' ):
        form = '-LRB-'+form[1:] #tokenisation error..
    if form.startswith( ')' ):
        form = '-RRB-'+form[1:] #tokenisation error..
    if form.endswith( '(' ):
        form = form[:-1]+'-LRB-' #tokenisation error..
    if form.startswith( '[' ):
        form = '-LSB-'+form[1:] #tokenisation error..
    if form.startswith( ']' ):
        form = '-RSB-'+form[1:] #tokenisation error..
    return form, lemma


def get_all_tokens( feats ):
    '''
    Return an ordered list of the tokens in the document described in feats
    list = [ [ form, infos ], ... ]
    '''
    all_tokens_doc = []
#     for sent_id in sorted( feats.keys(), lambda x,y:cmp(x,y) ):
    for sent_id in sorted( feats.keys() ):
#         for tok_id in sorted( feats[sent_id].keys(), lambda x,y:cmp(x,y) ):
        for tok_id in sorted( feats[sent_id].keys() ):
#             print( sent_id, tok_id, feats[sent_id][tok_id] )
            all_tokens_doc.append( [feats[sent_id][tok_id][0].lower(), feats[sent_id][tok_id]] )
    return all_tokens_doc



def write_edus_ud( edus, outfile, doc ):
    ud_cats = ["doc", "sent", "edu", "tok", "form", "lemma", "pos", "head", "fct", "morph", "xpos"]
    f = open( outfile, 'a', encoding='utf8' )
    if doc == 0:
        f.write( "\t".join( ud_cats )+"\n" )
        print( "Writing CATS" )
    else:
        f.write( '\n' )
    lenght = []

    for i,edu in enumerate( edus ):#EDU ids begin at 1
        lenght.append( len( edu ) )
        for w,(form, lemma, pos, xpos, morph, head, deprel, deps, misc, sent, id_tok) in edu:
            f.write( "\t".join( [str(doc), str(sent), str(i+1), str(id_tok), str(form), str(lemma), str(pos), str(head), str(deprel), str(morph), str(xpos)] )+"\n" )
#     f.write( "\n" )
    f.close()
#     print( "Lenght of the EDU:", np.mean( lenght ), max( lenght ), min( lenght ), file=sys.stderr )





# def get_parse_ud( indata, parse, outdata ):
#     '''
#     Associate each EDU with info on its tokens from the UD parses using udpipe
#     '''
#     for dset in ['train', 'dev', 'test']:
#         if os.path.isfile( os.path.join( indata, dset+'.brackets' ) ):
#             print( "\nReading", dset, file=sys.stderr )
#             feats, files = get_info_du( parse, dset )
#             #print( feats )
#             _f = os.path.join( indata, dset+'.brackets' )
#     #         print( "\tReading", _f, file=sys.stderr )
#             doc = 0
#             doc2edus = {}
#     #         with codecs.open( _f, 'r', encoding='utf8' ) as myfile:
#             with open( _f, 'r', encoding='utf8' ) as myfile:
#                 lines = myfile.readlines()
#                 print( "# of doc to read", len( lines ), file=sys.stderr )
#                 for l in lines:#1 line = 1 doc
#     #                 print( "\tReading doc", doc, os.path.basename(files[doc]), file=sys.stderr )
#                     l = l.strip()
#                     #print( doc )
#                     t = Tree.fromstring( l )
#                     leaves = t.leaves()
#                     edu_tokens = retrieve_edu( leaves, feats[doc] )
#                     doc2edus[doc] = edu_tokens
#                     #print( ['\n'+str(i)+' '+t for i,t in enumerate( leaves )] )
#                     doc += 1
#             write_edus_ud( doc2edus, dset, outdata )
#         else:
#             print( 'No file', os.path.join( indata, dset+'.brackets' ) )
#
#
#
# def get_parse( indata, parse, outdata ):
#     '''
#     Associate each EDU with info on its tokens from the Stanford dependency parse
#     '''
#     for dset in ['train', 'dev', 'test']:
#         print( "\nReading", dset, file=sys.stderr )
#         feats, files = get_info( parse, dset )
#         #print( feats )
#         _f = os.path.join( indata, dset+'.brackets' )
# #         print( "\tReading", _f, file=sys.stderr )
#         doc = 0
#         doc2edus = {}
# #         with codecs.open( _f, 'r', encoding='utf8' ) as myfile:
#         with open( _f, 'r', encoding='utf8' ) as myfile:
#             lines = myfile.readlines()
#             print( "# of doc to read", len( lines ), file=sys.stderr )
#             for l in lines:#1 line = 1 doc
# #                 print( "\tReading doc", doc, os.path.basename(files[doc]), file=sys.stderr )
#                 l = l.strip()
#                 #print( doc )
#                 t = Tree.fromstring( l )
#                 leaves = t.leaves()
#                 edu_tokens = retrieve_edu( leaves, feats[doc] )
#                 doc2edus[doc] = edu_tokens
#                 #print( ['\n'+str(i)+' '+t for i,t in enumerate( leaves )] )
#                 doc += 1
#         write_edus( doc2edus, dset, outdata )

# def write_edus( doc2edus, dset, outdata ):
# #     f = codecs.open( os.path.join( outdata, dset+'.parse' ), 'w', encoding='utf8' )
#     f = open( os.path.join( outdata, dset+'.parse' ), 'w', encoding='utf8' )
#     f.write( "\t".join( ["doc", "sent", "edu", "tok", "form", "lemma", "pos", "head", "fct", "ner"] )+"\n" )
#     lenght = []
#     for doc in sorted( doc2edus.keys() ): #, lambda x,y:cmp(x,y)
#         print( doc )
#         for i,edu in enumerate( doc2edus[doc] ):#EDU ids begin at 1
#             lenght.append( len( edu ) )
#             for w,(form, lemma, pos, ner, head, fct, sent, id_tok) in edu:
#                 f.write( "\t".join( [str(doc), str(sent), str(i+1), str(id_tok), str(form), str(lemma), str(pos), str(head), str(fct), str(ner)] )+"\n" )
#         f.write( "\n" )
#     f.close()
#     print( np.mean( lenght ), max( lenght ), min( lenght ) )
#






















# def correct_edu( e ):
#     '''
#     Get a version without space of the EDU
#     Additional modif to make it similar to the parsed text..
#     '''
#     cur_edu = ''.join( e ).lower()
# #     cur_edu = cur_edu.replace( u'....', u'...' )# points added in the original text.. Required if parse with corenlp but not with UD
#     cur_edu = cur_edu.replace( u'-lrb-', u'(' ).replace( u'-rrb-', u')' )
#     cur_edu = cur_edu.replace( u'-lsb-', u'[' ).replace( u'-rsb-', u']' )
#     cur_edu = cur_edu.replace( u'---', u'--' )
#     cur_edu = cur_edu.replace( u'{', u'-lcb-' ).replace( u'}', u'-rcb-' )
#     cur_edu = cur_edu.replace( u'(', u'-lrb-' ).replace( u')', u'-rrb-' )
#     cur_edu = cur_edu.replace( u'[', u'-lsb-' ).replace( u']', u'-rsb-' )
# #     cur_edu = cur_edu.replace( u'&amp;', u'&' ) #Required if parse with corenlp but not with UD
#     cur_edu = cur_edu.replace( u'`', u'\'' )
#     cur_edu = cur_edu.replace( u'\'\'', u'"' )
#     return cur_edu
#
# def common_check( edus_tokens, all_tokens_doc, pos, cur_edu, e ):
#     # Points added by the parser...
# #     if len( edus_tokens ) >= 1:
# #         print( pos )
# #         print( edus_tokens )
#
#     if len( edus_tokens ) >= 1 and edus_tokens[-1][-1][0].endswith( '.' ) and all_tokens_doc[pos][0] == '.':
#         # add this point to the previous EDU?
#         if edus_tokens[-1][-1][0] != '.':
#             edus_tokens[-1].append( all_tokens_doc[pos] )
#         pos += 1
#
#     if not ''.join( [t[0] for t in all_tokens_doc[pos:]] ).replace(' ', '').startswith( cur_edu ):
#         if ''.join( [t[0] for t in all_tokens_doc[pos:]] ).replace(' ', '').replace('.', '').startswith( cur_edu.replace('.', '') ):
#             print( ">> Point Issue" )
#             if ''.join( [t[0] for t in all_tokens_doc[pos:]] ).replace(' ', '').replace('..', '.').startswith( cur_edu ):
#                 print( "Double point in the parse" )
#                 for i,t in enumerate( all_tokens_doc[pos:] ):
# #                     print( t[0], all_tokens_doc[pos+i+1] )
#                     if t[0].endswith('.') and all_tokens_doc[pos+i+1][0] == '.':#TODO add check on cur edu
# #                             print( '>>', all_tokens_doc[pos+i+1] )
#                         all_tokens_doc.pop( pos+i+1 )
#                         if ''.join( [t[0] for t in all_tokens_doc[pos:]] ).replace(' ', '').startswith( cur_edu ):
#                             break
#
#
#     if not ''.join( [t[0] for t in all_tokens_doc[pos:]] ).replace(' ', '').replace( '\'\'', '"' ).startswith( cur_edu ):
#         print( "\n>> EDU not found at the beginning of the current text" )
# #         print(  ''.join( [t[0] for t in all_tokens_doc[pos:pos+20]] ).replace(' ', '') )
# #         print( "EDU", cur_edu )
# #         print( "EDU",' '.join( e ) )
# #         print( ' '.join( [t[0] for t in all_tokens_doc[pos:pos+20]] ) )
# #         sys.exit()
#     return edus_tokens, all_tokens_doc, pos
#
# def _retrieve_edu( edus, feats ):
#     '''
#     Asociate each EDU with a list of tokens from the parse
#     '''
# #     edus = [e.split('__') for e in edus] #split the tokens
#     edus = [e.split( ) for e in edus]
#     edus_tokens = [] #final list
#     all_tokens_doc = get_all_tokens( feats ) #the tokens in the doc according to the parser
#     pos = 0 #position of the current token in all_tokens_doc
#     check_next = False
#
#     for i,e in enumerate(edus):
#         cur_edu = correct_edu( ''.join( e ).lower() )
#         edus_tokens, all_tokens_doc, pos = common_check( edus_tokens, all_tokens_doc, pos, cur_edu, e )
#         temp_edu = ''
#         cur_pos = pos
#         edu_pos = 0
#         ignored = None
#         ignored_tokens = []
#         for t in all_tokens_doc[pos:]:
# #             print( "\n", cur_edu )
# #             print( temp_edu )
# #             print( all_tokens_doc[pos] )
# #             if pos+1 < len( all_tokens_doc ):
# #                 all_tokens_doc[pos+1]
#             cur_token = t[0]
#
# #             print( "\n", cur_edu )
# #             print( temp_edu+cur_token )
# #             if not cur_edu.startswith( temp_edu )
# #             if pos+1 < len(all_tokens_doc) and cur_edu.startswith( temp_edu+all_tokens_doc[pos+1][0] ):
# #                 temp_edu += cur_token+all_tokens_doc[pos+1][0]
# #                 pos += 2
# #                 continue
# #             if not cur_edu.startswith( temp_edu+cur_token ) and cur_edu.startswith( temp_edu+cur_token[1:] ):
# #                 cur_token = cur_token[1:]
#
#             if not cur_token in cur_edu and cur_token == '...' and cur_edu == temp_edu+cur_token.replace( '...', '.' ):
#                 temp = all_tokens_doc[pos+1]
#                 all_tokens_doc[pos+1] = all_tokens_doc[pos]
#                 all_tokens_doc[pos] = temp
#                 cur_token = all_tokens_doc[pos][0]
# #                 print( ">>ch" )
#
#
#
#             if temp_edu == cur_edu or cur_edu == temp_edu.replace( '\'', '"' ):# or cur_edu == temp_edu.replace( '...', '.' ):
# #                 print( '\n==> ok edu' )
# #                 print( "temp_edu", temp_edu )
# #                 print( pos, all_tokens_doc[pos:pos+10] )
#                 break
#             else:
# #                 print( "cur token", cur_token, cur_edu )
# #                 if not cur_token in cur_edu:
#                 if not cur_edu.startswith( temp_edu+cur_token ):
#                     if cur_token == '\'' and cur_edu.startswith( (temp_edu+cur_token).replace( '\'', '"' ) ):
#                         check_next = True
#                         temp_edu += cur_token
#                         pos += 1
# #                         print( ">>here" )
#
#                     else:
# #                         print( '\ntoken not found' )
# #                         print( cur_edu )
# #                         print( cur_token, t )
# #                         print( pos, all_tokens_doc[pos] )
#                         ignored_tokens.append( pos )
# #                         print( pos, all_tokens_doc[pos:] )
# #                         sys.exit()
#                         pos += 1
# #                         print( temp_edu )
# #                         print( ">>bla" )
#
#                 else:
#                     temp_edu += cur_token
#                     pos += 1
#                 if check_next:
#                     if all_tokens_doc[pos+1][0] == '\'':# TODO rhaaaaaaa ignore ' if we had a ' just before and replace the previous ' by "
#                         pos += 1 #ignore this token
#                         check_next = False
#                     else:
# #                         pos += 1
#                         check_next = False
#
#                     temp_edu += cur_token
#                     pos += 1
# #                     print( ">>check" )
#
#
#
# #                 print( '\t', temp_edu )
# #                 print( '\t', [t[0] for t in all_tokens_doc[pos+1:pos+21]] )
# #                 temp_edu += cur_token
# #                 pos += 1
#
#
# #                 if pos in ignored:
# #                     pos += 1
#                 edu_pos += 1
#         edus_tokens.append( [t for t in all_tokens_doc[cur_pos:pos]] )
#
#     # Final check
#     if pos != len( all_tokens_doc ):
#         if len( edus_tokens ) >= 1 and edus_tokens[-1][-1][0] != '.' and all_tokens_doc[pos][0] == '.':
# #             print( "ADDING APOINT?", edus_tokens[-1][-1][0] )
#             # add this point to the previous EDU?
#             edus_tokens[-1].append( all_tokens_doc[pos] )
#         else:
#             print( "\nMissing tokens?" )
#             print( all_tokens_doc[pos:] )
#             sys.exit()
#
#     return edus_tokens
#
# def get_info( parse, dset ):
#     feats = {} #document 2 token 2 info
#     files = {}
#     doc = 0
#     for f in [ os.path.join( parse, dset, _f ) for _f in os.listdir( os.path.join( parse, dset ) ) if _f.endswith( '.stanford.conll' ) ]:
#         files[doc] = f
#         feats[doc] = {}
#         sent = 0
#         feats[doc][sent] = {}
# #         with codecs.open( f, encoding='utf8' ) as myfile:
#         with open( f, encoding='utf8' ) as myfile:
#             lines = myfile.readlines()
#             for l in lines:
#                 l = l.strip()
#                 if l == '':
#                     sent += 1
#                     feats[doc][sent] = {}
#                 else:
#                     id_tok, form, lemma, pos, ner, head, fct = l.split('\t')
#                     form = form.replace( u'\xa0', u'' )
#                     lemma = lemma.replace( u'\xa0', u'' )
#                     if form == u'``':
#                         form = u'"'
#                         lemma = u'"'
#                     if form == u'\'\'':
#                         form = u'"'
#                         lemma = u'"'
#                     if form == u'---':
#                         form = u'--'
#                         lemma = u'--'
#                     if form == u'`':
#                         form = u'\''
#                         lemma = u'\''
#                     if '`' in form:
#                         form = form.replace(u'`',u'\'')
#                         lemma = lemma.replace(u'`',u'\'')
#                     feats[doc][sent][int(id_tok)] = tuple( [form, lemma, pos, ner, int(head), fct, sent, int(id_tok)] )
#         doc += 1
#
#     print( "# of doc read", doc, file=sys.stderr )
#     return feats, files
#
#
# def get_info_du_file( pf ):
#     feats = {} #token 2 info
# #     files = {}
# #     doc = 0
# #     for f in [ os.path.join( parse, dset, _f ) for _f in os.listdir( os.path.join( parse, dset ) ) if _f.endswith( '.conllu' ) ]:
# #         files[doc] = f
# #         feats[doc] = {}
#     sent = 0
#     feats[sent] = {}
# #         with codecs.open( f, encoding='utf8' ) as myfile:
#     keep_form = None
#     with open( pf, encoding='utf8' ) as myfile:
#         lines = myfile.readlines()
#         for l in lines:
#             l = l.strip()
#             if l == '':
#                 sent += 1
#                 feats[sent] = {}
#             else:
#                 #1	A	a	ADP	prp|<sam->	AdpType=Prep	5	case	_	SpaceAfter=No
#                 id_tok, form, lemma, pos, xpos, morph, head, deprel, deps, misc = l.split('\t')
#
#                 form, lemma = correct_form_ud( form, lemma )
#                 if '-' in id_tok:#strange case in German, see test/maz-18160.conllu
# #                     print( pf )
#                     keep_form = form
#                     continue
# #                 print( form, lemma, pos, xpos, morph, (head), deprel, deps, misc, sent, (id_tok) )
#                 if keep_form != None:
#                     form = keep_form
#                     keep_form = None
#                 feats[sent][int(id_tok)] = tuple( [form, lemma, pos, xpos, morph, int(head), deprel, deps, misc, sent, int(id_tok)] )
# #         doc += 1
# #     _final_modif( feats ) # some modification that need to know the following token
#     _file_final_modif( feats )
# #     print( "# of doc read", doc, file=sys.stderr )
#     return feats
#
#
#
#
#
# def get_info_du( parse, dset ):
#     feats = {} #document 2 token 2 info
#     files = {}
#     doc = 0
#     for f in [ os.path.join( parse, dset, _f ) for _f in os.listdir( os.path.join( parse, dset ) ) if _f.endswith( '.conllu' ) ]:
#         files[doc] = f
#         feats[doc] = {}
#         sent = 0
#         feats[doc][sent] = {}
# #         with codecs.open( f, encoding='utf8' ) as myfile:
#         with open( f, encoding='utf8' ) as myfile:
#             lines = myfile.readlines()
#             for l in lines:
#                 l = l.strip()
#                 if l == '':
#                     sent += 1
#                     feats[doc][sent] = {}
#                 else:
#                     #1	A	a	ADP	prp|<sam->	AdpType=Prep	5	case	_	SpaceAfter=No
#                     id_tok, form, lemma, pos, xpos, morph, head, deprel, deps, misc = l.split('\t')
#
#                     form, lemma = correct_form_ud( form, lemma )
#
#                     feats[doc][sent][int(id_tok)] = tuple( [form, lemma, pos, xpos, morph, int(head), deprel, deps, misc, sent, int(id_tok)] )
#         doc += 1
# #     _final_modif( feats ) # some modification that need to know the following token
#     _final_modif( feats )
#     print( "# of doc read", doc, file=sys.stderr )
#     return feats, files
#
# def _final_modif( feats ):
#     for doc in feats:
#         _file_final_modif( feats[doc] )
#
# def _file_final_modif( feats ):
#     for sent in feats:
#         new_sent = {}
#         removed = []
#         ids = sorted( feats[sent] )
#         for i in range( len(ids) ):
#             form, lemma, pos, xpos, morph, head, deprel, deps, misc, sent, id_tok = feats[sent][ids[i]]
#             if i+1 < len( ids ):
#                 if form == '\'' and feats[sent][ids[i+1]][0] == '\'':
#                     feats[sent][ids[i]] = tuple( [u'"', u'"', pos, xpos, morph, head, deprel, deps, misc, sent, id_tok] )
#                     form1, lemma1, pos1, xpos1, morph1, head1, deprel1, deps1, misc1, sent1, id_tok1 = feats[sent][ids[i+1]]
#                     feats[sent][ids[i+1]] = tuple( [form1[1:], lemma1, pos1, xpos1, morph1, head1, deprel1, deps1, misc1, sent1, id_tok1] )
#                     removed.append( ids[i+1] )
# #                     print( feats[sent][ids[i]] )
# #                     print( feats[sent][ids[i+1]] )
# #                         feats[doc][sent].pop( ids[i+1] )
# #                     print( "1 >>", form, "remove", i+1 )
#             if i+2 < len( ids ):
#                 if form == '.' and feats[sent][ids[i+1]][0] == '.' and feats[sent][ids[i+2]][0] == '.' and feats[sent][ids[i-1]][0] != '...':
#                     feats[sent][ids[i]] = tuple( [u'...', u'...', pos, xpos, morph, head, deprel, deps, misc, sent, id_tok] )
#
#                     removed.extend( [ids[i+1], ids[i+2]] )
# #                         feats[doc][sent].pop( ids[i+1] )
# #                         feats[doc][sent].pop( ids[i+2] )
# #                         print( "2 >>", form, "remove", i+1, i+2 )
#
#         for idr in removed:
#             feats[sent].pop( idr )
#         # TODO change tokens id
#         for k in range( len( feats[sent] ) ):
#             key = sorted( feats[sent].keys() )[k]
#             if key != k+1:
#                 beg = list(feats[sent][key] )[:-1]
#                 beg.append( k+1 )
#                 feats[sent][key] = tuple( beg )

    #










def findFile( tf, eduFiles ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.xml', '')
#     print( basename )
    for f in eduFiles:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' ).replace( '.conllu', '').replace( '.xml', '')
#         print( "\t", b )
        if b == basename:
            return f
    return None

def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files


def getSplit( split_files ):
    file2set = {}
    set2files = {}
    set2count = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for line in myfile.readlines():
                l = line.split('\t')[0]
                file2set[l.strip()] = dset
                set2files[dset].append( l.strip() )
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files



if __name__ == '__main__':
    main()



