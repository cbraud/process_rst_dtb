#!/bin/bash

indata=$1
outdata=$2
mkdir -p ${outdata}

# eg tools_import/udpipe-ud-1.2-160523/portuguese-ud-1.2-160523.udpipe
model=$3
#train dev
for c in dev test #train
do
for filename in ${indata}/${c}/*.raw;
do
    mkdir -p ${outdata}/${c}/
    echo ${indata}/${c}/$(basename "$filename" .raw).conllu
    /home/cbraud/data_tools/udpipe-1.0.0-bin/bin-linux64/udpipe --tokenize --tag --parse ${model} ${filename} >${outdata}/${c}/$(basename "$filename" .raw).conllu

done
done
