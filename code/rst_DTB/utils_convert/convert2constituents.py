#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the format used by the constituent parser
    - input: bracketed trees + edus files (ev. translated)
    - output: one bracketed file for each train/test sets containing the text of the EDUs
            + one embedding file containing a representation for each EDUs
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs
from nltk import Tree
from utils import *

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Discourse treebank directory (ie files .bracketed at least, ev also .edus files).')
#     parser.add_argument('--edus',
#             dest='edus',
#             action='store',
#             default=None,
#             help='Directory containing .edus files (if None, .edus files are in dtreebank/).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for constituent parser')
    parser.add_argument('--split',
            dest='split',
            action='store',
            help='List of files to make train/dev/test')
#     parser.add_argument('--ncomp',
#             dest='ncomp',
#             default=50,
#             action='store',
#             help='Number of components for dimensionality reduction')
#     parser.add_argument('--emb_type',
#             dest='emb_type',
#             default='svd',
#             action='store',
#             help='Method used to build a representation of the DU among svd and soa (typical features for disc parsing). Default=svd')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )


    SEP = '__' #TODO Used to replace blank spaces within EDUs

    print("\n-------------------------\nConvert to const format\n" )
    convert( args.treebank, args.outpath, args.split )

def convert( treebank, outpath, split_files ):
    # write the trees into the train/dev/test files
    # replace the id of the EDUS by text

    file2set, set2files = getSplit( split_files )

    for f in file2set.values():
        c = open( os.path.join( outpath, f+".brackets" ), 'w' )
        c.close()

    writeFilesConst( treebank, outpath, file2set, set2files )


def getSplit( split_files ):
    file2set = {}
    set2files = {}
    set2count = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for l in myfile.readlines():
                file2set[l.strip()] = dset
                set2files[dset].append( l.strip() )
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files

def writeFilesConst( treebank, outpath, file2set, set2files ):
    treeFiles = getFiles( treebank, ext=".bracketed" )
    eduFiles = getFiles( treebank, ext=".edus" )

    # we want to keep the order to know which document is where..
    for dset in set2files:
        outFile = os.path.join( outpath, dset+".brackets" )

        for bf in set2files[dset]:
            tf = findFile( bf, treeFiles )
            ef = findFile( bf, eduFiles )
            if ef == None:
                print( "File not found: "+bf )
                continue
#                 sys.exit( "Edu file not found" )
            if tf == None:
                print( "File not found: "+bf )
                continue
#                 sys.exit( "Tree file not found" )
            edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]
            _write( tf, edus, outFile )

#     for tf in treeFiles:
#         ef = findFile( tf, eduFiles )
#         outFile = getOutFile( tf, outpath, file2set )
#         if ef == None:
#             sys.exit( "Edu file not found" )
#         edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]
#         _write( tf, edus, outFile )

def _write( tf, edus, outFile ):
    print(">>> Reading", tf)
    tree = Tree.fromstring( open( tf, encoding='utf8' ).read() )
    # still required??
    for st in tree.subtrees():
        st.set_label( st.label().replace( '-', '' ) )
    for leafpos in tree.treepositions('leaves'):
        edu_form = u'__'.join( edus[int(tree[leafpos])-1].strip().lower().split() ).replace( '(', '-LRB-').replace(')', '-RRB-').replace( '[', '-LSB-' ).replace( ']', '-RSB-' )
#                 print( edu_form )
        tree[leafpos] = edu_form

    o = open( outFile, 'a', encoding='utf8' )
    o.write( ' '.join( [e.strip() for e in tree.__str__().strip().split('\n')] )+'\n' )
    o.close()


def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files


def findFile( tf, eduFiles ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
#     print( basename )
    for f in eduFiles:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
#         print( "\t", b )
        if b == basename:
            return f
    return None

def getOutFile( tf, outpath, file2set ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
    if not basename in file2set:
        sys.exit( basename+" not found" )
    return os.path.join( outpath, file2set[basename]+".brackets" )













# # ----------------------------------------------------------------------------------
# def write_bracketed( in_path, out_path, ext='.bracketed' ):
#     for i,subdir in enumerate( in_path ):
#         if subdir != None:
#             print( "Writing bracketed", subdir, file=sys.stderr )
#             if i == 0:
#                 out_file = os.path.join( out_path, "train.brackets" )
#             elif i == 1:
#                 out_file = os.path.join( out_path, "test.brackets" )
#             elif i == 2:
#                 out_file = os.path.join( out_path, "dev.brackets" )
#             elif i == 3:
#                 out_file = os.path.join( out_path, "testa.brackets" )
#             elif i == 4:
#                 out_file = os.path.join( out_path, "testb.brackets" )
#
#             _write_brackted( subdir, out_file, ext=ext )
#
# def _write_brackted( subdir, out_file, ext='.bracketed' ):
#     tree_ignored = 0
# #     o = codecs.open( out_file, 'w', encoding='utf8' )
#     o = open( out_file, 'w', encoding='utf8' )
#     for _file in os.listdir( subdir ):
#         if not _file.startswith( '.' ) and _file.endswith( ext ):
#             #print( "Modifying tree from", _file, file=sys.stderr )
#             edu_file = find_file( os.path.basename( _file ), subdir )
#             edus = read_edus( edu_file )# get the ordered EDUs for the tree
# #             print( edus )
#             tree = Tree.fromstring( open( os.path.join( subdir, _file ) ).read() )
#             for st in tree.subtrees():
#                 st.set_label( st.label().replace( '-', '' ) )
#             for leafpos in tree.treepositions('leaves'):
#                 edu_form = u'__'.join( edus[int(tree[leafpos])-1].strip().lower().split() ).replace( '(', '-LRB-').replace(')', '-RRB-').replace( '[', '-LSB-' ).replace( ']', '-RSB-' )
# #                 print( edu_form )
#                 tree[leafpos] = edu_form
#
#             # Add a root node with label S and a dummy left node
#             ##tree = Tree( 'S', [ Tree('EDU', ['-BEGIN-']), tree ] )
#
# #             tree.set_label( 'S' )
# #             print( ' '.join( [e.strip() for e in str(tree).strip().split('\n')] ) )
#             o.write( ' '.join( [e.strip() for e in tree.__str__().strip().split('\n')] )+'\n' )
#     o.close()
#
#
# #     # - Build the embedding file using translated EDUs or original if no translation
# #     if vars(args)['edus'] == None: # no translation
# #         in_edu_path = set_path( vars(args)['dtreebank'] )
# #         original_edus = None
# #         EXT = 'map18.bracketed'# TODO only for English, change that in the original code
# #     else: # translation
# #         in_edu_path = set_path( vars(args)['edus'] )
# #         original_edus = set_path( vars(args)['dtreebank'] )
# #         EXT = '.bracketed'
# #
# #     # Write a file associating each original EDU to a continuous repr
# #     emb_type = vars(args)['emb_type']
# #     if emb_type == "svd":
# #         out_embed = os.path.join( vars(args)['outpath'], 'embeddings_edus_svd-'+str(vars(args)['ncomp'])+'.txt' )
# #     else:
# #         out_embed = os.path.join( vars(args)['outpath'], 'embeddings_edus_'+emb_type+'.txt' )
# #     embeddings = build_embeddings(
# #             in_edu_path, original_edus,
# #             n_components=int(vars(args)['ncomp']), sep=SEP, emb_type=emb_type )
# #     write_embeddings( embeddings, out_embed )
# #
# #     # Write a file for each train/test containing all the bracketed trees with the text of the EDUs as leaves
# #     write_bracketed( set_path( vars(args)['dtreebank'] ), vars(args)['outpath'], ext=EXT )
#
#
#
#
#
#
#
#
if __name__ == '__main__':
    main()
#
#
#
