#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the format used by the parser
    - input: bracketed trees + edus files
    - output ?

TODO for now use the train/test/dev files made with the constituent converter

'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, gzip, re
from nltk import Tree
from utils import *


def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Discourse treebank directory (ie files .bracketed at least, ev also .edus files).')
#     parser.add_argument('--edus',
#             dest='edus',
#             action='store',
#             default=None,
#             help='Directory containing .edus files (if None, .edus files are in dtreebank/).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for constituent parser')
    parser.add_argument('--split',
            dest='split',
            action='store',
            help='List of files to make train/dev/test')
#     parser.add_argument('--ncomp',
#             dest='ncomp',
#             default=50,
#             action='store',
#             help='Number of components for dimensionality reduction')
#     parser.add_argument('--emb_type',
#             dest='emb_type',
#             default='svd',
#             action='store',
#             help='Method used to build a representation of the DU among svd and soa (typical features for disc parsing). Default=svd')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )


    SEP = '__' #TODO Used to replace blank spaces within EDUs


    convert( args.treebank, args.outpath, args.split )

def convert( treebank, outpath, split_files ):
    # write the trees into the train/dev/test files
    # replace the id of the EDUS by text

    file2set, set2files = getSplit( split_files )

    for f in file2set.values():
        c = open( os.path.join( outpath, f+".conll" ), 'w' )
        c.close()

    writeFilesDep( treebank, outpath, file2set, set2files )


def getSplit( split_files ):
    file2set = {}
    set2files = {}
    set2count = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for l in myfile.readlines():
                file2set[l.strip()] = dset
                set2files[dset].append( l.strip() )
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files


def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files


def findFile( tf, eduFiles ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
#     print( basename )
    for f in eduFiles:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
#         print( "\t", b )
        if b == basename:
            return f
    return None

def getOutFile( tf, outpath, file2set ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
    if not basename in file2set:
        sys.exit( basename+" not found" )
    return os.path.join( outpath, file2set[basename]+".conll" )




def writeFilesDep( treebank, outpath, file2set, set2files ):
    treeFiles = getFiles( treebank, ext=".bracketed" )
    eduFiles = getFiles( treebank, ext=".edus" )

    outFile2count = {}

    # we want to keep the order to know which document is where..
    for dset in set2files:
        outFile = os.path.join( outpath, dset+".conll" )

        for bf in set2files[dset]:
            tf = findFile( bf, treeFiles )
            ef = findFile(bf, eduFiles )
            if ef == None:
                print( "File not found: "+bf )
                continue
#                 sys.exit( "Edu file not found" )
            if tf == None:
                print( "File not found: "+bf )
                continue
#                 sys.exit( "Tree file not found" )
            if not outFile in outFile2count:
                outFile2count[outFile] = 0
            edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]

            tree = Tree.fromstring( open( tf, encoding='utf8' ).read() )

            edu2text = {i+1:'__'.join( [t for t in edus[i].split()] ) for i in range( len(edus) )}
            root, dependencies = convert_dependency( tree, keep_nuc=True )
            sorted_edus = sorted( edu2text.keys() )

            o = open( outFile, 'a', encoding='utf8' )

            if outFile2count[outFile] > 0:
                o.write( '\n' )

            for edu in sorted_edus:#we want that index begins at 1
                if edu == root:

                    o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', '0', 'ROOT', '_', '_' ] )+'\n' )
                else:
                    head, relation = find_dep_link( edu, dependencies, sorted_edus )
                    o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', str(head), relation, '_', '_' ] )+'\n' )

            outFile2count[outFile] += 1





#     for tf in treeFiles:
#         ef = findFile( tf, eduFiles )
#         outFile = getOutFile( tf, outpath, file2set )
#         if not outFile in outFile2count:
#             outFile2count[outFile] = 0
#         if ef == None:
#             sys.exit( "Edu file not found" )
#         edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]
#
#         tree = Tree.fromstring( open( tf, encoding='utf8' ).read() )
#
#         edu2text = {i+1:'__'.join( [t for t in edus[i].split()] ) for i in range( len(edus) )}
#         root, dependencies = convert_dependency( tree, keep_nuc=True )
#         sorted_edus = sorted( edu2text.keys() )
#
#         o = open( outFile, 'a', encoding='utf8' )
#
#         if outFile2count[outFile] > 0:
#             o.write( '\n' )
#
#         for edu in sorted_edus:#we want that index begins at 1
#             if edu == root:
#
#                 o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', '0', 'ROOT', '_', '_' ] )+'\n' )
#             else:
#                 head, relation = find_dep_link( edu, dependencies, sorted_edus )
#                 o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', str(head), relation, '_', '_' ] )+'\n' )

#        outFile2count[outFile] += 1
#     for f in outFile2count:
#         print( f, outFile2count[f] )


    # still required??
#     for st in tree.subtrees():
#         st.set_label( st.label().replace( '-', '' ) )
#     for leafpos in tree.treepositions('leaves'):
#         edu_form = u'__'.join( edus[int(tree[leafpos])-1].strip().lower().split() ).replace( '(', '-LRB-').replace(')', '-RRB-').replace( '[', '-LSB-' ).replace( ']', '-RSB-' )
# #                 print( edu_form )
#         tree[leafpos] = edu_form
#
#     o = open( outFile, 'a', encoding='utf8' )
#     o.write( ' '.join( [e.strip() for e in tree.__str__().strip().split('\n')] )+'\n' )
#     o.close()










# def _build_dependency( path, out_path, ext='.bracketed' ):
#     o = codecs.open( out_path, 'w', encoding='utf8' )
#     count_file = 0
#     for _file in os.listdir( path ):
#         if not _file.startswith('.') and _file.endswith( ext ):
#             if count_file > 0:
#                 o.write( '\n' )
#             edu_file =  os.path.join( path, _file.replace( ".bracketed", ".edus" ) )
#             if not os.path.isfile( edu_file ):
#                 edu_file = os.path.join( path, '.'.join( _file.split('.')[:2] )+".edus" )
#             if not os.path.isfile( edu_file ):
#                 edu_file = os.path.join( path,  _file.split('.')[0]+".edus" )
#             if not os.path.isfile( edu_file ):
#                 sys.exit( "edu file not found "+edu_file)
#
#             edu2text = getTextEdus( edu_file )
#             tree = Tree.fromstring( open( os.path.join( path, _file ) ).read().strip() )
#             root, dependencies = convert_dependency( tree, keep_nuc=True )
#             #edus = sorted( [int(e) for e in tree.leaves()] )
#             sorted_edus = sorted( edu2text.keys() )
#             for edu in sorted_edus:#we want that index begins at 1
#                 if edu == root:
#
#                     o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', '0', 'ROOT', '_', '_' ] )+'\n' )
#                 else:
#                     head, relation = find_dep_link( edu, dependencies, sorted_edus )
#                     o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', str(head), relation, '_', '_' ] )+'\n' )
#             count_file += 1
#     o.close()


def getTextEdus( filePath ):
    edu2text = {}
    eduId = 1
    c = codecs.open( filePath, encoding='utf8' )
    lines = c.readlines()
    c.close()
    for edu_txt in lines:
        edu_txt = edu_txt.strip()
        edu2text[eduId] = edu_txt.replace( ' ', '__' )
        eduId += 1
    return edu2text


def convert_dependency( tree, keep_nuc=False ):
    '''
     - choisir le root: premiere EDU qui est un nucleus
       (an EDU is a nucleus if it is in a sallient ensemble)
       --> [Un point interessant]_1 [parce qu'il definit  ...]_2 : root 1
       --> [Parce qu'il definit ...]_1 [c'est un point interessant]_2 : root 2
     - left headed but among nuclei
       (find the sallient set for each child, ie all nuclei)
       ( NN-textual ( NN-same (NN-list (1,2) (NS-elab (3, NS-elab(4,5))), NN-list (...
       1 -list-> 2 , 1 -same-> 3 , 3 -elab-> 4 , 4 -elab-> 5
    '''
    relations = [] # [label, left sallient set, right sallient set], ..
    _dep_form( tree, relations, keep_nuc=keep_nuc )
    root = set_root( relations ) # first nucleus in the document
    added_edus = set() # check all edus have been covered
    dependencies = { root:[] } # build dependency dict: head --> [ [rel, dep], ...]
    for ( r, left, right ) in relations: # left and right are sallient ens, ie nuclei only
#         print( r, left, right )
        if root in left:
            head = root
        else:
            head = left[0]
        dep = right[0]
        added_edus.add( head )
        added_edus.add( dep )
#         print( "head", head, "dep", dep )
        if head in dependencies:
            dependencies[head].append( [r, dep] )
        else:
            dependencies[head] = [ [r,dep] ]
#     print( "Choosen root:", root )
#     print( "Check covered edus:", len( added_edus ) )
    return root, dependencies


# def write_dependency( edu2text, root, dependencies, file_out ):
#     '''
#     Write a new file, format: edutext TAB label
#     '''
#     sorted_edus = sorted( edu2text, lambda x,y:cmp(x,y) )
#     with open( file_out, "w" ) as f:
#         for edu in sorted_edus:
#             if edu == root:
#                 f.write( edu2text[edu]+"\t"+"root"+"\n" )
#             else:
#                 find_dep_link( edu, dependencies, sorted_edus, edu2text, f )

#def find_dep_link( edu, dependencies, sorted_edus, edu2text, f ):
def find_dep_link( edu, dependencies, sorted_edus ):
    '''
    Get the dependency link for edu
    '''
    for h in dependencies:
        for (r,d) in dependencies[h]:
            if edu == d:
                #print( edu, h, sorted_edus.index( h ), sorted_edus.index( d ), sorted_edus.index( h )-sorted_edus.index( d ) )
                return h, r
            #sorted_edus.index( h ) we want the index to begin at 1
#                 f.write( "\t".join( [edu2text[edu],
#                     str(sorted_edus.index( h )-sorted_edus.index( d ))+"_"+r] )+"\n" )
#     print( edu, '\n', dependencies )

def set_root( relations ):
    nuclei = []
    for ( r, left, right ) in relations:
#         print( r, left, right )
        nuclei.extend( [e for e in left] )
        #nuclei.extend( [e for e in right] )
#     print( nuclei )
    #return sorted( nuclei, lambda x,y:cmp(x,y) )[0]
    return nuclei[0]

# Parcours en profondeur
def _dep_form( t, relations, keep_nuc=False ):
    try:
        label = t.label()
        label = label.replace('_', '')
        if not keep_nuc and ("NN-" in t.label() or "NS-" in t.label() or "SN-" in t.label()):
            #label = '-'.join( t.label().split('-')[1:] )
            label = t.label().split('-')[0] # keep only nuc
        if not label == "EDU":
            children = [c for c in t]
            if len( children ) > 2:
                sys.exit( "non binary tree ?" )
            # looking for sallient ensemble for each child
            if "NN-" in t.label() or "NS-" in t.label():
                salient_left = get_salient( children[0] )
                salient_right = get_salient( children[1] )
            elif "SN-" in t.label():
                salient_left = get_salient( children[1] )
                salient_right = get_salient( children[0] )
            relations.append( [label, salient_left, salient_right] )
            #print label, salient_left, salient_right
            _dep_form( children[0], relations, keep_nuc=keep_nuc )
            _dep_form( children[1], relations, keep_nuc=keep_nuc )
    except AttributeError:
        return

def get_salient( tree ):
    salient_set = []
    _get_salient( tree, salient_set )
    return salient_set

def _get_salient( t, salient_set ):
    try:
        label = t.label()
        children = [c for c in t]
        if label == "EDU":
            salient_set.append( int( children[0] ) )
        else:
            nuclearity = label.split('-')[0]
            left_nuc = nuclearity[0]
            right_nuc = nuclearity[1]
            if left_nuc == 'N':
                _get_salient( children[0], salient_set )
            if right_nuc == 'N':
                _get_salient( children[1], salient_set )

    except AttributeError:
        return




if __name__ == '__main__':
    main()



