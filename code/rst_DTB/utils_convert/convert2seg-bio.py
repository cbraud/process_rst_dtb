#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the BIO format on token for segmentation in EDUs
    - Input: conllu files augmented with EDU id (see read rstdt code) < Univ dependencies tokenization
    - Output: 1 file per sentence with BIO encoding on the tokens
'''


import argparse, os, sys


def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the BIO format to train a segmenter.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Treebank in conllu format')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for the segmenter')
    parser.add_argument('--trans',
            dest='trans',
            action='store_true',
            help='If True use the translated form')
    parser.add_argument('--pos',
            dest='pos',
            action='store_true',
            help='If True use the POS tag, else use the form.')
    parser.add_argument('--poswd',
            dest='poswd',
            action='store_true',
            help='If True use the POS tag and the form (Mix format).')
    parser.add_argument('--poswdfct',
            dest='poswdfct',
            action='store_true',
            help='If True use the dep fct, the POS tag and the form (Mix format).')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )

    #convert( args.treebank, args.outpath, trans=args.trans, pos=args.pos )

    _convert_form( args.treebank, args.outpath, trans=args.trans,
            pos=args.pos, poswd=args.poswd, poswdfct=args.poswdfct )


def convert( treebank, outpath, trans=False, pos=False, poswd=False, poswdfct=False ):
    for _file in [f for f in os.listdir( treebank ) if not f.startswith('.') ]:#train dev test
        basename = os.path.basename( _file ).split('.')[0]
        output_dir = os.path.join( outpath, basename )
        if not os.path.isdir( output_dir ):
            os.mkdir( output_dir )
        _convert_form( os.path.join( treebank, _file ), output_dir, trans=trans,
                pos=pos, poswd=poswd, poswdfct=poswdfct )
        #_convert_pos( os.path.join( treebank, _file ), output_dir, trans=trans, pos=pos )



def _convert_form( treebank, output_dir, trans=False, pos=False, poswd=False, poswdfct=False ):
    for dset in [d for d in os.listdir( treebank ) if not d.startswith( '.' )]:
        if not os.path.isdir( os.path.join( output_dir, dset ) ):
            os.mkdir( os.path.join( output_dir, dset ) )
        for _file in [f for f in os.listdir( os.path.join( treebank, dset ) ) if not f.startswith('.') ]:
            print( "Reading", _file )
            with open( os.path.join( treebank, dset, _file ) ) as f:
                lines = f.readlines()
                curdoc_id = -1
                curedu_id = -1
                cursent_id = -1
                for i,l in enumerate( lines[1:] ):#first line = categories
                    l = l.strip()
                    if not l == '':
                        if trans:
                            doc_id, sent_id, edu_id, tok_id, form, lemma, postag, head, fct, morph, xpos, transForm, transLemma, transStem  = l.split('\t')
                            if transForm.lower() != 'none':
                                form = transForm
                        else:
                            doc_id, sent_id, edu_id, tok_id, form, lemma, postag, head, fct, morph, xpos = l.split('\t')
                        if poswdfct:
                            form = form+'\t\t'+postag+'\t\t'+fct
                            pos=False
                            poswd=False
                        if poswd:
                            form = form+'\t\t'+postag
                            pos=False#just to be sure, can t be both!
                        if pos:
                            form = postag
                        doc_id = int( doc_id )

                        sent_id = int( sent_id )

                        ##if doc_id != curdoc_id:
                        if sent_id != cursent_id:
                            ##curdoc_id = doc_id
                            cursent_id = sent_id
                            out_file = os.path.join( output_dir, dset, "doc-"+ _file.split('.')[0][4:]+'-'+str(sent_id)+".seg" )
                            #out_file = os.path.join( output_dir, dset, '.'.join( _file.split('.')[:-1] )+".seg" )
                            #out_file = os.path.join( output_dir, dset, "doc-"+str( curdoc_id )+".seg" )
                            out = open( out_file, 'w' )
                            out.close()

                        out = open( out_file, 'a' )
                        if edu_id != curedu_id:
                            if poswdfct:
                                out.write( form.split('\t\t')[0]+"\t"+"B\n" )
                                out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                                out.write( form.split('\t\t')[2]+"\t"+"I\n" )
                            elif poswd:
                                out.write( form.split('\t\t')[0]+"\t"+"B\n" )
                                out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                            else:
                                out.write( form+"\t"+"B\n" )
                            curedu_id = edu_id
                        else:
                            if poswdfct:
                                out.write( form.split('\t\t')[0]+"\t"+"I\n" )
                                out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                                out.write( form.split('\t\t')[2]+"\t"+"I\n" )
                            elif poswd:
                                out.write( form.split('\t\t')[0]+"\t"+"I\n" )
                                out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                            else:
                                out.write( form+"\t"+"I\n" )
                        out.close()


# when input are 3 files, 1 per dset
def _convert_form_dset( treebank, output_dir, trans=False, pos=False, poswd=False ):
    for dset in [d for d in os.listdir( treebank ) if not d.startswith( '.' )]:
        out_dir = os.path.join( output_dir, dset.split('.')[0] )
        if not os.path.isdir( out_dir ):
            os.mkdir( out_dir )
        print( "Reading", treebank, dset )
        with open( os.path.join( treebank, dset ) ) as f:
            lines = f.readlines()
            curdoc_id = -1
            curedu_id = -1
    #         cursent_id = -1

            for l in lines[1:]:#first line = categories
                l = l.strip()
                if not l == '':
                    if trans:
                        doc_id, sent_id, edu_id, tok_id, form, lemma, postag, head, fct, morph, xpos, transForm, transLemma, transStem  = l.split('\t')
                        if transForm.lower() != 'none':
                            form = transForm
                    else:
                        doc_id, sent_id, edu_id, tok_id, form, lemma, postag, head, fct, morph, xpos = l.split('\t')
                    if poswd:
                        form = form+'\t\t'+postag
                        pos=False#just to be sure, can t be both!
                    if pos:
                        form = postag

                    doc_id = int( doc_id )

                    if doc_id != curdoc_id:
                        curdoc_id = doc_id
                        out_file = os.path.join( out_dir, "doc-"+str( curdoc_id )+".seg" )
                        out = open( out_file, 'w' )
                        out.close()

                    out = open( out_file, 'a' )
                    if edu_id != curedu_id:
                        if poswd:
                            out.write( form.split('\t\t')[0]+"\t"+"B\n" )
                            out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                        else:
                            out.write( form+"\t"+"B\n" )
                        curedu_id = edu_id
                    else:
                        if poswd:
                            out.write( form.split('\t\t')[0]+"\t"+"I\n" )
                            out.write( form.split('\t\t')[1]+"\t"+"I\n" )
                        else:
                            out.write( form+"\t"+"I\n" )
                    out.close()




if __name__ == '__main__':
    main()
