#!/usr/bin/python
# -*- coding: utf-8 -*-



import argparse, os, sys, subprocess, shutil, codecs
from nltk.tokenize.stanford_segmenter import StanfordSegmenter

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--raw',
            dest='raw',
            action='store',
            help='Raw data.')
    parser.add_argument('--stanford',
            dest='stanford',
            action='store',
            help='Directory containing the data and model used by the stanford segmenter')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for constituent parser')

    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )

    segmenter = getSegmenter( args.stanford )
    segment( args.raw, segmenter, args.outpath )


def getSegmenter( inpath ):
    return StanfordSegmenter(
            path_to_jar                 = os.path.join( inpath, "stanford-segmenter-3.6.0.jar" ),
            path_to_slf4j               = os.path.join( inpath, "slf4j-api.jar" ),
            path_to_sihan_corpora_dict  = os.path.join( inpath, "./data" ),
            path_to_model               = os.path.join( inpath, "./data/pku.gz" ),
            path_to_dict                = os.path.join( inpath, "./data/dict-chris6.ser.gz")
            )

def segment( raw, segmenter, outpath ):
    for dset in [d for d in os.listdir( raw ) if not d.startswith('.')]:
        outdset = os.path.join( outpath, dset )
        if not os.path.isdir( outdset ):
            os.mkdir( outdset )
        for f in [_f for _f in os.listdir( os.path.join( raw, dset ) ) if _f.endswith( '.raw' )]:
            o = open( os.path.join( outdset, f+'.tok' ), 'w', encoding='utf8' )
            sentence = open( os.path.join( raw, dset, f ) ).read().strip()
            o.write( segmenter.segment(sentence) )
            o.close()
#     sentence = u"这是斯坦福中文分词器测试"
#     tokens = segmenter.segment(sentence)
#     print( tokens )
#     return

if __name__ == '__main__':
    main()

