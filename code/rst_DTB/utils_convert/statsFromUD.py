#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
stats: #sentences, words, EDUs
'''

# TODO: redo the parse of the files using directly the .brackets files
# TODO redo the parse file without the previously done tokenization

from __future__ import print_function
import argparse, os, sys, shutil, codecs, numpy, collections
from nltk import Tree
import numpy as np


def main( ):
    parser = argparse.ArgumentParser(
            description='Stats.')
    parser.add_argument('--parse',
            dest='parse',
            action='store',
            help='Directory containing parsing info for the data.')
    args = parser.parse_args()

    try:
        stats( args.parse )
    except:
        stats_file( args.parse )



def stats_file( parse ):
    doc, sent, words, edu= 0,0,0,0
    for dset in [d for d in os.listdir( parse ) if not d.startswith('.')]:
        #for f in [_f for _f in os.listdir( os.path.join( parse, dset ) ) if not _f.startswith('.')]:
        cur_doc, cur_sent, cur_edu = -1, -1, -1
        lines = open( os.path.join( parse, dset ) ).readlines()
        for l in lines[1:]:
            l = l.strip()
            if not l == '':
                id_doc, id_sent, id_edu, id_tok, form, lemma, pos, head, deprel, morph, xpos = l.split('\t')
                words += 1
                if int(id_doc) != cur_doc:
                    doc += 1
                    cur_doc = int(id_doc)
                if int(id_sent) != cur_sent:
                    sent += 1
                    cur_sent = int(id_sent)
                if int(id_edu) != cur_edu:
                    edu += 1
                    cur_edu = int(id_edu)
    print( "#Doc", doc, "#Sent", sent, "#Edus", edu, "#Words", words )



# Write 1 file per input file
def stats( parse ):
    doc, sent, words, edu= 0,0,0,0
    for dset in [d for d in os.listdir( parse ) if not d.startswith('.')]:
        for f in [_f for _f in os.listdir( os.path.join( parse, dset ) ) if not _f.startswith('.')]:
            cur_doc, cur_sent, cur_edu = -1, -1, -1
            lines = open( os.path.join( parse, dset, f ) ).readlines()
            for l in lines[1:]:
                l = l.strip()
                if not l == '':
                    id_doc, id_sent, id_edu, id_tok, form, lemma, pos, head, deprel, morph, xpos = l.split('\t')
                    words += 1
                    if int(id_doc) > cur_doc:
                        doc += 1
                        cur_doc = int(id_doc)
                    if int(id_sent) > cur_sent:
                        sent += 1
                        cur_sent = int(id_sent)
                    if int(id_edu) > cur_edu:
                        edu += 1
                        cur_edu = int(id_edu)
    print( "#Doc", doc, "#Sent", sent, "#Edus", edu, "#Words", words )


if __name__ == '__main__':
    main()



