#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Split a conllu file into sentences
'''


import argparse, os, sys


def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the BIO format to train a segmenter.')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input directory containing the data in the conllu format')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the conllu format split into sentences')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )

    #convert( args.treebank, args.outpath, trans=args.trans, pos=args.pos )

    _split_sent( args.inpath, args.outpath )

def _split_sent( inpath, outpath ):
    for dset in [d for d in os.listdir( inpath ) if not d.startswith('.')]:
        print( 'Reading', dset )
        outdir = os.path.join( outpath, dset )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )
        for _file in [f for f in os.listdir( os.path.join( inpath, dset ) ) if not f.startswith('.') ]:
            print( 'Reading', _file )
            with open( os.path.join( inpath, dset, _file ) ) as f:
                lines = f.readlines()
                cur_sent = -1
                for l in lines[1:]:
                    l = l.strip()
                    doc, sent, edu, tok, form, lemma, pos, head, fct, morph, xpos = l.split('\t')
                    sent = int(sent)
                    if sent != cur_sent:
                        outf = os.path.join( outpath, dset,
                            _file.split('.')[0]+'_sent'+str(sent)+'.conllu' )
                        o = open( outf, 'w' )
                        o.write( '\t'.join( ['doc', 'sent', 'edu', 'tok', 'form', 'lemma', 'pos', 'head', 'fct', 'morph', 'xpos'] )+'\n' )
                        o.close()
                        cur_sent = sent
                    o = open( outf, 'a' )
                    o.write( l+'\n' )



if __name__ == '__main__':
    main()
