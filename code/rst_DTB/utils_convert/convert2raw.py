#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the format used by the constituent parser
    - input: bracketed trees + edus files (ev. translated)
    - output: one bracketed file for each train/test sets containing the text of the EDUs
            + one embedding file containing a representation for each EDUs
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs
from nltk import Tree
from utils import *

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Discourse treebank directory (ie files .bracketed at least, ev also .edus files).')
#     parser.add_argument('--edus',
#             dest='edus',
#             action='store',
#             default=None,
#             help='Directory containing .edus files (if None, .edus files are in dtreebank/).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for constituent parser')
    parser.add_argument('--split',
            dest='split',
            action='store',
            help='List of files to make train/dev/test')
#     parser.add_argument('--ncomp',
#             dest='ncomp',
#             default=50,
#             action='store',
#             help='Number of components for dimensionality reduction')
#     parser.add_argument('--emb_type',
#             dest='emb_type',
#             default='svd',
#             action='store',
#             help='Method used to build a representation of the DU among svd and soa (typical features for disc parsing). Default=svd')
    args = parser.parse_args()

    if not os.path.isdir(  args.outpath ):
        os.mkdir(  args.outpath )

    SEP = '__' #TODO Used to replace blank spaces within EDUs

    convert( args.treebank, args.outpath, args.split )

def convert( treebank, outpath, split_files ):
    # write the trees into the train/dev/test files
    # replace the id of the EDUS by text

    file2set, set2files = getSplit( split_files )

#     for f in file2set.values():
#         c = open( os.path.join( outpath, f+".brackets" ), 'w' )
#         c.close()

    writeFilesRaw( treebank, outpath, file2set, set2files )

def writeFilesRaw( treebank, outpath, file2set, set2files ):
    eduFiles = getFiles( treebank, ext=".edus" )
    for dset in set2files:
        outdir = os.path.join( outpath, dset )
        if not os.path.isdir( outdir ):
            os.mkdir( outdir )

        for bf in set2files[dset]:
            ef = findFile( bf, eduFiles )
            if ef == None:
                print( "File not found: "+bf )
                continue
            edus = [l.strip() for l in open( ef, 'r', encoding='utf8' ).readlines()]

            outFile = open( os.path.join( outdir, bf+'.raw' ), 'w' )
            for e in edus:
                e = e.strip()
                # For chinese, no additional space
                #outFile.write( e.replace( '-LSB-', '[' ).replace( '-RSB-', ']' ).replace( '-LRB-', '(' ).replace( '-RRB-', ')' ) )
                outFile.write( e+' ' )
            outFile.close()

def getSplit( split_files ):
    file2set = {}
    set2files = {}
    set2count = {}
    for f in [_f for _f in os.listdir( split_files ) if not _f.startswith( '.' )]:
        dset = os.path.basename( f.replace( '.txt', '' ) )
        set2files[dset] = []
        with open( os.path.join( split_files, f ) ) as myfile:
            for l in myfile.readlines():
                file2set[l.strip()] = dset
                set2files[dset].append( l.strip() )
                if dset in set2count:
                    set2count[dset] += 1
                else:
                    set2count[dset] = 1
    print( ", ".join( [dset+':'+str(set2count[dset]) for dset in sorted(set2count.keys())] ) )
    return file2set, set2files

def getFiles( tbpath, ext=".edus" ):
    _files = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                #and not 'ANOTADOR_B' in os.path.join( p,file )
                _files.append( os.path.join( p, file ) )
    return _files

def findFile( tf, eduFiles ):
    basename = os.path.basename( tf ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
    #print( basename )
    for f in eduFiles:
        b = os.path.basename( f ).replace( '.out', '').replace( '.dis', '' ).replace('.txt', '').replace('.edus', '').replace( '.bracketed', '').replace( '.lisp', '' ).replace( '.thiago', '' )
        #print( "\t", b )
        if b == basename:
            return f
    return None


if __name__ == '__main__':
    main()

