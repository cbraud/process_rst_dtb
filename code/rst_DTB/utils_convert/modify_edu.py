#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the format used by the constituent parser
    - input: bracketed trees + edus files (ev. translated)
    - output: one bracketed file for each train/test sets containing the text of the EDUs
            + one embedding file containing a representation for each EDUs
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs
from nltk import Tree
from utils import *

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Discourse treebank directory (ie files .bracketed at least, ev also .edus files).')

    args = parser.parse_args()


    for _f in os.listdir( args.treebank ):
        if _f.endswith( '.edus' ) and not _f.startswith( '.'):

            c = open( os.path.join( args.treebank, _f ), 'r' )
            lines = c.readlines()
            c.close()

            o = open( os.path.join( args.treebank, _f ), 'w' )

            for l in lines:
                try:
                    a = int( l.strip().split()[0] )
                except:
                    print( _f )
                    print( l )
                    sys.exit( "Not a number" )
                print( l.strip() )
                print( ' '.join( l.strip().split()[1:] ) )
                newl = l.strip().split()[1:]
                if newl[0] == "#":
                    o.write( ' '.join( l.strip().split()[2:] )+"\n" )
                else:
                    o.write( ' '.join( l.strip().split()[1:] )+"\n" )
            o.close()

if __name__ == '__main__':
    main()

