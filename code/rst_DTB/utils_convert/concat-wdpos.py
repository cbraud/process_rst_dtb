#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Concatenate Word and POS vectors as a new input for the discourse segmenter
'''


import argparse, os, sys, shutil, codecs, numpy, collections
import numpy as np


def main( ):
    parser = argparse.ArgumentParser(
            description='Preparse for tagging and parsing.')
    parser.add_argument('--tok',
            dest='tok',
            action='store',
            help='Input directory, tokenized (contains .tok files).')
    parser.add_argument('--pos',
            dest='pos',
            action='store',
            help='Input directory, tokenized (contains .tok files).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory (same as in the input if not given).')
    parser.add_argument('--mode',
            dest='mode',
            action='store',
            default='concat',
            help='Simple concatenation, leave the algo found a vector from WD+POS (concat), or use wd embeddings for the word part (embed) or use stg else?.')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    concat( args.tok, args.pos, args.outpath, args.mode )

def concat( tok, pos, outpath, mode ):
    if mode == 'concat':
        _concat( tok, pos, outpath )


def _concat( tokd, posd, outpath ):
    for dset in [d for d in os.listdir( tokd ) if not d.startswith('.')]:
        if not os.path.isdir( os.path.join( outpath, dset ) ):
            os.mkdir(  os.path.join( outpath, dset ) )
        for f in [_f for _f in os.listdir( os.path.join( tokd, dset ) ) if not _f.startswith('.')]:
            tokf = os.path.join( tokd, dset, f )
            posf = os.path.join( posd, dset, f )
            if not os.path.isfile( posf ):# find corresponding pos file
                sys.exit( "POS file not found: "+dset+' '+f )
            with open( os.path.join( outpath, dset, f ), 'w' ) as fout:
                tokens = [(l.strip().split('\t')[0],l.strip().split('\t')[1]) for l in open( tokf ).readlines()]
                pos = [(l.strip().split('\t')[0],l.strip().split('\t')[1]) for l in open( posf ).readlines()]
                #print( tokens )
                for i,(t,l) in enumerate( tokens ):
                    fout.write( t+'++'+pos[i][0]+'\t'+l+"\n" )            
    
if __name__ == '__main__':
    main()


