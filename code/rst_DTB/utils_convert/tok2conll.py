#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Adding EDU info to conll files
'''

# TODO: redo the parse of the files using directly the .brackets files
# TODO redo the parse file without the previously done tokenization

from __future__ import print_function
import argparse, os, sys, shutil, codecs, numpy, collections
from nltk import Tree
import numpy as np


def main( ):
    parser = argparse.ArgumentParser(
            description='Preparse for tagging and parsing.')
    parser.add_argument('--tok',
            dest='tok',
            action='store',
            help='Input directory, tokenized (contains .tok files).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory (same as in the input if not given).')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    convert( args.tok, args.outpath )

def convert( tok, outpath ):
    for dset in [d for d in os.listdir( tok ) if not d.startswith( '.' ) ]:
        o = open( os.path.join( outpath, dset+'.raw.tok' ), 'w' )

        for f in [_f for _f in os.listdir( os.path.join( tok, dset ) ) if _f.endswith( '.tok' )]:
            tokens = [t.strip() for t in open(  os.path.join( tok, dset, f ) ).read().strip().split()]
            for i,t in enumerate( tokens ):
                o.write( '\t'.join( [ str(i+1), t, '_', '_', '_', '_', '_', '_', '_', '_'
 ] )+'\n' )
            o.write( '\n' )
        o.close()



if __name__ == '__main__':
    main()



