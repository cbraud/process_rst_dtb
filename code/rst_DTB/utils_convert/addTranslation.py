#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Add a translation of each token/lemma in the conllu format (add 2 columns)
TODO
- use ths POS to find proper noun (and do not translate them) ?
- why no lemma for German ?
- lemmatize the dictionnary?
'''

import argparse, os, sys, string, snowballstemmer

# PUNCT = set(string.punctuation)

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--conllu',
            dest='conllu',
            action='store',
            help='dev/test/train.parse files with conllu info.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory.')
    parser.add_argument('--dict',
            dest='dict',
            action='store',
            help='Dictionnary for translations')
    parser.add_argument('--lang',
            dest='lang',
            action='store',
            default=None,
            help='Language, used for stemming.')

    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    if args.lang:
        stemmer = snowballstemmer.stemmer(args.lang)
    else:
        stemmer = None

    addTranslation( args.conllu, args.dict, args.outpath, stemmer )



def addTranslation( conllu, dict, outpath, stemmer ):
    lex = read_dict( dict, reverse=False ) # foreign/source word -> english/target word
    lex_stem = None
    if stemmer:
        lex_stem = stemLex( lex, stemmer )

    for f in [os.path.join( conllu, _f ) for _f in os.listdir( conllu ) if not _f.startswith( '.')]:
        print( "Reading:", f )
        outfile = os.path.join( outpath, os.path.basename( f ) )
        writeTranslations( f, lex, outfile, stemmer, lex_stem )

def writeTranslations( infile, lex, outfile, stemmer, lex_stem ):
    words = set()
    unkWords = {}
    lemmas = set()
    unkLemmas = {}
    foundStems = set()
    o = open( outfile, 'w', encoding='utf8' )

    with open( infile, 'r', encoding='utf8' ) as myfile:
        lines = myfile.readlines()
        cats = lines[0].strip().split('\t')
        cats.extend( [u"transForm", u"transLemma", u"transStem"] )
        o.write( "\t".join( cats )+"\n" )
        for l in lines[1:]:
            l = l.strip()
            if l == '':
                # write the line
                o.write( '\n' )
                continue
            cur_info = l.strip().split( '\t' )
            _addTranslation( cur_info, lex, words, unkWords, lemmas, unkLemmas, foundStems, stemmer, lex_stem )
#             print( "\t".join( cur_info ) )
            o.write( '\t'.join( cur_info )+"\n" )


    print( "\nDict size:", len(lex.keys()) )

    print( '\n#Words=', len( words ),
            '#Unknown words=', len( unkWords.keys() ), file=sys.stderr )
    print( "Most frequent unk words:",
            " ; ".join( [w+":"+str(f) for (w,f) in sorted( unkWords.items(), key=lambda x:x[1] , reverse=True)][:10] ) )

    print( '\n#Lemmas=', len( lemmas ),
            '#Unknown lemmas=', len( unkLemmas.keys() ), file=sys.stderr )
    print( "Most frequent unk lemmas:",
            " ; ".join( [w+":"+str(f) for (w,f) in sorted( unkLemmas.items(), key=lambda x:x[1] , reverse=True)][:10] ) )

    print( "\n#Stems found for unk words", len( foundStems ) )

def _addTranslation( cur_info, lex, words, unkWords, lemmas, unkLemmas, foundStems, stemmer, lex_stem ):
    form = cur_info[4]
    form_translated = translate( form, lex )
    cur_info.append( form_translated )

    words.add( form )
    if form_translated == 'NONE':
#         unkWords.add( form )
        if form in unkWords:
            unkWords[form] += 1
        else:
            unkWords[form] = 1

    lemma = cur_info[5]
    if lemma == '_':
        lemma_translated = 'NONE'
    else:
        lemma_translated = translate( lemma, lex )
    cur_info.append( lemma_translated )

    lemmas.add( lemma )
    if lemma_translated == 'NONE':
        if lemma in unkLemmas:
            unkLemmas[lemma] += 1
        else:
            unkLemmas[lemma] = 1

    if stemmer != None:#form_translated == 'NONE' and lemma_translated == 'NONE' and
        stem = stemmer.stemWords([form])[0]
        stem_translated = translate_stem( stem, lex_stem ) # TODO stem all the lex..
        if stem_translated != 'NONE':
            foundStems.add( form )
#         print( form, stem, stem_translated )
        cur_info.append( stem_translated )
    else:
        cur_info.append( 'NONE' )#if no stemmer


def translate( wd, lex ):
    if wd.lower() in string.punctuation:#keep punctuation
        return wd
    elif wd in lex:
        return lex[wd][0]#no disambiguation
    elif wd.lower() in lex:#try lowercase
        return lex[wd.lower()][0]
    elif wd[:-1] in lex:#try to deal with plural at least
        return lex[wd[:-1]][0]
    elif wd[0].isupper():#after checking that the str.lower() in not in lex, keep proper names
        return wd
    return 'NONE'

def translate_stem( stem, lex ):
    for wd in lex:
        if wd == stem:
            return lex[wd][0]
    return 'NONE'

# ----------------------------------------------------------------------------------
# def translate( in_path, out_path, dictionnary, ext=".en.edus" ):
#     lex = read_dict( dictionnary )
#
#     for subdir in in_path:
#         if subdir != None:
#             print( "\nReading ", subdir, file=sys.stderr )
#             out_subdir = os.path.join( out_path, os.path.basename( subdir ) )
#             if not os.path.isdir( out_subdir ):
#                 os.mkdir( out_subdir )
#
#             write_translated( subdir, out_subdir, lex, ext=ext )

# def write_translated( path, out_translated, lex, ext=".en.edus" ):
#     file_count = 0
#     unk_words = set()
#     all_words = set()
#     for _file in os.listdir( path ):
#         if not _file.startswith('.') and _file.endswith( ".edus" ):
#             out_path = os.path.join( out_translated, _file.replace( '.edus', ext ) )
#             o = codecs.open( out_path, 'w', encoding='utf8' )
#             c = codecs.open( os.path.join( path, _file ), encoding='utf8' )
#             l = c.readline()
#             while l:
#                 edu_text = l.strip()
#                 trans_edu_text = ''
#                 for wd in edu_text.split():
#                     wd = unicode( wd.strip() )
#                     all_words.add( wd )
#                     translated_wd = get_translation( wd, lex )
#                     if translated_wd == None:
#                         trans_edu_text += 'UNK '
#                         unk_words.add( wd )
#                     else:
#                         trans_edu_text += translated_wd+' '
#                 o.write( trans_edu_text.strip()+'\n' )
#                 l = c.readline()
#             c.close()
#             o.close()
#     print( '#Words=', len( all_words ), '#Unknown words=', len( unk_words ), file=sys.stderr )


# ----------------------------------------------------------------------------------
def read_dict( dictionnary, reverse=False ):
    lex = {}
    with open( dictionnary, encoding='utf8' ) as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            # Using pt-en dict, should I use en-pt rather?
            foreign_wd, english_wd = l.split('|||')
#             foreign_wd, english_wd = l.split('|||')
            foreign_wd = foreign_wd.strip()
            english_wd = english_wd.strip()
            if foreign_wd in lex:
                lex[foreign_wd].append( english_wd )
            else:
                lex[foreign_wd] = [english_wd]

#             print( foreign_wd, english_wd )
#
    # Reverse dict? for now, no dict foreign -> english for some languages
    if reverse:
        lex = {v:k for (k,v) in lex.items()}
    return lex

def stemLex( lex, stemmer ):
    lex_stem = {}
    for wd in lex:
        stem = stemmer.stemWords([wd])[0]
        if stem in lex_stem:
            lex_stem[stem].extend( lex[wd] )
        else:
            lex_stem[stem] = lex[wd]
    return lex_stem

if __name__ == '__main__':
    main()

