#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Check whether the parser added sentence split
Input: parsed data split into gold sentences and parsed (one file per sentence)
Output: parsed data without predicted sentence split into the gold sentences
'''


import argparse, os, sys


def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the BIO format to train a segmenter.')
    parser.add_argument('--parsed',
            dest='parsed',
            action='store',
            help='Treebank in conllu format, 1 file per sentence.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Parsed data with predicted sentence split removed (1 file per sent).')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    check( args.parsed, args.outpath )

def check( parsed, outpath ):
    for dset in [d for d in os.listdir( parsed ) if not d.startswith( '.')]:
        print( "Reading", dset )
        count_errors = 0
        if not os.path.isdir( os.path.join( outpath, dset ) ):
            os.mkdir( os.path.join( outpath, dset ) )
        for f in [_f for _f in os.listdir( os.path.join( parsed, dset ) ) if not _f.startswith( '.')]:
            with open( os.path.join( parsed, dset, f ) ) as myfile:
                lines = myfile.readlines()
                o = open( os.path.join( outpath, dset, f ), 'w' )
                # we just don t want any new sentence so keep increasing the token id and copy the lines without blank lines
                tok_id = 1
                for i,l in enumerate( lines ):
                    l = l.strip()
                    if l == '':# and i+1 < len( lines ):#new sentence
                        #print( l, i, len(lines) )
                        #sys.exit( "Add sent:"+f )
                        if i+1 < len( lines ):
                            count_errors += 1
                        continue
                    #tidtok, rest = l.split('\t')[0], l.split('\t')[1:]
                    o.write( str(tok_id)+'\t'+'\t'.join( l.split('\t')[1:] )+'\n' )
                    tok_id += 1
                o.close()
        print( '#New sentences created', count_errors )

if __name__ == '__main__':
    main()

