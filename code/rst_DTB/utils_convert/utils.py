#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import argparse, os, sys, shutil, codecs, string
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer

# ----------------------------------------------------------------------------------
def set_path( dtreebank ):
    train_path = os.path.join( dtreebank, "train" )
    test_path = os.path.join( dtreebank, "test" )
    dev_path = os.path.join( dtreebank, "dev" )
    if os.path.isdir( test_path ):
        if os.path.isdir( dev_path ):
            return train_path, test_path, dev_path, None, None
        else:
            return train_path, test_path, None, None, None
    else:
        # if we re dealing with the spanish dtb
        testa_path, testb_path = None, None
        if os.path.isdir( os.path.join( dtreebank, "testa" ) ):
            testa_path = os.path.join( dtreebank, "testa" )
        if os.path.isdir( os.path.join( dtreebank, "testb" ) ):
            testb_path = os.path.join( dtreebank, "testb" )
        if os.path.isdir( dev_path ):
            return train_path, None, dev_path, testa_path, testb_path
        else:
            return train_path, None, None, testa_path, testb_path

# ----------------------------------------------------------------------------------
def get_edus( in_edu_path, original_edus ):
    ''' file --> [ list of translated EDUs, list of original EDUs ]  '''
    file2edu = {}
    for i,subdir in enumerate( in_edu_path ):
        if subdir != None:
            print( "Get original EDUs for", subdir, file=sys.stderr )
            for f in os.listdir( subdir ):
                if not f.startswith( '.' ) and f.endswith( 'edus' ):
                    if original_edus != None:
                        original_edu = find_file( os.path.basename( f ), original_edus[i] )
                        file2edu[f] = [read_edus( os.path.join( subdir, f ) ), read_edus( original_edu )]
                    else:
                        file2edu[f] = [read_edus( os.path.join( subdir, f ) ), None]
    return file2edu

def read_edus( path ):
    edus = []
#     with codecs.open( path, encoding='utf8' ) as f:
    with open( path, encoding='utf8' ) as f:
        lines = f.readlines()
        for l in lines:
#             l = unicode( l.strip() )
            l = l.strip()
            edus.append( l )
    return edus

def find_file( basename, original_edus ):
    for f in os.listdir( original_edus ):
        if f.endswith( 'edus' ) and os.path.basename( f ).split('.')[0] == basename.split('.')[0]:
            #print( f, basename )
            return os.path.join( original_edus, f )
    sys.exit( "File not found for", basename, "in", original_edus )



# ----------------------------------------------------------------------------------
def fit_representation( train_path, n_components=50 ):
    # Vectorize
    # -- One hot
    vectorizer, X_train = build_vectorizer( train_path )
    # Dim red
    # -- SVD
    transformer = build_svd_transformer( X_train, n_components=n_components )
    return vectorizer, transformer

# -- Fit on train
def build_vectorizer( train_path ):
    edus = []
    for _file in os.listdir( train_path ):
        if _file.endswith( ".edus" ):
            c = codecs.open( os.path.join( train_path, _file ), encoding='utf8' )
            edus.extend( [l.strip() for l in c.readlines()] )
            c.close()
    print( "#TRAIN EDUS: ", str( len( edus ) ) )
    # --
    vectorizer = CountVectorizer(min_df=1)
    X = vectorizer.fit_transform(edus)
#     print( "Count Vec", X.shape )
    return vectorizer, X


# --
def build_svd_transformer( X_train, n_components=50 ):
    svd = TruncatedSVD(n_components=n_components, random_state=42)
    svd.fit( X_train )
    return svd



# ----------------------------------------------------------------------------------
def embed_files( vectorizer, transformer, in_edu_path, original_edus, out_path, n_components=50, sep='__' ):
    o = codecs.open( out_path, 'w', encoding='utf8' )
    count_file = 0
    for _file in os.listdir( in_edu_path ):
        if not _file.startswith( '.' ) and _file.endswith( 'edus' ):
            if count_file != 0:
                o.write( '\n' )
#             c = codecs.open( os.path.join( in_edu_path, _file ), encoding='utf8' )
            c = open( os.path.join( in_edu_path, _file ), encoding='utf8' )
            lines = c.readlines()
            for l in lines:
#                 edu_txt = unicode( l.strip() )
                edu_txt = l.strip()
                X = vectorizer.transform( [edu_txt] )#!! need a list
                v = transformer.transform( X )
                o.write( ' '.join( [str(e) for e in v[0]] )+'\n' )
            c.close()
            count_file += 1
    o.close()

def build_embeddings( in_edu_path, original_edus, n_components=50, sep='__', emb_type="svd" ):
    embeddings = {}


    if emb_type == "svd":
        # Fit the dim red on the train, ie in_path[0]
        vectorizer, transformer = fit_representation( in_edu_path[0], n_components=n_components )

        # need to save the edus of each file, we transform the translated edus but we want
        # to keep the original ones in the files

        file2edus = get_edus( in_edu_path, original_edus )

        # Build a representation for each EDU, associate the original EDU to a repr built on the translation
        for _file in file2edus:
            translated_edus = file2edus[_file][0]
            original_edus = file2edus[_file][1]
            for i, e in enumerate( translated_edus ):
                original_edu = None
                if original_edus != None:
                    original_edu = original_edus[i]
                add_embeddings( vectorizer, transformer, translated_edus[i], original_edu, embeddings )
    elif emb_type == "soa":
        sys.exit( "not implemented yet" )
#         # Build a vector representing typical info used for discourse parsing
#         vectorizer, X_train = build_vectorizer( in_edu_path[0] )
#         print( X_train[0] )
#         print( X_train[0].shape[1] )
#         #print( vectorizer.vocabulary_ )
#         print( ' '.join( [vectorizer.vocabulary_[i] for i in range( X_train[0].shape[1] ) if X_train[0][i]!=0] ) )
    return embeddings

def write_embeddings( embeddings, out_path ):
    # Write the representation
    print( '#TOTAL EDUs', len( embeddings ), file=sys.stderr )
    o = codecs.open( out_path, 'w', encoding='utf8' )
    for e,v in embeddings.items():
        o.write( e+' '+' '.join( [str(i) for i in v] )+'\n' )
    o.close()

def add_embeddings( vectorizer, transformer, edu_en, edu_foreign, embeddings ):
    X = vectorizer.transform( [edu_en] )#!! need a list
    v = transformer.transform( X )
    if edu_foreign != None:
        edu_form = u'__'.join( edu_foreign.split() )
    else:
        edu_form = u'__'.join( edu_en.split() )
    if not edu_form in embeddings:
        embeddings[edu_form] = v[0]

