#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import os, sys, shutil, codecs
import numpy as np
from lxml import etree
from nltk.tokenize import treebank

from common import *
import data

TOKENIZER = treebank.TreebankWordTokenizer()

# binarization = {
#         'NN+':'right',
#         ''
#         }
#
# ----------------------------------------------------------------------------------
# READ
# ----------------------------------------------------------------------------------
def getCHNFiles( tbpath, ext=".chn" ):
    chnfiles = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                chnfiles.append( os.path.join( p, file ) )
    return chnfiles

def writeEdusChn( doc, ext, pathout, docno=-1 ):
    """
    Write files similar to the .edus files in the RST DT for the other RST Treebanks.

    doc: Document instance, contains info about the the tokens in each EDU
    forigin: the discourse file, keep the same basename and path
    ext: the original extension (ie .rs3) to be replaced by the new one (ie .edus)
    """
    edufile = os.path.join( pathout, os.path.basename( doc.path ).replace( ext, ".edus" ) )
    f = codecs.open( edufile, 'w', encoding="utf8" )
    for edu in doc.edudict:
        f.write( doc.edudict[edu].strip()+"\n" )
    f.close()


# def parseXML( rs3file ):
#     if not os.path.isfile( rs3file ):
#         sys.exit( "Path file error: "+self.path )
#     for enc in ["utf8","iso-8859-3","windows-1250", "gb2312"]:# spanish RST DT:"iso-8859-3", basque:None, CST:"windows-1250"
#         try:
#             xml_parser = etree.XMLParser( encoding=enc )
#             rs3_xml_tree = etree.parse( rs3file, xml_parser )
#             doc_root = rs3_xml_tree.getroot()
#             return doc_root, rs3_xml_tree
#         except:#TODO catch the right exception..
#             continue
#     try:
#         xml_parser = etree.XMLParser( )
#         rs3_xml_tree = etree.parse( rs3file, xml_parser )
#         doc_root = rs3_xml_tree.getroot()
#         return doc_root, rs3_xml_tree
#     except:#TODO catch the right exception..
#         sys.exit("Unable to read file: "+rs3file)
#     return None, None
#
# # TODO ignore empty relation + make some corrections see the Spanish relation set (readme) + GUM + need a mapping for the multiling
# def getRelationsType( rs3_xml_tree ):
#     relations = {}
#     for rel in rs3_xml_tree.iterfind('//header/relations/rel'):
#         relName = rel.attrib['name'].replace(' ', '-' )#.encode("utf8")
#         if 'type' in rel.attrib:
#             if relName in relations:
#                 relations[relName].add( rel.attrib['type'] )
#             else:
#                 relations[relName] = set()
#                 relations[relName].add( rel.attrib['type'] )
#         else:
#             continue #Ignore 'schema' see for ex the Basque corpus SENTARG01-A1.rs3 sentiment schema
#     for r in relations:
#         relations[r] = list( relations[r] )
#     return relations
#
# def readRS3Annotation( doc_root ):
#     '''
#         a group's ``type`` attribute tells us whether the group
#         node represents a span (of RST segments or other groups) OR
#         a multinuc(ular) relation (i.e. it dominates several RST nucleii).
#
#         a group's ``relname`` gives us the name of the relation between
#         the group node and the group's parent node.
#
#     '''
#     eduList = []
#     groupList = []
#     # - EDU
#     eduIds = []
#     for i,segment in enumerate( doc_root.iter('segment') ):
#         if i == 0 and not 'parent' in segment.attrib:# Ignore the 1st for the Postdam corpus (title)
#             continue
#         # Get information for the current segment
#         id_ = int(segment.attrib['id'])
#         parent = int(segment.attrib['parent'])
#         relname = segment.attrib['relname'].replace(' ', '-' )#.decode( "iso-8859-3" )
#         edu = {"id":id_,
#                 "parent":parent,
#                 "relname":relname,
#                 "text":segment.text.strip().replace("\n", " "),#.decode("iso-8859-3"),
#                 "position":i}
#         if not id_ in eduIds:#Avoid the repeted segments
#             eduList.append( edu )
#             eduIds.append( id_ )
#     # - CDU
#     cduIds = []
#     for group in doc_root.iter('group'):
#         id_ = int(group.attrib['id'])
#         if 'parent' in group.attrib:
#             cdu = {"id":id_,
#                     "parent":int(group.attrib['parent']),
#                     "relname":group.attrib['relname'].replace(' ', '-' ),#.decode( "iso-8859-3" ),
#                     "type":group.attrib['type']}
#             if not id_ in cduIds:#Avoid the repeted segments
#                 groupList.append( cdu )
#                 cduIds.append( id_ )
#         else:
#             root = {"id":id_,
#                     "parent":None,
#                     "type":group.attrib['type']}
#             if not id_ in cduIds:#Avoid the repeted segments
#                 groupList.append( root )
#                 cduIds.append( id_ )
#     return eduList, groupList, root
#
#
#
#
# # ----------------------------------------------------------------------------------
# # EDU TEXT
# # ----------------------------------------------------------------------------------
# def retrieveEdu( tree, eduIds ):
#     """
#     Read information from the edus
#     Use the TOKENIZER to get tokens
#
#     tokendict: each token in the document, id in the doc -> form
#     edudict: each edu in the document, id in the tree -> list of token id
#     """
#     # Parcourir l arbre, recuperer les edu
#     gidx, tokendict, edudict = 0, {}, {}
#     for eidx in eduIds: #with this list, we are sure to have ordered Edus
#         eduNode = findNodeTree( eidx, tree )
#         if eduNode == None:
#             sys.exit( "EDU not found: "+str(eidx) )
#         text = eduNode.text
#         edudict[eidx] = []
#         tokens = TOKENIZER.tokenize( text )
#         for tok in tokens:
#             #tok = unicode(tok, "utf8")
#             tokendict[gidx] = tok
#             edudict[eidx].append( gidx )
#             gidx += 1
#     return tokendict, edudict
#
#
#
#
#
# # ----------------------------------------------------------------------------------
# # TREE UTILS
# # ----------------------------------------------------------------------------------
# def buildNodes( eduList, groupList, rootDict, relations ):
#     '''
#     Build a tree using the SpanNode class defined in DPLP
#
#     For each DU, retrieve: id, relation, prop (ie nucleus or satellite)
#     + for EDU: span
#     We add the span for the CDU later, need to be sure to have the EDU covered ordered
#     '''
#     renameDus( eduList, groupList, rootDict )# Rename EDUs and CDUs to get continuous indexing for EDUs
#
#     eduIds = [e["id"] for e in eduList] #Ordered EDUs
#     cduIds = [e["id"] for e in groupList] #CDUs
#     units = [e for e in eduList ]
#     units.extend( groupList ) #All DU
#
#     root = data.SpanNode( "Root" ) #Root node
#     root._id, root.eduSpan = rootDict["id"], tuple([eduIds[0], eduIds[-1]]) #Set the span for the root
#
#     # Build the other nodes
#     allNodes = [root]
#     for e in units:
#         # Check if the corresponding node has been built already (even an EDU can be a parent for now)
#         node = findNode( e["id"], allNodes )
#         if node == None:
#             #newNode = data.SpanNode( "Unk" ) #Prop is unknown for now
#             newNode = data.SpanNode( None ) #Prop is unknown for now
#             newNode._id, newNode.relation = e["id"], e["relname"]
#
#             #setNuclearity( newNode, relations )
#
#             if e["id"] in eduIds: # EDU ie isLeave
#                 newNode.text = e["text"]
#                 newNode.eduspan, newNode.nucspan = tuple( [e["id"], e["id"]] ),tuple( [e["id"], e["id"]] )
#                 newNode.nucedu = e["id"]
#                 newNode.position = e["position"]
#             allNodes.append( newNode )
#     # Update info for the parent
#     updateParentNodes( allNodes, units, eduIds )
#     updateNuclearityEDU( allNodes, units, relations )
#     return root
#
#
# def updateNuclearityEDU( allNodes, units, nucRelations ):
#     '''
#     - if an EDU is annotated with a multinuc relation, it s a nucleus
#     - if an EDU is annotated with a mononuc relation, it s a satellite and its parent a nucleus
#     - else, if an edu is annotated with span, it s a nucleus
#
#     -> Not enough for GUM corpus, because of rel that can be multi or mono TODO
#     (For the spanish RST DT, the initial problem came from the fact that same-unit was
#     present twice, reader relations modified)
#     '''
#     for n in allNodes:
#         # dictUnit["type"] == "multinuc" do not always correspond to all children being nucleus
#         # when a node have more than 2 children annotated with different relations
#         # for ex see Brazilian CST D2-C16
#         # This info seems redundant with the types of the relation defined in the list, but somes rel can be both
#         # -> Rely on the type of relation
#         if n.relation in nucRelations and len( nucRelations[n.relation] ) == 1:
#             if nucRelations[n.relation][0] == "multinuc":
#                 n.prop = 'Nucleus'
#             else:
#                 n.prop = 'Satellite' #in Discourse Graph, implies that the parent is a nucleus but not sure for the multi children cases
#                 parent = findParentNode( n, allNodes )
#                 if parent != None:
#                     parent.prop = 'Nucleus'
#         if n.prop == None and n.relation == "span":
#             n.prop = 'Nucleus'
#
#     for n in allNodes:
#         if n.prop == None:
#             if n.relation in nucRelations and "multinuc" in nucRelations[n.relation]:
#                 n.prop = 'Nucleus'
#
#                 print( "PROP CHOSOE NUC", n._id, n.prop, sorted([m._id for m in n.eduCovered]), n.relation )
#             else:
#                 print( "PROP UNDEF", n._id, n.prop, sorted([m._id for m in n.eduCovered]), n.relation )
# #             if n.relation in nucRelations:
# #                 print( '\t', nucRelations[n.relation] )
#
#
# def updateParentNodes( allNodes, units, eduIds ):
#     ''' Update eduCovered and nodelist '''
#     for e in units:
#         # retrieve the node
#         node = findNode( e["id"], allNodes )
#         # find the children
#         for f in units:
#             if f["parent"] == e["id"]:
#                 node.nodelist.append( findNode( f["id"], allNodes ) )
#     for n in allNodes:
#         # Begin directly with the children to not add the node itself if it s an EDU
#         n.eduCovered = getEduCoveredChildren( n, eduIds )
#
#
# def getEduCoveredChildren( node, eduIds ):
#     eduCovered = set()
#     queue = [m for m in node.nodelist if m._id != node._id]#seems to happen in the spanish RST DT
#     while queue:
#         n= queue.pop(0)
#         if n._id in eduIds:
#             eduCovered.add( n )
#
#         for m in n.nodelist:
#             queue.append( m )
#     return list(eduCovered)
#
# def renameDus( eduList, groupList, rootDict ):
#     '''
#     Change the ID of the EDUs in order to have a continuous list of ID
#     '''
#     eduIds = [e["id"] for e in eduList] #Ordered EDUs wr text
#     cduIds = [e["id"] for e in groupList] #CDU ids
#     cduIds.append( rootDict["id"] )
#     #newEdusIds = range( 1, max( eduIds )+1) #New list of EDU ids
#     newEdusIds = range( 1, len( eduIds )+1) #New list of EDU ids
#     if len( newEdusIds ) != len( eduIds ):
#         sys.exit( "Do not have the same number of old and new ids" )
#     if eduIds == newEdusIds:
#         return eduList, groupList
#     mappingEdus = {} #Mapping depending on the position of the EDU in the text
#     for i,e in enumerate( eduIds ):
#         mappingEdus[e] = newEdusIds[i]
#     intersec = list(set(newEdusIds) & set(cduIds)) #Check with CDU ids, may be changed
#     mappingCdus = {}# Mapping CDUs with non existing ids
#     if len( intersec ) != 0:
#         curId = sorted( list(set(eduIds) | set(cduIds)) )[-1]+1 #Be sure to not use the same id
#         for i in intersec:
#             mappingCdus[i] = curId
#             curId += 1
#     if len(list(set(mappingEdus.keys()) & set(mappingCdus.keys()))) != 0: #Check
#         print( mappingEdus, file=sys.stderr )
#         print( mappingCdus, file=sys.stderr )
#         sys.exit( "Overlap between EDU Ids and CDU Ids." )
#     # Merge the mappings
#     mappingDus = mappingEdus.copy()
#     mappingDus.update(mappingCdus)
#     # Change the id in the structure
#     for e in eduList:
#         if e["id"] in mappingDus:
#             e["id"] = mappingDus[e["id"]]
#         if e["parent"] in mappingDus:
#             e["parent"] = mappingDus[e["parent"]]
#     for e in groupList:
#         if e["id"] in mappingDus:
#             e["id"] = mappingDus[e["id"]]
#         if e["parent"] in mappingDus:
#             e["parent"] = mappingDus[e["parent"]]
#
#
# # def setNuclearity( newNode, relations ):
# #     #TODO should check if a node ha a brother that is also annotated with a relation, cause some rel are multinuc and mononuc
# #     if newNode.relation != "span": #If a node is annotated with a relation, it is either the satellite or the nucleu of a NN relation
# #         if "multinuc" in relations[newNode.relation]: # not totally True, + comparison on == doesnt work for Gum
# #             newNode.prop = "Nucleus"
# #         else:
# #             newNode.prop = "Satellite"
# #     else:
# #         newNode.prop = "Nucleus"
#
#
#
# def orderSpanList( tree, eduIds ):
#     # First sort node.eduCovered, and ordered eduSpan for each node
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         eduCovered = []
#         setEduCovered( node, eduIds, eduCovered )
#         node.eduCovered = sortEdu( eduCovered, eduIds )
#         node.eduspan = tuple( [node.eduCovered[0], node.eduCovered[-1]] )
#         for m in node.nodelist:
#             queue.append( m )
#     # Then, check order of nodes in node.nodelist
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         orderNodeList( node )
#         for m in node.nodelist:
#             queue.append( m )
#
#
# # Utils fct when we do not yet have a tree
# def findNode( id_, allNodes ):
#     for n in allNodes:
#         if n._id == id_:
#             return n
#     return None
#
# def findParentNode( child, allNodes ):
#     for n in allNodes:
#         if child in n.nodelist:
#             return n
#     return None
#
# def getParentDict( allGroup, parent ):
#     for g in allGroup:
#         if g["id"] == parent:
#             return g
#     return None
#
# def getParentNode( parent, allNodes ):
#     for n in allNodes:
#         if n._id == parent:
#             return n
#     return None
#
# # Utils fct for tree structure
# def findNodeTree( id_, tree ):
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if node._id == id_:
#             return node
#         for m in node.nodelist:
#             queue.append( m )
#     return None
#
# def getParentTree( child, tree ):
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if child._id in [m._id for m in node.nodelist]:
#             return node
#         for m in node.nodelist:
#             queue.append( m )
#     return None
#
#
#
#
#
# # ----------------------------------------------------------------------------------
# # TREE: Clean
# # ----------------------------------------------------------------------------------
#
# def cleanTree( tree, eduIds, relationSet, doc ):
#     '''
#     DPLP code deals with: retrieving prop info and moving the relation from the
#     children to the parent (see backprop()), and binarizing the tree (binarizeTree()).
#
#     But DPLP doesn t like:
#     - lonely child: a node with only one child (EDU or CDU)
#     - same-unit/embedded: this relation is treated differently at least in the Basque data,
#     we modify the tree in order to make it more similar to the RST DT.
#     '''
#     allId = list(getIdDu( tree ))
#     # 4 - DU with only one child
#     print( "Before cleaning:" )
#     printTreeRS3( tree )
#     print( "\n" )
#     cleanLonelyEDU( tree, eduIds, relationSet, allId )
#     cleanEDU( tree, eduIds, relationSet, allId )
#     print( "Cleaned EDU:" )
#     printTreeRS3( tree )
#     print( "\n" )
#     cleanLonelyCDU( tree, eduIds, relationSet )
#     print( "Cleaned CDU:" )
#     printTreeRS3( tree )
#     print( "\n" )
# # #     # 5 - Embedded relations
#     cleanEmbedded( tree, eduIds, relationSet, allId, doc )
# #     print( "Cleaned EMB:" )
# #     printRS3Tree( tree )
# #     print( "\n" )
# # #
#     cleanLonelyCDU( tree, eduIds, relationSet )
#     print( "Cleaned CDU:" )
#     printTreeRS3( tree )
#     print( "\n" )
#     # Check all nodelists are still ordered
#     orderSpanList( tree, eduIds )
#     tree.prop = "Root" #in case it has been changed somewhere..
#     print( "End:" )
#     printTreeRS3( tree )
#     print( "\n" )
#
#
#
# def cleanEmbedded( tree, eduIds, relationSet, allId, doc ):
#     '''
#     Only deals with problematic cases leading to a pb in the order of the units
#     - In general, we have Node_X ( Node_Y( e1-SU, e2-Ri, e4-SU ), e3-Rj )
#     with in the text the order e1 e2 e3 e4.
#     We put the e3 back as a child of Node_X and then binarize normally ( right attach )
#     - We have also more stranger cases
#
#     For now we do not annotate as embedded the embedded relations (not done in several corpora)
#     since this info is generally removed in the RST DT for parsing it
#     '''
#     queue = [tree]
#     while queue:
#         n = queue.pop(0)
#         if "same-unit" in [m.relation for m in n.nodelist]:
#             if not areAdjacent( [m for m in n.nodelist], eduIds ):
#                 # Cas general: un noeud manquand qui est le voisin du parent
#                 # -> added in n.nodelist
#                 id_linked = [m._id for m in n.nodelist]
#                 missing_nodes_id = [i for i in range( min( id_linked ), min( id_linked )+len( id_linked )+1 ) if not i in id_linked ]
#                 parent = getParentTree( n, tree )
#                 # check if these nodes are neighbors of the current node
#                 neighbors_id = [m._id for m in parent.nodelist if not m._id == n._id]
#                 check = True
#                 for i in missing_nodes_id:
#                     if not i in neighbors_id:
#                         check = False
# #                         print( "missing_nodes_id", missing_nodes_id )
# #                         print( "neighbors_id", neighbors_id )
# #                         print( "\n#######\nUnknown case for embedded relations" )
#                 if check:
#                     new_nodelist = []
#                     for m in parent.nodelist:
#                         if m._id in missing_nodes_id:
#                             n.nodelist.append( m )
#                             parent.nodelist.remove( m )
#                     orderSpanList( n, eduIds )
#                     # Can lead to a lonely CDU, a CDU with only one child, re apply clean lonely CDU after that
#                 # we don t do anything more, the tree will be removed as uncorrect later
# #                 # --> not sur how to interpret the structures..
# #                 else:
# #
# #                     #was ok for Doc TERM38-GS.rs3 nodes 10 to 14 but not for nodes 15 to 19 TODO
# #                     # idem, need to find the upper node covering all the range of EDUs
# #                     #- get all the needed EDU nodes
# #                     #- get the upper parent
# #                     #- put all these EDUs as children of this parent and remove the rest
# #                     id_linked.extend( missing_nodes_id )
# #                     id_linked = sorted( id_linked )
# #                     upper_node = getNodeCovering( tree, eduIds, id_linked )
# #                     if upper_node == None:
# #                         # try with one additional node
# #                         id_linked.append( max(id_linked)+1 )#only case found
# #                         id_linked = sorted( id_linked )
# #                         upper_node = getNodeCovering( tree, eduIds, id_linked )
# # #                         if upper_node == None:
# # #                             # continue adding ?
# # #                             sys.exit( "No covering node found for EDU "+' '.join( [str(i) for i in id_linked] ) )
# #                     if upper_node != None:
# #                         new_nodelist = []
# #                         q = [upper_node]
# #                         while q:
# #                             m = q.pop(0)
# #                             if m._id in eduIds:
# #                                 new_nodelist.append( m )
# #                             for h in m.nodelist:
# #                                 q.append( h )
# #                         upper_node.nodelist = new_nodelist
# #                         orderSpanList( upper_node, eduIds )
#         for m in n.nodelist:
#             queue.append( m )
#
#
#
# def getNodeCovering( tree, eduIds, id_linked ):
#     queue = [tree]
#     while queue:
#         n = queue.pop(0)
#         eduCovered = getEduCovered( n, eduIds )
#         if id_linked == sorted( [m._id for m in eduCovered] ):
#             return n
#         for m in n.nodelist:
#             queue.append( m )
#     return None
#
#
#
# def cleanEmbedded_( tree, eduIds, relationSet, allId ):
#     '''
#     Basque RSTDT, several cases:
#         (1) Parent -> X_SU Y_SU .. Z_SU : normal case, do nothing, nodes have children
#         with a node RI. We note subnodes with R_k relations as embedded.
#
#         (2) Parent -> X_SU Y_SU M_Ri .. N_Rj : similar to case (1), make a node above
#         the two SU nodes and then binarize normally (TO CHECK)
#         Search for embedded rel in X and Y
#
#         (3) Parent -> X_SU M_Ri .. N_Rj T_SU : typical case, means that all EDU between
#         X and T are in a Same Unit, with embedded relations inbetween, need to put
#         these relations as child, keeping the structure. The R_k relations are
#         between an EDU and the SU structure (all the others EDU), we keep the EDU as
#         a child and put another EDU as a child (kind of artificial, but see RST DT
#         manual). We note R_k relations as embedded
#
#         (3') Parent -> X_SU M_Ri Y_SU Z_SU :  not a clear case, Y and Z should probably
#         be merged, but we don t want to modify the annotated relations. We thus try
#         to stay coherent with the 1st case ending with: SU( Ri(X, M), SU(Y, Z) )
#         We mark Ri as embedded.
#
#         Check if we have X_SU Y_Ri Z_SU T_Rj with Rj not embedded
#     '''
#     queue = [tree]
#     while queue:
#         n = queue.pop(0)
#         if "same-unit" in [m.relation for m in n.nodelist]:
#             sameunitNodes = [m for m in n.nodelist if m.relation.lower() == "same-unit"]
#             ##print( [(m.eduspan,m.relation) for m in n.nodelist] )
#             # Look for the other node (the embedded one)
#             otherNode = [m for m in n.nodelist if m.relation.lower() != "same-unit"]
#             if len( otherNode ) == 0:# just mark as embedded the relations under the SU nodes
#                 for m in sameunitNodes:
#                     _markEmbed( m )
#             else:
#                 if areAdjacent( sameunitNodes, eduIds ):# (2) the SU nodes are all adjacent
#                     # Make a new node with all these SU nodes as children, then let binarize
#                     newNode = data.SpanNode( "Nucleus" ) #if its neighbor has the rel, it s a satellite
#                     newNode._id = allId[-1]+1
#                     allId.append( newNode._id )
#                     # no need to put the relation, propagated after (should be same unit)
#                     newNode.nodelist = sameunitNodes
#                     n.nodelist = [newNode]
#                     n.nodelist.extend( otherNode )
#                     eduCovered = []
#                     setEduCovered( newNode, eduIds, eduCovered )
#                     newNode.eduCovered = sortEdu( eduCovered, eduIds )
#                     newNode.eduspan = tuple( [newNode.eduCovered[0], newNode.eduCovered[-1]] )
#                     _markEmbed( newNode ) # mark as embedded all Rk in the subnodes
#                     queue.append( n )
#                 else:# (3) wa have DU_SU DU_Rk DU_SU
#                     # new node with the first 2 children, parent has this newnode as lnode the rest as rnode
#                     newNode = SpanNode( "Nucleus" ) #if its neighbor has the rel, it s a satellite
#                     newNode._id = allId[-1]+1
#                     allId.append( newNode._id )
#                     newNode.nodelist = n.nodelist[:2]
#                     eduCovered = []
#                     setEduCovered( newNode, eduIds, eduCovered )
#                     newnode.eduCovered = sortEdu( eduCovered, eduIds )
#                     newnode.eduspan = tuple( [newnode.eduCovered[0], newnode.eduCovered[-1]] )
#                     n.nodelist = [newNode]
#                     n.nodelist.extend( n.nodelist[2:] )
#                     _markEmbed( n )
#                     queue.append( n )
#             for m in n.nodelist:
#                 queue.append( m )
#
#
# def cleanEDU( tree, eduIds, relationSet, allId  ):
#     """
#     EDU with more than one child.. Found in the CSTNews corpus (doc D5_C11_GPovo):
#     EDU_1 -> 24_elab 5_elab with 24 a CDU
#     Massive phenomena in fact at least in Brazilian and German
#
#     Need to be careful keeping adjacency: the parent EDU can be before its children, or in between
#
#     The nodes have already been ordered, thus the right most is after the left most
#     ? the parent could be after its children ?
#
#     Means that one argument is shared:
#         Edu1 -> Elab(2-4) Elab(5)  ==> Elab( 1, 2-4 ) Elab( 1, 5 )
#         -> Create a group that will replace Edu1 with relation span,
#             Create a group that replace the CDU and has Edu1 and the original Cdu as children
#             newCDU_span -> newCDU_span( Edu1_span, Elab(2-4) ) Edu5_elab
#             (Edu1_span = nucleus of the relation)
#
#     """
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if node._id in eduIds and len( node.nodelist ) > 1:
# #             if node.relation != "span":
# #                 print( "\nEDU with several children (no span)", node._id, node.relation )
# #                 print( [m.relation for m in node.nodelist] )
# #                 print( [m.eduspan for m in node.nodelist] )
#
#             newnode = data.SpanNode( node.prop )#Keep the nuclearity of the group
#             newnode.nodelist = [node]
#             newnode.nodelist.extend( [m for m in node.nodelist] )
#             node.nodelist = []#an EDU has no children
#             newnode.relation = "span"
#
#             eduCovered = []
#             setEduCovered( newnode, eduIds, eduCovered )
#             newnode.eduCovered = sortEdu( eduCovered, eduIds )
#             newnode.eduspan = tuple( [newnode.eduCovered[0], newnode.eduCovered[-1]] )
#             newnode._id = node._id+200
#
#             # Put this node in place of node
#             parent = getParentTree( node, tree )
#             parent.nodelist.remove( node )
#             parent.nodelist.append( newnode )
#
#             # left attach
#             left_node = data.SpanNode( "Nucleus" )#Has to be a nucleus to give the correct interpretation
#             left_node.relation = "span"
#             left_node.nodelist = newnode.nodelist[:-1]
#             newnode.nodelist = [left_node, newnode.nodelist[-1]]
#
#             eduCovered = []
#             setEduCovered( left_node, eduIds, eduCovered )
#             left_node.eduCovered = sortEdu( eduCovered, eduIds )
#             left_node.eduspan = tuple( [left_node.eduCovered[0], left_node.eduCovered[-1]] )
#             left_node._id = node._id+201
#
#             orderSpanList( tree, eduIds )
#
#             newnode.nodelist = sorted( newnode.nodelist, lambda x,y:cmp(x.eduspan[0],y.eduspan[0]) )
#             left_node.nodelist = sorted( left_node.nodelist, lambda x,y:cmp(x.eduspan[0],y.eduspan[0]) )
#             parent.nodelist = sorted( parent.nodelist, lambda x,y:cmp(x.eduspan[0],y.eduspan[0]) )
#
# #             print( "\tNewnode", newnode._id, newnode.relation, newnode.eduspan )
# #             print( "\t", [m.relation for m in newnode.nodelist] )
# #             print( [m.eduspan for m in newnode.nodelist] )
# #
# #             print( "\n\tLeftnode", left_node._id, left_node.relation, left_node.eduspan )
# #             print( "\t", [m.relation for m in left_node.nodelist] )
# #             print( [m.eduspan for m in left_node.nodelist] )
#
#             queue.append( newnode )
#         else:
#             for m in node.nodelist:
#                 queue.append( m )
#
#
#
# def cleanLonelyEDU( rootNode, eduIds, relationSet, allId  ):
#     """
#     (1) EDU with no neighbor: the parent CDU receives this EDU as lnode and its children
#     as rnode
#     (2) EDU with a neighbor: create an new artificial node with this EDU as lnode and its
#     children as rnode
#     """
#     queue = [rootNode]
#     while queue:
#         n = queue.pop(0)
#         if n._id in eduIds and len( n.nodelist ) == 1:# Lonely child
#             child = n.nodelist[0]
#             parent = getParentTree( n, rootNode )
#
# #             print( "Lonely EDU", n._id, n.relation, n.eduspan, child._id, child.relation, child.eduspan )
#             if parent == None:
#                 sys.exit( "No parent found, but we are supposed to deal with an EDU ?!" )
#             # - EDU: 2 cas, soit l EDU a un voisin soit elle n en n a pas
# #             if n.relation != "span":
# #                 print( "LonelyEDU relation not span: "+n.__str__() )
# #                 print( "Parent info: "+str(parent._id)+" "+parent.__str__() )
# #                 print( "Child info: "+str( n.nodelist[0]._id )+" "+n.nodelist[0].__str__() )
# #                 #sys.exit( "EDU with one child with a relation different from span" )
#             if len( parent.nodelist ) > 1:#(2) EDU with neighbor
#                 # New CDU with the same parent as n and n and its children as children
#                 newnode = data.SpanNode( n.prop ) # TODO PROP ? "span"?
#                 newnode.relation = n.relation
#                 n.relation = "span"
#                 newnode.nodelist = [n]
#                 newnode._id = allId[-1]+1
#                 allId.append( newnode._id )
#                 newnode.nodelist += n.nodelist
#                 eduCovered = []
#                 setEduCovered( newnode, eduIds, eduCovered )
#                 newnode.eduCovered = sortEdu( eduCovered, eduIds )
#                 newnode.eduspan = tuple( [newnode.eduCovered[0], newnode.eduCovered[-1]] )
#                 # Ce nouveau noeud est tres probablement un nucleus ...
#                 # - posible que son voisin ne porte pas de relation ? TODO check
#                 newnode.prop = "Nucleus"
#                 newnode.relation = "span"
#                 index = parent.nodelist.index( n )
#                 parent.nodelist.remove( n )
#                 parent.nodelist.insert( index, newnode )
#                 queue.append( parent )
#             else: #(1) EDU with no neighbor
#                 if parent._id in eduIds:
#                     print( "Node:", n._id, "Parent:", parent._id, "Child:", child._id )
#                     sys.exit( "Edu with one child and no neighbors but whose parent is also en EDU")
#                 parent.nodelist.append( n.nodelist[0] )
#                 queue.append( parent )# just to be sure
#             n.nodelist = []
#             n.eduspan = tuple([n._id,n._id])
#         else:
#             for m in n.nodelist:
#                 queue.append( m )
#
#
# def cleanLonelyCDU( rootNode, eduIds, relationSet ):
#     """
#     1- CDU with 1 CDU child: the CDU child should have the span relation, can be safely removed
#     """
#     queue = [rootNode]
#     while queue:
#         n = queue.pop(0)
#         if len( n.nodelist ) == 1: #Lonely child
#             child = n.nodelist[0]
#             if not n._id in eduIds: #CDU
# #                 if child.relation != "span":
# #                     print( "Only child of a CDU with a relation different from span", file=sys.stderr )
# #                     print( "Current CDU info: "+n.__str__()+" "+str(n._id), file=sys.stderr )
# #                     print( "Child info: "+str( n.nodelist[0]._id )+" "+n.nodelist[0].__str__(), file=sys.stderr )
# #                     #sys.exit( "Only child of a CDU with a relation different from span" )
#                 if not child._id in eduIds:
#                     # We have parent -> n (a cdu) -> another cdu -> its children
#                     # We want parent -> n -> its children
#                     n.nodelist = child.nodelist
#                 else:# the child is an EDU
#                     # We have parent -> a cdu -> an edu
#                     # We want parent -> an edu
#                     parent = getParentTree( n, rootNode )
#                     parent.nodelist.remove( n )
#
#                     if len( child.nodelist ) == 0:
#                         if child.relation.lower() == 'span' and n.relation.lower() != 'span': #only if the EDU has no child?
#                             child.relation = n.relation
#                             child.prop = n.prop
#                         parent.nodelist.append( child )
#                     else:
#                         parent.nodelist.extend( child.nodelist )
#                 for m in n.nodelist:
#                     queue.append( m )
#         else:
#             for m in n.nodelist:
#                 queue.append( m )
#
# # TODO apparently node.eduCovered is not updated somewhere, remove the field or update
# def areAdjacent( sameunitNodes, eduIds ):
#     eduCovered = []
#     for n in sameunitNodes:
#         eduCovered.extend( getEduCovered( n, eduIds ) )
#     idEduCovered = [m._id for m in eduCovered]
#     if sorted( idEduCovered ) == range( min( idEduCovered ), max( idEduCovered )+1 ):
#         return True
#     return False
#
# def setEduCovered( n, eduIds, eduCovered ):
#     if n._id in eduIds:
#         eduCovered.append( n )
#     for m in n.nodelist:
#         setEduCovered( m, eduIds, eduCovered )
#
# def getEduCovered( tree, eduIds ):
#     eduCovered = []
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if node._id in eduIds:
#             eduCovered.append( node )
#         for m in node.nodelist:
#             queue.append( m )
#     return eduCovered
#
# def _markEmbed( tree ):
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if node.relation != None and node.relation.lower() != "span" and node.relation.lower() != "same-unit" and node.relation.lower()[-2:] != "-e":
#             node.relation += "-e"
#         for m in node.nodelist:
#             queue.append( m )
#
# # TODO remove no longer usefule now that we rename the DU before
# def sortEdu( eduCovered, eduIds ):
#     # Only return a sorted list of id for now
#     positions = [eduIds.index(i) for i in [n._id for n in eduCovered]]
#     #print( "\nEDUCOVERED", [m._id for m in eduCovered] )
#     #print( "POSITIONS", positions )
#     sortedIds = [x for (y,x) in sorted(zip(positions,[n._id for n in eduCovered]))]
#     #print( "SORTED IDS", sortedIds )
#     return sortedIds
#
#
# def orderNodeList( node ):
#     node.nodelist = sorted( node.nodelist, lambda x,y:cmp( x.eduspan[0], y.eduspan[0] ) )
#
#
# # def getLeavesId( tree ):
# #     # Non binary tree, use nodelist ! be careful, at the beginning an EDU may have children
# #     idEdu = set()
# #     queue = [tree]
# #     while queue:
# #         n = queue.pop(0)
# #         if len(n.nodelist) == 0:
# #             idEdu.add( n._id )
# #         for m in n.nodelist:
# #             queue.append( m )
# #     return idEdu
#
# def getIdDu( tree ):
#     idDu = set()
#     queue = [tree]
#     while queue:
#         n = queue.pop(0)
#         idDu.add( n._id )
#         if n.nodelist:
#             for m in n.nodelist:
#                 queue.append( m )
#         else:
#             if n.lnode and n.rnode:
#                 queue.append( n.lnode )
#                 queue.append( n.rnode )
#     return idDu
#
#
#
#
# # ----------------------------------------------------------------------------------
# # TREE: BINARIZE
# # ----------------------------------------------------------------------------------
# def binarizeTreeGeneral(tree, doc, nucRelations=None):
#     """
#     Convert a general RST tree to a binary RST tree
#     < DPLP but modified: no more systematic right branching, depend of the relations
#     of the (children) nodes, keeping a right branching by default (ie if all nodes are NN)
#
#     Simple rules:
#     - if the last node is a nucleus or holds the span relation: RA
#     - if the last node is a satellite: left attach (pb: makes a diff with the RST DT for the S span S cases)
#
#     But to stay coherent with the always Right Attach in the Eng-RST DT, we use:
#     - if the last node is a nucleus or holds the span relation: RA
#     - if we have a sequence S N+ S, we don't care, both RA and LA are ok, so we choose the RA to be consistent with RSTDT
#     - else (ie if the last node is a satellite), LA
#
#     :type tree: instance of SpanNode
#     :param tree: a general RST tree
#     """
#     cases = set()
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         if len(node.nodelist) == 2:
#             node.lnode = node.nodelist[0]
#             node.rnode = node.nodelist[1]
#             # Parent node
#             node.lnode.pnode = node
#             node.rnode.pnode = node
#
#         elif len(node.nodelist) > 2:
#             childrenRelations = [m.relation for m in node.nodelist]
#             childrenNuclearity = [m.prop for m in node.nodelist]
#
#             # Simple rules
# #             if childrenNuclearity[-1].lower() == 'satellite':
# #                 newnode = leftAttach( node )
# #             else: #last node = nucleus or span relation
# #                 newnode = rightAttach( node )
#             # RST DT coherent rules
#             if childrenNuclearity[-1].lower() == 'nucleus' or childrenRelations[-1].lower() == 'span' or snsPattern( childrenRelations, childrenNuclearity ):#last node = nucleus or span relation or specific pattern 'S N+ S'
#                 newnode = rightAttach( node )
#             else:
#                 newnode = leftAttach( node )#last node = satellite except for the specific pattern
#
#         if node.lnode != None and node.rnode != None:
#             queue.append( node.lnode )
#             queue.append( node.rnode )
#
# def snsPattern( relations, nuclearity ):
#     # At least 3 nodes, a satellite at the beg and at the end, and only nuclei in between or span
# #     if len( relations ) != len( nuclearity ):#TODO Relations can be both mono and multinuclear, what to do ??
# #         # --> shouldn t be an isue here, we just focus on having nucleus or satellite not on the type of relation
# #         print( relations )
# #         print( nuclearity )
# #         sys.exit("A relation is mono and multi nuclear: "+' '.join( relations ))
#     if len( nuclearity ) < 3:
#         return False
#     if nuclearity[0].lower() != "satellite" or  nuclearity[-1].lower() != "satellite":
#         return False
#     if len( np.unique( nuclearity[1:-1] ) ) != 1:
#         # should be the case where we have (S N S) S: first LA then come back to RA (N generally span)
#         return False
#     if np.unique( nuclearity[1:-1] )[0].lower() != "nucleus" or not (len( np.unique( relations[1:-1] ) ) == 1 and np.unique( relations[1:-1] )[0].lower() == "span" ):
#         return False
#     return True
#
#
# def leftAttach( node ):
#
# #     print( "NODE", node._id, node.relation, [m._id for m in node.nodelist] )
#     node.rnode = node.nodelist.pop(-1)
#     newnode = data.SpanNode('Nucleus') #has to be a nucleus since we do it when the last/right node is a satellite and we have a NS rel (CHECK ? TODO)
#     newnode.nodelist += node.nodelist
#     # ADDED
#     newnode.eduspan = tuple( [newnode.nodelist[0].eduspan[0], newnode.nodelist[-1].eduspan[1]] )
#     # Left-branching
#     node.lnode = newnode
#     # ADDED
#     newnode._id = node._id+100
# #     print( "\tLeft Attach, create node", newnode._id, [m._id for m in newnode.nodelist] )
#     # Parent node
#     node.lnode.pnode = node
#     node.rnode.pnode = node
# #     print( "8NODE", node.rnode )
# #     print( "NEWNODE", newnode._id, newnode.relation, [m._id for m in newnode.nodelist] )
# #     print( "NODE", node._id, node.relation, [m._id for m in node.nodelist] )
# #     newnode.relation = "span"
#     return newnode
#
# def rightAttach( node ):
#     node.lnode = node.nodelist.pop(0)
#     newnode = data.SpanNode('Nucleus') # has to be a nucleus ince we do it when the last/right node is a nucleus node.nodelist[0].prop
#     newnode.nodelist += node.nodelist
#     # ADDED
#     newnode.eduspan = tuple( [newnode.nodelist[0].eduspan[0], newnode.nodelist[-1].eduspan[1]] )
#     # Right-branching
#     node.rnode = newnode
#     # ADDED
#     newnode._id = node._id+100
# #     print( "\tRight Attach, create node", newnode._id, [m._id for m in newnode.nodelist] )
#     # Parent node
#     node.lnode.pnode = node
#     node.rnode.pnode = node
#
# #     newnode.relation = "span"
#     return newnode
#
#
#
#
# # ----------------------------------------------------------------------------------
# # WRITE
# # ----------------------------------------------------------------------------------
#
# def writeEdus( doc, ext, pathout ):
#     """
#     Write files similar to the .edus files in the RST DT for the other RST Treebanks.
#
#     doc: Document instance, contains info about the the tokens in each EDU
#     forigin: the discourse file, keep the same basename and path
#     ext: the original extension (ie .rs3) to be replaced by the new one (ie .edus)
#     """
#     edufile = os.path.join( pathout, os.path.basename( doc.path.replace( ext, ".edus" ))  )
# #     print( pathout )
# #     edufile = forigin.replace( ext, ".edus" )
#     f = codecs.open( edufile, 'w', encoding="utf8" )
#     #with open( edufile, 'w', encoding="utf8" ) as f:
#     for edu in doc.edudict:
#         line = " ".join( [doc.tokendict[gidx] for gidx in doc.edudict[edu]] )
#         f.write( line.strip()+"\n" )
#     f.close()
#
# def writeEdusChn( doc, ext, pathout, docno=-1 ):
#     """
#     Write files similar to the .edus files in the RST DT for the other RST Treebanks.
#
#     doc: Document instance, contains info about the the tokens in each EDU
#     forigin: the discourse file, keep the same basename and path
#     ext: the original extension (ie .rs3) to be replaced by the new one (ie .edus)
#     """
#     edufile = os.path.join( pathout, "SENT-"+str(docno)+"-"+os.path.basename( doc.path+".edus" ) )
#     f = codecs.open( edufile, 'w', encoding="utf8" )
#     for edu in doc.edudict:
#         line = doc.edudict[edu]
#         #line = " ".join( [doc.tokendict[gidx] for gidx in doc.edudict[edu]] ) # no tokenization?
#         f.write( line.strip()+"\n" )
#     f.close()
#
#
#
# def printTreeRS3( tree ):
#     '''
#     Print a tree when a node is associated with a nodelist
#     '''
#     queue = [tree]
#     while queue:
#         n = queue.pop(0)
#         try:
#             print( "-->", n._id, n.relation, n.eduspan, n.prop, [m._id for m in n.nodelist] )
#         except:
#             print( "-->", n._id, n.relation.encode('utf8'), n.eduspan, n.prop, [m._id for m in n.nodelist] )
#         for m in n.nodelist:
#             queue.append( m )
#
#
#
#
#
#
# # ----------------------------------------------------------------------------------
# # Check
# # ----------------------------------------------------------------------------------
#
# # Doesn t work because the end of the backprop id done when we use parse(), need to check on the nltk tree
# def checkTreeRs3( tree, eduIds ):
#     idEduOrdered = []
#     queue=[tree]
#     while queue:
#         node = queue.pop(0)
#         if node._id != tree._id and not node.prop in ['Nucleus', 'Satellite']:#only the root has None prop
#             print( "Node prop unknown", node._id, node.eduspan )
#             return False
#         if node.lnode == None or node.rnode == None:
#             if not node._id in eduIds:
#                 print( "CDU with None children", node._id, node.eduspan, node.lnode, node.rnode )
#                 return False
#             else:
#                 if node.text == None:
#                     print( "EDU with None text", node._id, node.eduspan )
#                     return False
#                 if node._id in idEduOrdered:
#                     print( "EDU already seen", node._id, node.eduspan )
#                     return False
#                 idEduOrdered.append( node._id )
#         else:
#             # CDU
#             if node.relation == None:
#                 print( "None relation", node._id, node.eduspan )
#                 return False
#             else:
#                 if node.relation.lower() == "span":
#                     print( "Span relation kept", node._id, node.eduspan )
#                     return False
#     if idEduOrdered != range( 1, len( idEduOrdered ) ):
#         print( "Pb in EDU ids\n", idEduOrdered, "\n != ", range( 1, len( idEduOrdered ) ) )
#         return False
#
#     return True
#
# # ----------------------------------------------------------------------------------
# # Some stats
# # ----------------------------------------------------------------------------------
# def computeStatsRs3( tree, doc, edulist, nucRelations ):
#     '''
#     edulist: list of the EDU, for now EDU may have children, and they may cover several spans...
#     '''
#     eduIds = [e["id"] for e in edulist]
#
#     doc.statistics = {
#             '#EDU':0,
#             '#CDU':0,
#             '#BIN':0,
#             '#NOBIN':0,
#             '#CDU01CHILD':0,
#             '#EDUPAR':0,
#             'NOBIN-PATT':{}
#             }
#     queue = [tree]
#     while queue:
#         node = queue.pop(0)
#         queue += node.nodelist
#         if node._id in eduIds:
#             doc.statistics['#EDU'] += 1
#             if len( node.nodelist ) != 0:
#                 doc.statistics['#EDUPAR'] += 1
#         else:
#             doc.statistics['#CDU'] += 1
#             if len( node.nodelist ) < 2:
#                 doc.statistics['#CDU01CHILD'] += 1
#             elif len( node.nodelist ) == 2:
#                 doc.statistics['#BIN'] += 1
#             else:
#                 doc.statistics['#NOBIN'] += 1
#                 patt = ' '.join( [n.prop for n in node.nodelist] )
#                 if patt in doc.statistics['NOBIN-PATT']:
#                     doc.statistics['NOBIN-PATT'][patt] += 1
#                 else:
#                     doc.statistics['NOBIN-PATT'][patt] = 1
#
#
