#!/bin/bash


# Convert the discourse treebanks into files to be used with a parser
# 
# - First, we can translate the corpora
# 
#         python navi/discourse/code/rst_DTB/utils_convert/translate.py --dtreebank path_to_the_discourse_treebank/ --outpath out_directory_path --dictionnary path_to_derived-word-dicts_dict
# 
#     Will produce new .edus files in the outpath directory
# 
# - For dependency parsers, we need .conll files:
# 
#         python convert_dependency.py --dtreebank path_to_the_discourse_treebank --outpath out_directory_path
# 
#     Will produce one .conll file for each train/ and test/ set (for testa/ and testb/ for spanish dtb)
# 
# - For constituent parsers, we need .bracketed files grouped into one file + an embedding files containing each DU associated with its representation:
# 
#         python convert_constituent.py --dtreebank path_to_the_discourse_treebank --outpath out_directory_path








# TODO change the name of this script ..
# convert the corpora into the dependency format + build embeddings TODO split the code


# Exit immediately if a command exits with a non-zero status.
set -e


# TODO options
NCOMP=50 #for SVD


#CORPORA=data/experiments/eacl-16-multiling-rst/
CORPORA=$1

# Each directory must contain a train and ev a test subdir
BASQUE_RSTDT=${CORPORA}basque-rstdt/
BRAZILIAN_CST=${CORPORA}brazilian-cst/
ENGLISH_RSTDT=${CORPORA}english-rstdt/
ENGLISH_GUM=${CORPORA}/english-gum-rstdt/
GERMAN_PCC=${CORPORA}german-pcc/
ML_RSTDT=${CORPORA}basq_span_engl-rstdt/
SPANISH_RSTDT=${CORPORA}spanish-rstdt/

# Translated EDUs
#TRANS_BASQUE_RSTDT=${CORPORA}basque-rstdt/translated_edus/
TRANS_BRAZILIAN_CST=${CORPORA}brazilian-cst/translated_edus/
TRANS_GERMAN_PCC=${CORPORA}german-pcc/translated_edus/
#TRANS_ML_RSTDT=${CORPORA}basq_span_engl-rstdt/translated_edus/ #TODO
TRANS_SPANISH_RSTDT=${CORPORA}spanish-rstdt/translated_edus/

# Dictionnaries
DICT=data/derived-word-dicts/
#BASQUE_DICT=${DICT}/
BRAZILIAN_DICT=${DICT}/en-pt-enwiktionary.txt
GERMAN_DICT=${DICT}/en-de-enwiktionary.txt
SPANISH_DICT=${DICT}/en-es-enwiktionary.txt

# Constituent format
OUT_CONST_BASQUE=${BASQUE_RSTDT}constituent_format/
OUT_CONST_BRAZILIAN=${BRAZILIAN_CST}constituent_format/
OUT_CONST_ENGLISH_RSTDT=${ENGLISH_RSTDT}constituent_format/
OUT_CONST_ENGLISH_GUM=${ENGLISH_GUM}constituent_format/
OUT_CONST_GERMAN_PCC=${GERMAN_PCC}constituent_format/
OUT_CONST_ML_RSTDT=${ML_RSTDT}constituent_format/ #TODO
OUT_CONST_SPANISH_RSTDT=${SPANISH_RSTDT}constituent_format/

##OUT_CONST_ENGLISH_RSTDT=expe_data/test_parser_trance/data/


# Dependency format
OUT_DEP_BASQUE=${BASQUE_RSTDT}dependency_format/
OUT_DEP_BRAZILIAN=${BRAZILIAN_CST}dependency_format/
OUT_DEP_ENGLISH_RSTDT=${ENGLISH_RSTDT}dependency_format/
OUT_DEP_ENGLISH_GUM=${ENGLISH_GUM}dependency_format/
OUT_DEP_GERMAN_PCC=${GERMAN_PCC}dependency_format/
OUT_DEP_ML_RSTDT=${ML_RSTDT}dependency_format/ #TODO
OUT_DEP_SPANISH_RSTDT=${SPANISH_RSTDT}dependency_format/


# # # ---- TRANSLATE THE EDUs
# # # echo BASQUE
# # # python navi/discourse/code/rst_DTB/translate.py --dtreebank ${BASQUE_RSTDT}/train-test-basque-rstdt/ --outpath ${TRANS_BASQUE_RSTDT} --dictionnary ${BASQUE_DICT}
# # 
# # echo BRAZILIAN CST
# python navi/discourse/code/rst_DTB/utils_convert/translate.py --dtreebank ${BRAZILIAN_CST}/train-test-brazilian-cst/ --outpath ${TRANS_BRAZILIAN_CST} --dictionnary ${BRAZILIAN_DICT}
# # 
# # echo SPANISH
# python navi/discourse/code/rst_DTB/utils_convert/translate.py --dtreebank ${SPANISH_RSTDT}/train-test-spanish-rstdt/ --outpath ${TRANS_SPANISH_RSTDT} --dictionnary ${SPANISH_DICT}
# # 
# # echo GERMAN PCC
# python navi/discourse/code/rst_DTB/utils_convert/translate.py --dtreebank ${GERMAN_PCC}/train-test-german-pcc/ --outpath ${TRANS_GERMAN_PCC} --dictionnary ${GERMAN_DICT}
# # 
# # 
# # ---- CONSTITUENT FORMAT
NCOMP=50 # number of components for SVD
# # 
# echo BASQUE
# python navi/discourse/code/rst_DTB/translate.py --dtreebank ${BASQUE_RSTDT}/train-test-basque-rstdt/ --outpath ${OUT_CONST_BASQUE}

# echo BRAZILIAN CST
# python navi/discourse/code/rst_DTB/utils_convert/convert2brack_const.py --dtreebank ${BRAZILIAN_CST}/train-test-brazilian-cst/ --outpath ${OUT_CONST_BRAZILIAN} --edus ${TRANS_BRAZILIAN_CST} --ncomp ${NCOMP}
# 
# python navi/discourse/code/rst_DTB/utils_convert/text_format.py --inpath ${TRANS_BRAZILIAN_CST}/test/ --outpath ${OUT_CONST_BRAZILIAN}/test.en.edus


# echo SPANISH
# python navi/discourse/code/rst_DTB/utils_convert/convert2brack_const.py --dtreebank ${SPANISH_RSTDT}/train-test-spanish-rstdt/ --outpath ${OUT_CONST_SPANISH_RSTDT} --edus ${TRANS_SPANISH_RSTDT} --ncomp ${NCOMP}
# 
# python navi/discourse/code/rst_DTB/utils_convert/text_format.py --inpath ${TRANS_SPANISH_RSTDT}/testa/ --outpath ${OUT_CONST_SPANISH_RSTDT}/testa.en.edus
# 
# python navi/discourse/code/rst_DTB/utils_convert/text_format.py --inpath ${TRANS_SPANISH_RSTDT}/testb/ --outpath ${OUT_CONST_SPANISH_RSTDT}/testb.en.edus
# 
# 
# 
# 
# echo GERMAN PCC
# python navi/discourse/code/rst_DTB/utils_convert/convert2brack_const.py --dtreebank ${GERMAN_PCC}/train-test-german-pcc/ --outpath ${OUT_CONST_GERMAN_PCC} --edus ${TRANS_GERMAN_PCC} --ncomp ${NCOMP}
# 
# python navi/discourse/code/rst_DTB/utils_convert/text_format.py --inpath ${TRANS_GERMAN_PCC}/test/ --outpath ${OUT_CONST_GERMAN_PCC}/test.en.edus
# 
# 


# echo RST DT
# python navi/discourse/code/rst_DTB/utils_convert/convert2brack_const.py --dtreebank ${ENGLISH_RSTDT}/train-test-english-rstdt/ --outpath ${OUT_CONST_ENGLISH_RSTDT} --ncomp ${NCOMP} --emb_type svd
# 
# 
# python navi/discourse/code/rst_DTB/utils_convert/text_format.py --inpath ${OUT_CONST_ENGLISH_RSTDT}/test.brackets --outpath ${OUT_CONST_ENGLISH_RSTDT}/test.en.edus --mode eng




# # ---- DEPENDENCY FORMAT
echo BRAZILIAN CST
python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${BRAZILIAN_CST}/train-test-brazilian-cst/ --outpath ${OUT_DEP_BRAZILIAN} --edus ${TRANS_BRAZILIAN_CST} --ncomp ${NCOMP}
# 
# echo SPANISH
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${SPANISH_RSTDT}/train-test-spanish-rstdt/ --outpath ${OUT_DEP_SPANISH_RSTDT} --edus ${TRANS_SPANISH_RSTDT} --ncomp ${NCOMP}
# 
# echo GERMAN PCC
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${GERMAN_PCC}/train-test-german-pcc/ --outpath ${OUT_DEP_GERMAN_PCC} --edus ${TRANS_GERMAN_PCC} --ncomp ${NCOMP}
# 
# echo RST DT
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${ENGLISH_RSTDT} --outpath ${OUT_DEP_ENGLISH_RSTDT} --ncomp ${NCOMP}







# echo BASQUE
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${BASQUE_RSTDT}/train-test-basque-rstdt/ --outpath ${BASQUE_RSTDT} --ncomp ${NCOMP}

# echo BRAZILIAN CST
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${BRAZILIAN_CST}/train-test-brazilian-cst/ --outpath ${OUT_DEP_BRAZILIAN} --ncomp ${NCOMP}

# echo SPANISH
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${SPANISH_RSTDT}/train-test-spanish-rstdt/ --outpath ${SPANISH_RSTDT} --ncomp ${NCOMP}
# 
# echo GERMAN PCC
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${GERMAN_PCC}/train-test-german-pcc/ --outpath ${GERMAN_PCC} --ncomp ${NCOMP}
# 
# echo ENGLISH rst dt
# python navi/discourse/code/rst_DTB/utils_convert/convert2conll_dep.py --dtreebank ${ENGLISH_RSTDT}/train-test-english-rstdt/ --outpath ${ENGLISH_RSTDT}
# 

# echo MULTILING


# PB to define nuclearity of some nodes, TO CHECK
# echo GUM
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${ENGLISH_GUM} --outpath ${OUT_ENGLISH_GUM}

# python navi/discourse/code/rst_DTB/read_dis.py --treebank ${ENGLISH_RSTDT} --outpath ${OUT_ENGLISH_RSTDT} --mapping ${ENGLISH_RSTDT_MAPPING}


