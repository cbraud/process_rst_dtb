#!/bin/bash

# TODO change the extension of the conllu/ files (ie train/test/dev files in the conllu format, use.conllu rather than .parse)


# Exit immediately if a command exits with a non-zero status.
set -e

# src=navi/discourse/code/rst_DTB/
src=projects/navi/discourse/code/read-rstdt/rst_DTB/



# Preprocess the data, mapping the relation to a common set of 18 classes

# log=log_preprocess_corpora_060916.txt
log=log_preprocess_corpora_160718.txt


# TODO cant find the Basque dictionary
# ----------------------------------------------------------------------------------
### BASQUE
# data=/Users/chloe/data/discourse-rst/eu/
data=/home/cbraud/sandbox/basque-pardi/data/
original=${data}/original
echo Processing Basque corpus - ${original}

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/
conllt=${data}/conllut/

# model=tools_import/udpipe-ud-1.2-160523/basque-ud-1.2-160523.udpipe
model=/home/cbraud/data_tools/udpipe-ud-1.2-160523/basque-ud-1.2-160523.udpipe
#language=basque #no stemmer
dict=data/wiktionary/en-eu-enwiktionary.txt

# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping >${log}

## dmrg=/home/cbraud/sandbox/basque-pardi/data/original-basque-rstdt/

# echo ${const}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/ >>${log}

# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/ >>${log}

# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/ >>${log}

# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model} >>${log}

# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/ #>>${log}

# echo ${conllt}
# mkdir -p ${conllt}
# python ${src}/utils_convert/addTranslation.py --conllu ${conll} --dict ${dict} --outpath ${conllt}

# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conllt}
# cd

echo version without translation
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}
## cd projects/multilingual/script/
## parse=/home/cbraud/sandbox/basque-pardi/data/dep_feats/
## bash generate_discourse_data.sh ${data} ${parse}
# cd
# 



# ----------------------------------------------------------------------------------
### BRAZILIAN CST+SUMMIT+TCC+RHETALHO
data=/Users/chloe/data/discourse-rst/pt/
original=${data}/original
echo Processing Brazilian Portuguese corpus - ${original}

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/
conllt=${data}/conllut/

model=tools_import/udpipe-ud-1.2-160523/portuguese-ud-1.2-160523.udpipe
language=portuguese
dict=data/wiktionary/en-pt-enwiktionary.txt
# 
# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping
# 

# echo ${const}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/
# 
# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/
# 
# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/
# 
# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model}
# 
# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/
# 
# echo ${conllt}
# mkdir -p ${conllt}
#  python ${src}/utils_convert/addTranslation.py --conllu ${conll} --dict ${dict} --outpath ${conllt} --lang ${language}
# 
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conllt}
# cd
# 
# echo version without translation
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}
# cd
# 


# # ----------------------------------------------------------------------------------
### GERMAN
data=/Users/chloe/data/discourse-rst/de/
original=${data}/original
echo Processing German corpus - ${original}

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/
conllt=${data}/conllut/

model=tools_import/udpipe-ud-1.2-160523/german-ud-1.2-160523.udpipe
language=german
dict=data/wiktionary/en-de-enwiktionary.txt

# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping
# 
# echo ${cont}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/
# 
# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/
# 
# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/
# 
# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model}

# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/
# 
# echo ${conllt}
# mkdir -p ${conllt}
# python sandbox/read-rstdt/rst_DTB/utils_convert/addTranslation.py --conllu ${conll} --dict ${dict} --outpath ${conllt} --lang ${language}
# 
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conllt}
# cd
# 
# echo version without translation
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}
# cd
# 


#----------------------------------------------------------------------------------
### SPANISH
data=/Users/chloe/data/discourse-rst/es/
original=${data}/original
echo Processing Spanish corpus - ${original}

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/
conllt=${data}/conllut/

model=tools_import/udpipe-ud-1.2-160523/spanish-ud-1.2-160523.udpipe
language=spanish
dict=data/wiktionary/en-es-enwiktionary.txt

# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping
# 
# echo ${const}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/
# 
# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/
# 
# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/

# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model}
# 
# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/
# 
# echo ${conllt}
# mkdir -p ${conllt}
# python sandbox/read-rstdt/rst_DTB/utils_convert/addTranslation.py --conllu ${conll} --dict ${dict} --outpath ${conllt} --lang ${language}
# 
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conllt}
# cd
# 
# echo version without translation
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}
# cd



# ----------------------------------------------------------------------------------
### DUTCH
data=/Users/chloe/data/discourse-rst/nl/
original=${data}/original
echo Processing Dutch corpus - ${original}

dmrg=${data}/dmrg
const=${data}/constituent_format/
dep=${data}/dependency_format/
raw=${data}/raw/
parse=${data}/parse-ud/
conll=${data}/conllu/
conllt=${data}/conllut/

model=tools_import/udpipe-ud-1.2-160523/dutch-ud-1.2-160523.udpipe
language=dutch
dict=data/wiktionary/en-nl-enwiktionary.txt

# TODO : problem of number # at the beg of the EDU... deal with another script modify_edu but add something in read_rs3
# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping mapping

# echo ${const}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/
# 
# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/
# 
# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/
# 
# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model}
# 
# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/

# echo ${conllt}
# mkdir -p ${conllt}
# python ${src}/utils_convert/addTranslation.py --conllu ${conll} --dict ${dict} --outpath ${conllt} --lang ${language}
# 
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conllt}
# cd
# 
# 
# echo version without translation
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}
# cd
# 









# ----------------------------------------------------------------------------------
# ### ENGLISH RSTDT
# data=data/discourse-rst/en-rstdt
# original=${data}/original
# dmrg=${data}/dmrg-notmapped
# mkdir -p ${dmrg}
# 
# # - Read the corpus: from rs3/dis to bracketed
# python ${src}/read_dis.py --treebank ${original} --outpath ${dmrg} --mapping none
# #rstdt_mapping18
# 
# 
# # ----------------------------------------------------------------------------------
# ### ENGLISH GUM
# data=data/discourse-rst/en-gum
# original=${data}/original
# dmrg=${data}/dmrg
# mkdir -p ${dmrg}
# 
# # - Read the corpus: from rs3/dis to bracketed
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping rstdt_mapping18
# 
# 
# 
# # ----------------------------------------------------------------------------------
# ### ENGLISH INSTR doesn t work for now
# data=data/discourse-rst/en-instr
# original=${data}/original
# dmrg=${data}/dmrg
# mkdir -p ${dmrg}
# 
# # - Read the corpus: from rs3/dis to bracketed
# python ${src}/read_dis.py --treebank ${original} --outpath ${dmrg} --mapping rstdt_mapping18












#
# 
# ----------------------------------------------------------------------------------
# ### CHINESE DISC
# data=data/discourse-rst/ch-disc
# original=${data}/original
# dmrg=${data}/dmrg
# mkdir -p ${dmrg}
# 
# # # - Make one file for each tree and convert to utf8
# # python ${src}/split_tree_files.py --inpath ${original}/origin_file/Chinese_discourse_corpus-4747.txt --outpath ${original} --outext chn --inenc GB18030
# echo ${original}
# python ${src}/read_chn.py --treebank ${original} --outpath ${dmrg} --mapping chinese_labels
# 
# 
# 


# # # ----------------------------------------------------------------------------------
# # ### MULTILING
# # data=data/discourse-rst/ba_sp_en-rstdt
# # original=${data}/original
# # dmrg=${data}/dmrg
# # mkdir -p ${dmrg}
# # 
# # echo ${original}
# # python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping multiLing_labels
# # 
# # 


# ----------------------------------------------------------------------------------
### BRAZILIAN CST
# data=/Users/chloe/data/discourse-rst/br-cst/
# original=${data}/original
# 
# echo ${original}
# dmrg=${data}/dmrg
# echo ${dmrg}
# mkdir -p ${dmrg}
# python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping brazilianCst_labels
# 
# 
# const=${data}/constituent_format/
# echo ${const}
# mkdir -p ${const}
# python ${src}/utils_convert/convert2constituents.py --treebank ${dmrg} --outpath ${const} --split ${data}/files_list/
# 
# dep=${data}/dependency_format/
# echo ${dep}
# mkdir -p ${dep}
# python ${src}/utils_convert/convert2dependencies.py --treebank ${dmrg} --outpath ${dep} --split ${data}/files_list/
# 
# raw=${data}/raw/
# echo ${raw}
# mkdir -p ${raw}
# python ${src}/utils_convert/convert2raw.py --treebank ${dmrg} --outpath ${raw} --split ${data}/files_list/
# 
# parse=${data}/parse-ud/
# model=tools_import/udpipe-ud-1.2-160523/portuguese-ud-1.2-160523.udpipe
# echo ${parse}
# mkdir -p ${parse}
# bash ${src}/utils_convert/parse_ud.sh ${raw} ${parse} ${model}
# 
# conll=${data}/conllu/
# echo ${conll}
# mkdir -p ${conll}
# python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conll} --parse ${parse} --split ${data}/files_list/
# 
# cd discoursecph/multilingual/script/
# bash generate_discourse_data.sh ${data} ${conll}

# # # ----------------------------------------------------------------------------------
# # ### BRAZILIAN SUMMIT
# # data=data/discourse-rst/br-summit
# # original=${data}/original
# # dmrg=${data}/dmrg
# # mkdir -p ${dmrg}
# # 
# # echo ${original}
# # python ${src}/read_rs3.py --treebank ${original} --outpath ${dmrg} --mapping brazilianSum_labels
# # 
# # 
# # 

