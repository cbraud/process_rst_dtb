#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Convert the dataset into the format used by the parser
    - input: bracketed trees + edus files
    - output ?
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs, gzip, re
from sklearn.decomposition import TruncatedSVD
from scipy.sparse import csr_matrix
from sklearn.feature_extraction.text import CountVectorizer
from nltk import Tree

def main( ):
    parser = argparse.ArgumentParser(
            description='Convert the discourse dataset into the desired format for learning.')
    parser.add_argument('--dtb',
            dest='dtb',
            action='store',
            help='Discourse treebank (ie files .bracketed and .edus).')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Files in the desired format for learning')
    parser.add_argument('--ncomp',
            dest='ncomp',
            default=50,
            action='store',
            help='Number of components for dimensionality reduction')
    parser.add_argument('--translate',
            dest='translate',
            default=None,
            action='store',
            help='Dictionnary to translate text to English')
    args = parser.parse_args()

    if not os.path.isdir(  vars(args)['outpath'] ):
        os.mkdir(  vars(args)['outpath'] )

    convert( vars(args)['dtb'], vars(args)['outpath'],
            n_components=int(vars(args)['ncomp']),
            translate=vars(args)['translate'] )


# ----------------------------------------------------------------------------------
def convert( discTreebank, outpath, n_components=50, translate=None ):
    # If language != english (translate != none), translate the text of the EDU
    edu_path = discTreebank
    if translate != None:
        edu_path = translate_edu( discTreebank, outpath, translate )
    # Write files with the continuous representation for each EDU
    build_embeddings( edu_path, outpath, n_components=n_components )
    # Write files into ConLL format
    build_dependency( discTreebank, outpath ) #TODO need to add specific path to EDU or keep the original form in the conll?




# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
def translate_edu( discTreebank, outpath, dictionnary ):
    out_translated = os.path.join( outpath, "translated_en" )
    if not os.path.isdir( out_translated ):
        os.mkdir( out_translated )
    train_path = os.path.join( discTreebank, "train" )
    test_path = os.path.join( discTreebank, "test" )
    if not os.path.isdir( test_path ):
        test_path = None
        testa_path = None #to test if we re dealing with the spanish dtb
        if os.path.isdir( os.path.join( discTreebank, "testa" ) ):# and os.path.isdir( os.path.join( discTreebank, "testb" ) ):
            testa_path = os.path.join( discTreebank, "testa" )
            testb_path = os.path.join( discTreebank, "testb" )
    out_train = os.path.join( out_translated, "train" )
    if not os.path.isdir( out_train ):
        os.mkdir( out_train )
    if test_path != None:
        out_test = os.path.join( out_translated, "test" )
        if not os.path.isdir( out_test ):
            os.mkdir( out_test )
    else:
        out_testa = os.path.join( out_translated, "testa" )
        if not os.path.isdir( out_testa ):
            os.mkdir( out_testa )
        out_testb = os.path.join( out_translated, "testb" )
        if not os.path.isdir( out_testb ):
            os.mkdir( out_testb )


    lex = read_dict( dictionnary )

    write_translated( train_path, out_train, lex )
    if test_path != None:
        write_translated( test_path, out_test, lex )
    else:
        write_translated( testa_path, out_testa, lex )
#         write_translated( testb_path, out_testb, lex )

    return out_translated


def read_dict( dictionnary ):
    lex = {}
    with codecs.open( dictionnary, encoding='utf8' ) as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            foreign_wd, english_wd = l.split('|||')
            foreign_wd = unicode( foreign_wd.strip() )
            english_wd = unicode( english_wd.strip() )
            if foreign_wd in lex:
                lex[foreign_wd].append( english_wd )
            else:
                lex[foreign_wd] = [english_wd]
    return lex

def write_translated( path, out_translated, lex ):
    file_count = 0
    unk_words = 0
    for _file in os.listdir( path ):
        if not _file.startswith('.') and _file.endswith( ".edus" ):
            out_path = os.path.join( out_translated, _file.replace( '.edus', '.en.edus' ) )
            o = codecs.open( out_path, 'w', encoding='utf8' )
            c = codecs.open( os.path.join( path, _file ), encoding='utf8' )
            l = c.readline()
            while l:
                edu_text = l.strip()
                trans_edu_text = ''
                for wd in edu_text.split():
                    wd = unicode( wd.strip() )
#                     print( wd )
                    if wd.lower() in string.punctuation:
                        trans_edu_text += wd+' '
                    elif wd in lex:
                        trans_edu_text += lex[wd][0]+' '
                    elif wd.lower() in lex:
                        trans_edu_text += lex[wd.lower()][0]+' '
                    elif wd[:-1] in lex:
                        trans_edu_text += lex[wd[:-1]][0]+' '
                    else:
                        trans_edu_text += 'UNK '
                        unk_words += 1
                o.write( trans_edu_text.strip()+'\n' )
                l = c.readline()
            c.close()
            o.close()
    print( 'Unknown words=', unk_words )

# def read_dict( dictionnary ):
#     lex = {}
#     linked_forms = {}
#     pat = re.compile( '<.>' )
#     pat2 = re.compile( '\[.\]' )
#     c = gzip.open( dictionnary )
#     lines = c.readlines()
#     begin = False
#     i = 0
#     while i != len( lines )-1:
#         line = lines[i]
#         line = line.strip()
#         if '<' in line and '>' in line:
#             begin = True
#         if begin:
#             if not '<' in line and not '>' in line:
#                 i += 1
#             else:
#                 eng_form = line.split('<')[0].strip()
#                 if eng_form == '':
#                     sys.exit( "No eng form: "+line )
#                 if "SEE" in line:
#                     linked_form = line.split('SEE:')[-1].strip().replace('{', '').replace('}', '')
#                     if not eng_form in linked_forms:
#                         linked_forms[eng_form] = set()
#                     linked_forms[eng_form].add( linked_form )
#                 else:
#
#                     for_form = lines[i+1].strip()
#                     if for_form == '':
#                         sys.exit( "No for form: \n"+eng_form+" "+lines[i-1].strip()+"\n"+line+"\n"+lines[i+1].strip()+"\n"+lines[i+2].strip() )
# #                     mat = re.search( pat, for_form )
# #                     if mat:
# #                         for_form = for_form.replace( mat.group(), '' )
#
#                     for_forms = [f.strip() for f in for_form.split(',')]
#
#                     #print( eng_form, for_forms, '(', lines[i+1].strip(), ')' )
#                     # We want spanish o English dict
#                     for f in for_forms:
#                         f = f.strip()
#                         mat = re.search( pat, f )
#                         if mat:
#                             f = f.replace( mat.group(), '' )
#                         mat2 = re.search( pat2, f )
#                         if mat2:
#                             f = f.replace( mat2.group(), '' )
#                         f = f.strip()
#                         if f != '':
#                             if not f in lex:
#                                 lex[f] = set()
#                             lex[f].add( eng_form )
#                 i += 1
#         i += 1
# #         if i >= 30:
# #             sys.exit()
#     c.close()
#
#     for j,f in enumerate( sorted( lex ) ):
#         print( '\''+f+'\'', '\t', lex[f] )
#         if j == 10:
#             sys.exit()




# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
def build_embeddings( discTreebank, outpath, n_components=50 ):
    ''' Build a continuous representation for each EDU '''

    train_path = os.path.join( discTreebank, "train" )
    test_path = os.path.join( discTreebank, "test" )
    if not os.path.isdir( test_path ):
        test_path = None
        testa_path = None #to test if we re dealing with the spanish dtb
        if os.path.isdir( os.path.join( discTreebank, "testa" ) ) and os.path.isdir( os.path.join( discTreebank, "testb" ) ):

            testa_path = os.path.join( discTreebank, "testa" )
            testb_path = os.path.join( discTreebank, "testb" )

    # Vectorize
    # -- One hot
    vectorizer, X_train = build_vectorizer( train_path )

    # Dim red
    # -- SVD
    transformer = build_svd_transformer( X_train, n_components=n_components )


    # Write
    out_train_path = os.path.join( outpath, "train_svd-"+str(n_components)+".edus" )
    print( "Writing", out_train_path )
    write_transformed( vectorizer, transformer, train_path, out_train_path )

    if test_path != None:
        out_test_path = os.path.join( outpath, "test_svd-"+str(n_components)+".edus" )
        print( "Writing", out_test_path )
        write_transformed( vectorizer, transformer, test_path, out_test_path )
    elif testa_path != None:#spanish data
        out_testa_path = os.path.join( outpath, "testa_svd-"+str(n_components)+".edus" )
        out_testb_path = os.path.join( outpath, "testb_svd-"+str(n_components)+".edus" )
        print( "Writing", out_testa_path )
        write_transformed( vectorizer, transformer, testa_path, out_testa_path )
        print( "Writing", out_testb_path )
        write_transformed( vectorizer, transformer, testa_path, out_testb_path )

    if os.path.isdir( os.path.join( discTreebank, "dev" ) ):
        dev_path = os.path.join( discTreebank, "dev" )
        out_dev_path = os.path.join( outpath, "dev_svd-"+str(n_components)+".edus" )
        print( "Writing", out_dev_path )
        write_transformed( vectorizer, transformer, dev_path, out_dev_path )


# -- Fit on train
def build_vectorizer( train_path ):
    edus = []
    for _file in os.listdir( train_path ):
        if _file.endswith( ".edus" ):
            c = codecs.open( os.path.join( train_path, _file ), encoding='utf8' )
            edus.extend( [l.strip() for l in c.readlines()] )
            c.close()
    print( "#EDUS: ", str( len( edus ) ) )
    # --
    vectorizer = CountVectorizer(min_df=1)
    X = vectorizer.fit_transform(edus)
#     print( "Count Vec", X.shape )
    return vectorizer, X


# --
def build_svd_transformer( X_train, n_components=50 ):
    svd = TruncatedSVD(n_components=n_components, random_state=42)
    svd.fit( X_train )
    return svd


def write_transformed( vectorizer, transformer, path, out_path ):
    ''' One EDU per line, a linebreak between each document  '''
    o = codecs.open( out_path, 'w', encoding='utf8' )
    file_count = 0
    for _file in os.listdir( path ):
        if not _file.startswith('.') and _file.endswith( ".edus" ):
            if file_count > 0:
                o.write( '\n' )
            c = codecs.open( os.path.join( path, _file ), encoding='utf8' )
            l = c.readline()
            while l:
                l = l.strip()
                edu_text = l.strip()
                X = vectorizer.transform( [l] )#!! need a list
                v = transformer.transform( X )
                o.write( ' '.join( [str(e) for e in v[0]] )+'\n' )
                l = c.readline()
            c.close()
            file_count += 1
    o.close()



# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
def build_dependency( discTreebank, outpath ):
    train_path = os.path.join( discTreebank, "train" )
    test_path = os.path.join( discTreebank, "test" )
    if not os.path.isdir( test_path ):
        test_path = None
        testa_path = None
        if os.path.isdir( os.path.join( discTreebank, "testa" ) ) and os.path.isdir( os.path.join( discTreebank, "testb" ) ):
            test_path = None
            testa_path = os.path.join( discTreebank, "testa" )
            testb_path = os.path.join( discTreebank, "testb" )

    print( "Writing train.conll" )
    out_train_path = os.path.join( outpath, "train"+".conll" )
    _build_dependency( train_path, out_train_path )

    if test_path != None:
        out_test_path = os.path.join( outpath, "test"+".conll" )
        print( "Writing", out_test_path )
        _build_dependency( test_path, out_test_path )
    elif testa_path != None:
        out_testa_path = os.path.join( outpath, "testa"+".conll" )
        print( "Writing", out_testa_path )
        _build_dependency( testa_path, out_testa_path )
        out_testb_path = os.path.join( outpath, "testb"+".conll" )
        print( "Writing", out_testb_path )
        _build_dependency( testb_path, out_testb_path )

    if os.path.isdir( os.path.join( discTreebank, "dev" ) ):
        dev_path = os.path.join( discTreebank, "dev" )
        out_dev_path = os.path.join( outpath, "dev"+".conll" )
        print( "Writing", out_dev_path )
        _build_dependency( dev_path, out_dev_path )



def _build_dependency( path, out_path ):
    o = codecs.open( out_path, 'w', encoding='utf8' )
    count_file = 0
    for _file in os.listdir( path ):
        if not _file.startswith('.') and _file.endswith( ".bracketed" ):
#             print( _file )
            if count_file > 0:
                o.write( '\n' )
            edu_file =  os.path.join( path, _file.replace( ".bracketed", ".edus" ) )
            if not os.path.isfile( edu_file ):
                edu_file = os.path.join( path, '.'.join( _file.split('.')[:2] )+".edus" )
            if not os.path.isfile( edu_file ):
                edu_file = os.path.join( path,  _file.split('.')[0]+".edus" )
            if not os.path.isfile( edu_file ):
                sys.exit( "edu file not found "+edu_file)

            edu2text = getTextEdus( edu_file )
            tree = Tree.fromstring( open( os.path.join( path, _file ) ).read().strip() )
            root, dependencies = convert_dependency( tree, keep_nuc=True )
            #edus = sorted( [int(e) for e in tree.leaves()] )
            sorted_edus = sorted( edu2text.keys() )
            for edu in sorted_edus:#we want that index begins at 1
                if edu == root:

                    o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', '0', 'ROOT', '_', '_' ] )+'\n' )
                else:
                    head, relation = find_dep_link( edu, dependencies, sorted_edus )
                    o.write( '\t'.join( [str(edu), edu2text[edu], '_', '_', '_', '_', str(head), relation, '_', '_' ] )+'\n' )
            count_file += 1
    o.close()


def getTextEdus( filePath ):
    edu2text = {}
    eduId = 1
    c = codecs.open( filePath, encoding='utf8' )
    lines = c.readlines()
    c.close()
    for edu_txt in lines:
        edu_txt = edu_txt.strip()
        edu2text[eduId] = edu_txt.replace( ' ', '__' )
        eduId += 1
    return edu2text


def convert_dependency( tree, keep_nuc=False ):
    '''
     - choisir le root: premiere EDU qui est un nucleus
       (an EDU is a nucleus if it is in a sallient ensemble)
       --> [Un point interessant]_1 [parce qu'il definit  ...]_2 : root 1
       --> [Parce qu'il definit ...]_1 [c'est un point interessant]_2 : root 2
     - left headed but among nuclei
       (find the sallient set for each child, ie all nuclei)
       ( NN-textual ( NN-same (NN-list (1,2) (NS-elab (3, NS-elab(4,5))), NN-list (...
       1 -list-> 2 , 1 -same-> 3 , 3 -elab-> 4 , 4 -elab-> 5
    '''
    relations = [] # [label, left sallient set, right sallient set], ..
    _dep_form( tree, relations, keep_nuc=keep_nuc )
    root = set_root( relations ) # first nucleus in the document
    added_edus = set() # check all edus have been covered
    dependencies = { root:[] } # build dependency dict: head --> [ [rel, dep], ...]
    for ( r, left, right ) in relations: # left and right are sallient ens, ie nuclei only
#         print( r, left, right )
        if root in left:
            head = root
        else:
            head = left[0]
        dep = right[0]
        added_edus.add( head )
        added_edus.add( dep )
#         print( "head", head, "dep", dep )
        if head in dependencies:
            dependencies[head].append( [r, dep] )
        else:
            dependencies[head] = [ [r,dep] ]
#     print( "Choosen root:", root )
#     print( "Check covered edus:", len( added_edus ) )
    return root, dependencies


# def write_dependency( edu2text, root, dependencies, file_out ):
#     '''
#     Write a new file, format: edutext TAB label
#     '''
#     sorted_edus = sorted( edu2text, lambda x,y:cmp(x,y) )
#     with open( file_out, "w" ) as f:
#         for edu in sorted_edus:
#             if edu == root:
#                 f.write( edu2text[edu]+"\t"+"root"+"\n" )
#             else:
#                 find_dep_link( edu, dependencies, sorted_edus, edu2text, f )

#def find_dep_link( edu, dependencies, sorted_edus, edu2text, f ):
def find_dep_link( edu, dependencies, sorted_edus ):
    '''
    Get the dependency link for edu
    '''
    for h in dependencies:
        for (r,d) in dependencies[h]:
            if edu == d:
                #print( edu, h, sorted_edus.index( h ), sorted_edus.index( d ), sorted_edus.index( h )-sorted_edus.index( d ) )
                return h, r
            #sorted_edus.index( h ) we want the index to begin at 1
#                 f.write( "\t".join( [edu2text[edu],
#                     str(sorted_edus.index( h )-sorted_edus.index( d ))+"_"+r] )+"\n" )
#     print( edu, '\n', dependencies )

def set_root( relations ):
    nuclei = []
    for ( r, left, right ) in relations:
#         print( r, left, right )
        nuclei.extend( [e for e in left] )
        #nuclei.extend( [e for e in right] )
#     print( nuclei )
    #return sorted( nuclei, lambda x,y:cmp(x,y) )[0]
    return nuclei[0]

# Parcours en profondeur
def _dep_form( t, relations, keep_nuc=False ):
    try:
        label = t.label()
        label = label.replace('_', '')
        if not keep_nuc and ("NN-" in t.label() or "NS-" in t.label() or "SN-" in t.label()):
            #label = '-'.join( t.label().split('-')[1:] )
            label = t.label().split('-')[0] # keep only nuc
        if not label == "EDU":
            children = [c for c in t]
            if len( children ) > 2:
                sys.exit( "non binary tree ?" )
            # looking for sallient ensemble for each child
            if "NN-" in t.label() or "NS-" in t.label():
                salient_left = get_salient( children[0] )
                salient_right = get_salient( children[1] )
            elif "SN-" in t.label():
                salient_left = get_salient( children[1] )
                salient_right = get_salient( children[0] )
            relations.append( [label, salient_left, salient_right] )
            #print label, salient_left, salient_right
            _dep_form( children[0], relations, keep_nuc=keep_nuc )
            _dep_form( children[1], relations, keep_nuc=keep_nuc )
    except AttributeError:
        return

def get_salient( tree ):
    salient_set = []
    _get_salient( tree, salient_set )
    return salient_set

def _get_salient( t, salient_set ):
    try:
        label = t.label()
        children = [c for c in t]
        if label == "EDU":
            salient_set.append( int( children[0] ) )
        else:
            nuclearity = label.split('-')[0]
            left_nuc = nuclearity[0]
            right_nuc = nuclearity[1]
            if left_nuc == 'N':
                _get_salient( children[0], salient_set )
            if right_nuc == 'N':
                _get_salient( children[1], salient_set )

    except AttributeError:
        return




if __name__ == '__main__':
    main()



