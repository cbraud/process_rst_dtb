#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

src=navi/discourse/code/read-rstdt/rst_DTB/


# Original data in:
DATA=/Users/chloe/data/discourse-rst/
OUTMAIN=/Users/chloe/discoursecph/segmentation/data/main/
#OUTMAIN=/Users/chloe/sandbox/
OUTAUX=/Users/chloe/discoursecph/segmentation/data/aux/

DE=${DATA}de/
DU=${DATA}du/
EN=${DATA}en/
EU=${DATA}eu/
ES=${DATA}es/
PT=${DATA}pt/



# -- Main Task: ie convert based on words
# - no translation (for baseline systems + when using wd embeddings)
echo Main Task no translation

echo German
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DE}conllu/ --outpath ${OUTMAIN}de/no-translation/
echo Dutch
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DU}conllu/ --outpath ${OUTMAIN}du/no-translation/
echo English
python ${src}/utils_convert/convert2seg-bio.py --treebank ${EN}conllu/ --outpath ${OUTMAIN}en/no-translation/
echo Basque
python ${src}/utils_convert/convert2seg-bio.py --treebank ${EU}conllu/ --outpath ${OUTMAIN}eu/no-translation/
echo Spanish
python ${src}/utils_convert/convert2seg-bio.py --treebank ${ES}conllu/ --outpath ${OUTMAIN}es/no-translation/
echo Portuguese
python ${src}/utils_convert/convert2seg-bio.py --treebank ${PT}conllu/ --outpath ${OUTMAIN}pt/no-translation/

echo

# - with translation (for combinations based on translations)
echo Main Task with translation

echo German
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DE}conllut/ --outpath ${OUTMAIN}de/translation/ --trans
echo Dutch
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DU}conllut/ --outpath ${OUTMAIN}du/translation/ --trans
echo Basque
python ${src}/utils_convert/convert2seg-bio.py --treebank ${EU}conllut/ --outpath ${OUTMAIN}eu/translation/ --trans
echo Spanish
python ${src}/utils_convert/convert2seg-bio.py --treebank ${ES}conllut/ --outpath ${OUTMAIN}es/translation/ --trans
echo Portuguese
python ${src}/utils_convert/convert2seg-bio.py --treebank ${PT}conllut/ --outpath ${OUTMAIN}pt/translation/ --trans



# -- Aux Tasks:

# - POS: segmentation based on POS tags (rather than words)
mkdir -p ${OUTAUX}pos/
echo Aux task: POS

echo German
mkdir -p ${OUTAUX}pos/de/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DE}conllu/ --outpath ${OUTAUX}pos/de/ --pos
echo Dutch
mkdir -p ${OUTAUX}pos/du/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${DU}conllu/ --outpath ${OUTAUX}pos/du/ --pos
echo English
mkdir -p ${OUTAUX}pos/en/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${EN}conllu/ --outpath ${OUTAUX}pos/en/ --pos
echo Basque
mkdir -p ${OUTAUX}pos/eu/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${EU}conllu/ --outpath ${OUTAUX}pos/eu/ --pos
echo Spanish
mkdir -p ${OUTAUX}pos/es/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${ES}conllu/ --outpath ${OUTAUX}pos/es/ --pos
echo Portuguese
mkdir -p ${OUTAUX}pos/pt/
python ${src}/utils_convert/convert2seg-bio.py --treebank ${PT}conllu/ --outpath ${OUTAUX}pos/pt/ --pos

