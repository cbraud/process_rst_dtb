#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
- Generate edu files
'''

import argparse, os, sys, shutil
from utils_rs3 import getRS3Files,parseXML
import numpy as np
#from utils_dis import *
#from utils_chn import *
from nltk.tokenize import treebank

TOKENIZER = treebank.TreebankWordTokenizer()

def main( ):
    parser = argparse.ArgumentParser(
            description='Read discourse corpora (for now .dis or .rs3) and output EDU files for building a discourse segmenter.')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            default="/Users/chloe/navi/discourse/data/rstdt_samples/",
            help='Input file/directory to read (RST .dis files, RST .rs3 files)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            default="/Users/chloe/navi/discourse/data/rstdt_samples/",
            help='Output directory (.edus)')
    parser.add_argument('--format',
            dest='format',
            action='store',
            default="rs3",
            help='Format (Default:rs3)')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    readSeg( args.treebank, args.outpath, args.format )


def readSeg( tbpath, outpath, format ):

    tokenize = True

    count_edu = 0
    count_doc = 0
    nbedus = []
    if format == "rs3":
        files = getRS3Files( tbpath )
        for fd in files:
            print( "\nReading", fd, file=sys.stderr )
            doc_root, rs3_xml_tree = parseXML( fd )
            eduList = readRS3Annotation( doc_root )
            #print( eduList )
            o = open( os.path.join( outpath, os.path.basename(fd).replace( '.rs3', '.edus' ) ), 'w', encoding='utf8' )
            edu_doc = 0
            for e in sorted( eduList, key=lambda x:x['position'] ):
                #print( e['position'], e['id'] )

                if tokenize:
                    tokens = TOKENIZER.tokenize( e['text'] )
                    o.write( ' '.join( tokens ).strip()+'\n' )
                else:
                    o.write( e['text'].strip()+'\n' )
                count_edu += 1
                edu_doc += 1
            o.close()
            count_doc += 1
            nbedus.append( edu_doc )
    elif format == "dis":#simply copy the edu files
        files = getEduFiles( tbpath, ext=".edus" )
        for fd in files:
            #print( "\nReading", fd, file=sys.stderr )
            edu_doc = len( open( fd ).readlines() )
            count_edu += edu_doc
            shutil.copy( fd, outpath )
            count_doc += 1
            nbedus.append( edu_doc )
        
    print( "#DOC", count_doc, "#EDUS", count_edu, file=sys.stderr )
    print( "max/min/avg", max( nbedus ), min( nbedus ), np.mean( nbedus ) )

def readRS3Annotation( doc_root ):
    '''
        a group's ``type`` attribute tells us whether the group
        node represents a span (of RST segments or other groups) OR
        a multinuc(ular) relation (i.e. it dominates several RST nucleii).

        a group's ``relname`` gives us the name of the relation between
        the group node and the group's parent node.

    '''
    eduList = []
    # - EDU
    eduIds = []
    for i,segment in enumerate( doc_root.iter('segment') ):
        # Get information for the current segment
        id_ = int(segment.attrib['id'])
        #parent = int(segment.attrib['parent']) #may be no parent for an EDU in SFU ??

        #relname = segment.attrib['relname'].replace(' ', '-' )#.decode( "iso-8859-3" ) and no relname ??
        edu = {"id":id_,
                #"parent":parent,
                #"relname":relname,
                "text":segment.text.strip().replace("\n", " "),#.decode("iso-8859-3"),
                "position":i}
        if not id_ in eduIds:#Avoid the repeted segments
            eduList.append( edu )
            eduIds.append( id_ )
    return eduList



def getEduFiles( tbpath, ext=".edus" ):
    edufiles = []
    for p, dirs, files in os.walk( tbpath ):
        subdir = os.path.basename(p)
        dirs[:] = [d for d in dirs if not d[0] == '.']
        for file in files:
            if not file.startswith('.') and file.endswith(ext):
                edufiles.append( os.path.join( p, file ) )
    return edufiles










if __name__ == '__main__':
    main()



