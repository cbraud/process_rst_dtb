#!/bin/bash



# Exit immediately if a command exits with a non-zero status.
set -e


NORMALIZER=trance-master/progs/trance_treebank
MAKEGRAM=trance-master/progs/trance_grammar
LEARNER=trance-master/progs/trance_learn
EVAL=trance-master/progs/trance_parse


# INPUT DATA
##CORPORA=data/corpora/discourse/rst_scheme/
CORPORA=discoursecph/multilingual/data/

ENGLISH_RSTDT=${CORPORA}english-rstdt/constituent_format/
##BASQUE_RSTDT=${CORPORA}basque_rstdt/
BRAZILIAN_CST=${CORPORA}brazilian-cst/constituent_format/
##ENGLISH_GUM=${CORPORA}english/GUM/
GERMAN_PCC=${CORPORA}german-pcc/constituent_format/
##ML_RSTDT=${CORPORA}multilingualRST/multiRST/
SPANISH_RSTDT=${CORPORA}spanish-rstdt/constituent_format/


# OUTPUT NORMALIZED FILES
NORMALIZED=experiments/coling16_multilingual/trance_constituent/normalized/
# OUTPUT MODELS+GRAMMARS
MODELS=experiments/coling16_multilingual/trance_constituent/models/
# OUTPUT PREDICTIONS
PREDS=experiments/coling16_multilingual/trance_constituent/predictions/



#LOG=log_multilingual_trance_supervised_020716.txt

# ENGLISH RST DT
echo ENGLISH RST DT
cat ${ENGLISH_RSTDT}train.brackets | ${NORMALIZER} --output ${NORMALIZED}eng-rstdt_train.norm.brackets --normalize --remove-none --remove-cycle --debug 

cat ${ENGLISH_RSTDT}dev.brackets | ${NORMALIZER} --output ${NORMALIZED}eng-rstdt_dev.norm.brackets --normalize --remove-none --remove-cycle --debug 

cat ${ENGLISH_RSTDT}test.brackets | ${NORMALIZER} --output ${NORMALIZED}eng-rstdt_test.norm.brackets --normalize --remove-none --remove-cycle --debug 


${MAKEGRAM} --input ${NORMALIZED}eng-rstdt_train.norm.brackets --output ${MODELS}/eng-rstdt_grammar --cutoff 2 --debug --signature English



${LEARNER} --input ${NORMALIZED}eng-rstdt_train.norm.brackets --test ${NORMALIZED}eng-rstdt_dev.norm.brackets --output ${MODELS}eng-rstdt_model --grammar ${MODELS}/eng-rstdt_grammar --unary 0 --beam 32 --kbest 128 --randomize --learn all:opt=adadec,violation=max,margin-all=true,batch=4,iteration=100,eta=1e-2,gamma=0.9,epsilon=1,lambda=1e-5 --mix-select --averaging --debug=2 --word-embedding ${ENGLISH_RSTDT}embeddings_edus_svd-50.txt --embedding 50



${EVAL} --grammar ${MODELS}/eng-rstdt_grammar --model ${MODELS}eng-rstdt_model --unary 0 --signature English --word-embedding ${ENGLISH_RSTDT}embeddings_edus_svd-50.txt --embedding 50 --input ${ENGLISH_RSTDT}test.en.edus --output ${PREDS}eng-rstdt_predictions





# GERMAN PCC
echo GERMAN PCC
cat ${GERMAN_PCC}train.brackets | ${NORMALIZER} --output ${NORMALIZED}germ-pcc_train.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${GERMAN_PCC}dev.brackets | ${NORMALIZER} --output ${NORMALIZED}germ-pcc_dev.norm.brackets --normalize --remove-none --remove-cycle --debug


cat ${GERMAN_PCC}test.brackets | ${NORMALIZER} --output ${NORMALIZED}germ-pcc_test.norm.brackets --normalize --remove-none --remove-cycle --debug


${MAKEGRAM} --input ${NORMALIZED}germ-pcc_train.norm.brackets --output ${MODELS}/germ-pcc_grammar --cutoff 2 --debug --signature English



${LEARNER} --input ${NORMALIZED}germ-pcc_train.norm.brackets --test ${NORMALIZED}germ-pcc_dev.norm.brackets --output ${MODELS}germ-pcc_model --grammar ${MODELS}/germ-pcc_grammar --unary 0 --beam 32 --kbest 128 --randomize --learn all:opt=adadec,violation=max,margin-all=true,batch=4,iteration=100,eta=1e-2,gamma=0.9,epsilon=1,lambda=1e-5 --mix-select --averaging --debug=2 --word-embedding ${GERMAN_PCC}embeddings_edus_svd-50.txt --embedding 50




${EVAL} --grammar ${MODELS}/germ-pcc_grammar --model ${MODELS}germ-pcc_model --unary 0 --signature English --word-embedding ${GERMAN_PCC}embeddings_edus_svd-50.txt --embedding 50 --input ${GERMAN_PCC}test.en.edus --output ${PREDS}germ-pcc_predictions








# SPANISH RST DT
echo SPANISH RST DT
cat ${SPANISH_RSTDT}train.brackets | ${NORMALIZER} --output ${NORMALIZED}span-rstdt_train.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${SPANISH_RSTDT}dev.brackets | ${NORMALIZER} --output ${NORMALIZED}span-rstdt_dev.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${SPANISH_RSTDT}testa.brackets | ${NORMALIZER} --output ${NORMALIZED}span-rstdt_testa.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${SPANISH_RSTDT}testb.brackets | ${NORMALIZER} --output ${NORMALIZED}span-rstdt_testb.norm.brackets --normalize --remove-none --remove-cycle --debug


${MAKEGRAM} --input ${NORMALIZED}span-rstdt_train.norm.brackets --output ${MODELS}/span-rstdt_grammar --cutoff 2 --debug --signature English




${LEARNER} --input ${NORMALIZED}span-rstdt_train.norm.brackets --test ${NORMALIZED}span-rstdt_dev.norm.brackets --output ${MODELS}span-rstdt_model --grammar ${MODELS}/span-rstdt_grammar --unary 0 --beam 32 --kbest 128 --randomize --learn all:opt=adadec,violation=max,margin-all=true,batch=4,iteration=100,eta=1e-2,gamma=0.9,epsilon=1,lambda=1e-5 --mix-select --averaging --debug=2 --word-embedding ${SPANISH_RSTDT}embeddings_edus_svd-50.txt --embedding 50



${EVAL} --grammar ${MODELS}/span-rstdt_grammar --model ${MODELS}span-rstdt_model --unary 0 --signature English --word-embedding ${SPANISH_RSTDT}embeddings_edus_svd-50.txt --embedding 50 --input ${SPANISH_RSTDT}testa.en.edus --output ${PREDS}span-rstdt_predictions



# BRAZILIAN CST
echo BRAZILIAN CST
cat ${BRAZILIAN_CST}train.brackets | ${NORMALIZER} --output ${NORMALIZED}braz-cst_train.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${BRAZILIAN_CST}dev.brackets | ${NORMALIZER} --output ${NORMALIZED}braz-cst_dev.norm.brackets --normalize --remove-none --remove-cycle --debug

cat ${BRAZILIAN_CST}test.brackets | ${NORMALIZER} --output ${NORMALIZED}braz-cst_test.norm.brackets --normalize --remove-none --remove-cycle --debug


${MAKEGRAM} --input ${NORMALIZED}braz-cst_train.norm.brackets --output ${MODELS}/braz-cst_grammar --cutoff 2 --debug --signature English



${LEARNER} --input ${NORMALIZED}braz-cst_train.norm.brackets --test ${NORMALIZED}braz-cst_dev.norm.brackets --output ${MODELS}braz-cst_model --grammar ${MODELS}/braz-cst_grammar --unary 0 --beam 32 --kbest 128 --randomize --learn all:opt=adadec,violation=max,margin-all=true,batch=4,iteration=100,eta=1e-2,gamma=0.9,epsilon=1,lambda=1e-5 --mix-select --averaging --debug=2 --word-embedding ${BRAZILIAN_CST}embeddings_edus_svd-50.txt --embedding 50



${EVAL} --grammar ${MODELS}/braz-cst_grammar --model ${MODELS}braz-cst_model --unary 0 --signature English --word-embedding ${BRAZILIAN_CST}embeddings_edus_svd-50.txt --embedding 50 --input ${BRAZILIAN_CST}test.en.edus --output ${PREDS}braz-cst_predictions



