#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Read the discourse corpus and output the desired files
'''

import argparse, os, sys, shutil


def main( ):
    parser = argparse.ArgumentParser(
            description='Read discourse corpora (.dis, .rs3, .lisp + chinese corpus) and output desired files (discourse mrg files, edu files, feature files...')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Input directory to read (RST files)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            default="/Users/chloe/navi/discourse/data/rstdt_samples/",
            help='Output directory')
    parser.add_argument('--mode',
            dest='mode',
            action='store',
            default="all",
            help='Mode "all" produces all files (from dmrg to feat files), "edu" produced only segmentation files (Default:all)')
    parser.add_argument('--format',
            dest='format',
            action='store',
            default="rs3",
            help='Format (Default:rs3)')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    read( args.treebank, args.outpath, mode=args.mode, format=args.format )


def read( treebank, outpath, mode, format ):




if __name__ == '__main__':
    main()

