#!/bin/bash



# Exit immediately if a command exits with a non-zero status.
set -e

# src=navi/discourse/code/rst_DTB/
src=projects/navi/discourse/code/read-rstdt/rst_DTB/

: <<'END'
# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/en-sfu

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd



# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/en-instr

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
#python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd


END
# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/en-gum2

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd



: <<'END'
# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/du

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd


# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/es

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd



# ----------------------------------------------------------------------------------

data=/Users/chloebraud/data/expe/discourse-rst/rst-segmenter/en

dmrg=${data}/dmrg/
parse=${data}/parse-ud/
conllu=${data}/conllu/
feat=${data}/feat/
pos=${data}/feat/pos/
word=${data}/feat/word/
poswd=${data}/feat/word+pos-mix/

mkdir -p ${conllu}
python ${src}/utils_convert/convert2conll.py --treebank ${dmrg} --outpath ${conllu} --parse ${parse} --split ${data}/files_list/

mkdir -p ${feat}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${word}
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${pos} --pos
python ${src}/utils_convert/convert2seg-bio.py --treebank ${conllu} --outpath ${poswd} --poswd


END
