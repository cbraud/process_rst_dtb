#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Specific format for chinese RST DT
'''



from __future__ import print_function
import argparse, os, sys, subprocess, shutil, codecs

import data


def main( ):
    parser = argparse.ArgumentParser(
            description='Discourse corpus (chinese rst dt = .txt file).')
    parser.add_argument('--treebank',
            dest='treebank',
            action='store',
            help='Input file/directory to read (chinese rst dt = .txt file)')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output directory (.bracketed, .edus and .ps files)')
    parser.add_argument('--mapping',
            dest='mapping',
            action='store',
            default=None,
            help='Label mapping (original_relation class, described in relationSet.py, here give the name of the mapping)')
    parser.add_argument('--draw',
            dest='draw',
            action='store',
            default=True,
            help='Draw a ps file for each tree (Default=True)')
    args = parser.parse_args()

    readChin( vars(args)['treebank'], vars(args)['outpath'], vars(args)['mapping'], vars(args)['draw'] )


def readChin( tbpath, outpath, mappingFile, draw ):
    corpus = data.Corpus( tbpath, mappingFile, datatype="chn", draw=draw )
    print( corpus.__str__(), file=sys.stderr )
    corpus.read( )
#     corpus.mapRelations( )
    corpus.write( outpath )
#

if __name__ == '__main__':
    main()



