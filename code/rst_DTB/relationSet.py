#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
import os, sys, shutil


'''
Utils for relation sets

TODO change the translation of some relations (eg alternativa = otherwise), see paper

'''


# Dictionnaries for mapping to the RST DT relation set (18 coarse grained classes)

mapping = {
    u'ahalbideratzea':'Enablement',
u'alderantzizko-baldintza':'Condition',
u'alternativa':'Condition',
u'analogy':'Comparison',
u'antitesia':'Contrast',
u'antithesis':'Contrast',
u'antítesis':'Contrast',
u'arazo-soluzioa':'Topic-Comment',
u'attribution':'Attribution',
u'attribution-negative':'Attribution',
u'aukera':'Condition',
u'background':'Background',
u'baldintza':'Condition',
u'bateratzea':'Joint',
u'birformulazioa':'Summary',
u'capacitación':'Enablement',
u'causa':'Cause',
u'cause':'Cause',
u'cause-result':'Cause',
u'circumstance':'Background',
u'circunstancia':'Background',
u'comment':'Evaluation',
u'comment-topic':'Topic-Comment',
u'comparison':'Comparison',
u'concesión':'Contrast',
u'concession':'Contrast',
u'conclusion':'Evaluation',
u'condición':'Condition',
u'condición-inversa':'Condition',
u'condition':'Condition',
u'conjunción':'Joint',
u'conjunction':'Joint',
u'consequence':'Cause',
u'contingency':'Condition',
u'contrast':'Contrast',
u'contraste':'Contrast',
u'definition':'Elaboration',
u'definitu-gabeko-erlazioa':'Summary',
u'disjunction':'Joint',
u'disjuntzioa':'Joint',
u'disyunción':'Joint',
u'e-elaboration':'Elaboration',
u'ebaluazioa':'Evaluation',
u'ebidentzia':'Explanation',
u'elaboración':'Elaboration',
u'elaboration':'Elaboration',
u'elaboration-additional':'Elaboration',
u'elaboration-general-specific':'Elaboration',
u'elaboration-object-attribute':'Elaboration',
u'elaboration-part-whole':'Elaboration',
u'elaboration-process-step':'Elaboration',
u'elaboration-set-member':'Elaboration',
u'elaborazioa':'Elaboration',
u'enablement':'Enablement',
u'evaluación':'Evaluation',
u'evaluation':'Evaluation',
u'evidence':'Explanation',
u'evidencia':'Explanation',
u'example':'Elaboration',
u'explanation':'Explanation',
u'explanation-argumentative':'Explanation',
u'ez-baldintzatzailea':'Condition',
u'fondo':'Background',
u'helburua':'Enablement',
u'hypothetical':'Condition',
u'interpretación':'Evaluation',
u'interpretation':'Evaluation',
u'interpretazioa':'Evaluation',
u'inverted-sequence':'Temporal',
u'joint':'Joint',
u'justificación':'Explanation',
u'justifikazioa':'Explanation',
u'justify':'Explanation',
u'kausa':'Cause',
u'konjuntzioa':'Joint',
u'kontrastea':'Contrast',
u'kontzesioa':'Contrast',
u'laburpena':'Summary',
u'list':'Joint',
u'lista':'Joint',
u'manner':'Manner-Means',
u'means':'Manner-Means',
u'medio':'Manner-Means',
u'metodoa':'Manner-Means',
u'motibazioa':'Explanation',
u'motivación':'Explanation',
u'motivation':'Explanation',
u'non-volitional-cause':'Cause',
u'non-volitional-result':'Cause',
u'nonvolitional-cause':'Cause',
u'nonvolitional-result':'Cause',
u'ondorioa':'Cause',
u'otherwise':'Condition',
u'parenthetical':'Elaboration',
u'preference':'Comparison',
u'preparación':'Background',
u'preparation':'Background',
u'prestatzea':'Background',
u'problem-solution':'Topic-Comment',
u'proportion':'Comparison',
u'propósito':'Enablement',
u'purpose':'Enablement',
u'question-answer':'Topic-Comment',
u'reason':'Explanation',
u'reformulación':'Summary',
u'restatement':'Summary',
u'restatement-mn':'Summary',
u'result':'Cause',
u'resultado':'Cause',
u'resumen':'Summary',
u'rhetorical-question':'Topic-Comment',
u'same-unit':'Same-unit',
u'secuencia':'Temporal',
u'sekuentzia':'Temporal',
u'sequence':'Temporal',
u'solución':'Topic-Comment',
u'solutionhood':'Topic-Comment',
u'statement-response':'Topic-Comment',
u'summary':'Summary',
u'temporal-after':'Temporal',
u'temporal-before':'Temporal',
u'temporal-same-time':'Temporal',
u'testuingurua':'Background',
u'textual-organization':'Textual-organization',
u'textualorganization':'Textual-organization',
u'topic-comment':'Topic-Comment',
u'topic-drift':'Topic-Change',
u'topic-shift':'Topic-Change',
u'unconditional':'Condition',
u'unión':'Joint',
u'unless':'Condition',
u'volitional-cause':'Cause',
u'volitional-result':'Cause',
u'zirkunstantzia':'Background'
}

# mapping = {#The most similar to the RST
#         # Brazilian
#     u'antithesis':u'Contrast',
#     u'attribution':u'Attribution',
#     u'background':u'Background',
#     u'circumstance':u'Background',
#     u'comparison':u'Comparison',
#     u'concession':u'Contrast',
#     u'conclusion':u'Evaluation',
#     u'condition':u'Condition',
#     u'contrast':u'Contrast',
#     u'elaboration':u'Elaboration',
#     u'enablement':u'Enablement',
#     u'evaluation':u'Evaluation',
#     u'evidence':u'Explanation',
#     u'explanation':u'Explanation',
#     u'interpretation':u'Evaluation',
#     u'joint':u'Joint',
#     u'justify':u'Explanation', #TODO
#     u'list':u'Joint',
#     u'means':u'Manner-Means',
#     u'motivation':u'Explanation', #TODO
#     u'non-volitional-cause':u'Cause',# Not in the RST DT (bu in the theoretical framework)
#     u'non-volitional-result':u'Cause',#Map these relations to cause and result? see the # of inst
#     u'otherwise':u'Condition',
#     u'parenthetical':u'Elaboration', #TODO
#     u'purpose':u'Enablement',
#     u'restatement':u'Summary',
#     u'same-unit':u'Same-unit',
#     u'sequence':u'Temporal',
#     u'solutionhood':u'Topic-Comment',#TODO
#     u'volitional-cause':u'Cause',
#     u'volitional-result':u'Cause',
#     # BASQUE
#     u'ahalbideratzea':u'Enablement',
#     u'alderantzizko-baldintza':u'Condition',#not used in the rst dt #TODO
#     u'antitesia':u'Contrast',
#     u'arazo-soluzioa':u'Topic-Comment',#not used in the rst dt, solution-hood changed to 'solutionhood' used in the other corpora, map to problem-solution (<rst dt) ? #TODO
#     u'aukera':u'Condition',
#     u'baldintza':u'Condition',
#     u'bateratzea':u'Joint',#Join on the website but Joint is a most common name, note however that it is the group name for List and disjunction, not a relation is the original set, check the meaning of this relation
#     u'birformulazioa':u'Summary',
#     u'definitu-gabeko-erlazioa':u'Summary',#not used in the rst dt
#     u'disjuntzioa':u'Joint',
#     u'ebaluazioa':u'Evaluation',
#     u'ebidentzia':u'Explanation',
#     u'elaborazioa':u'Elaboration',
#     u'ez-baldintzatzailea':u'Condition',#not used in the rst dt#TODO
#     u'helburua':u'Enablement',
#     u'interpretazioa':u'Evaluation',
#     u'justifikazioa':u'Explanation',#not used in the rst dt#TODO
#     u'kausa':u'Cause',
#     u'konjuntzioa':u'Joint',#not used in the rst dt, replace by List? Joint?#TODO
#     u'kontrastea':u'Contrast',
#     u'kontzesioa':u'Contrast',
#     u'laburpena':u'Summary',
#     u'lista':u'Joint',
#     u'metodoa':u'Manner-Means',
#     u'motibazioa':u'Explanation',#not used in the rst dt#TODO
#     u'ondorioa':u'Cause',
#     u'prestatzea':u'Background',#Preparation not used in the rst dt#TODO
#     u'same-unit':u'Same-unit',
#     u'sekuentzia':u'Temporal',
#     u'testuingurua':u'Background',
#     u'zirkunstantzia':u'Background',
#     # multiling
#     u'antithesis':u'Contrast',
#     u'antítesis':u'Contrast',
#     u'background':u'Background',
#     u'causa':u'Cause',
#     u'cause':u'Cause',
#     u'circumstance':u'Background',
#     u'circunstancia':u'Background',
#     u'concesión':u'Contrast',
#     u'concession':u'Contrast',
#     u'condición':u'Condition',
#     u'condition':u'Condition',
#     u'conjunction':u'Joint',#TODO
#     u'contrast':u'Contrast',
#     u'contraste':u'Contrast',
#     u'disjunction':u'Joint',
#     u'elaboración':u'Elaboration',
#     u'elaboration':u'Elaboration',
#     u'evaluación':u'Evaluation',
#     u'evaluation':u'Evaluation',
#     u'evidence':u'Explanation',
#     u'evidencia':u'Explanation',
#     u'fondo':u'Background',
#     u'interpretación':u'Evaluation',
#     u'interpretation':u'Evaluation',
#     u'justificación':u'Explanation',#TODO
#     u'justify':u'Explanation',#TODO
#     u'list':u'Joint',
#     u'lista':u'Joint',
#     u'means':u'Manner-Means',
#     u'medio':u'Manner-Means',
#     u'motivación':u'Explanation',#TODO
#     u'motivation':u'Explanation',#TODO
#     u'preparación':u'Background',#TODO
#     u'preparation':u'Background',#TODO
#     u'propósito':u'Enablement',
#     u'purpose':u'Enablement',
#     u'reformulación':u'Summary',
#     u'restatement':u'Summary',
#     u'result':u'Cause',
#     u'resultado':u'Cause',
#     u'resumen':u'Summary',
#     u'same-unit':u'Same-unit',
#     u'secuencia':u'Temporal',
#     u'sequence':u'Temporal',
#     u'solución':u'Topic-Comment',#TODO
#     u'solutionhood':u'Topic-Comment',#TODO
#     u'summary':u'Summary',
#     u'unless':u'Condition',#TODO
#     # SPANISH
#     u'alternativa':u'Condition',#TODO
#     u'antítesis':u'Contrast',
#     u'capacitación':u'Enablement',
#     u'causa':u'Cause',
#     u'circunstancia':u'Background',
#     u'concesión':u'Contrast',
#     u'condición':u'Condition',
#     u'condición-inversa':u'Condition',#TODO
#     u'conjunción':u'Joint',#TODO
#     u'contraste':u'Contrast',
#     u'disyunción':u'Joint',
#     u'elaboración':u'Elaboration',
#     u'evaluación':u'Evaluation',
#     u'evidencia':u'Explanation',
#     u'fondo':u'Background',
#     u'interpretación':u'Evaluation',
#     u'justificación':u'Explanation',#TODO
#     u'lista':u'Joint',
#     u'medio':u'Manner-Means',
#     u'motivación':u'Explanation',#TODO
#     u'preparación':u'Background',#TODO
#     u'propósito':u'Enablement',
#     u'reformulación':u'Summary',
#     u'resultado':u'Cause',
#     u'resumen':u'Summary',
#     u'same-unit':u'Same-unit',
#     u'secuencia':u'Temporal',
#     u'solución':u'Topic-Comment',#TODO
#     u'unión':u'Joint',#never seen this relation!#TODO
#     u'unless':u'Condition',#TODO,
#     # DUTCH
#     u'antithesis':u'Contrast',
#     u'background':u'Background',
#     u'circumstance':u'Background',
#     u'concession':u'Contrast',
#     u'condition':u'Condition',
#     u'conjunction':u'Joint',#TODO
#     u'contrast':u'Contrast',
#     u'disjunction':u'Joint',
#     u'elaboration':u'Elaboration',
#     u'enablement':u'Enablement',
#     u'evaluation':u'Evaluation',
#     u'evidence':u'Explanation',
#     u'interpretation':u'Evaluation',
#     u'joint':u'Joint',
#     u'justify':u'Explanation',#TODO
#     u'list':u'Joint',
#     u'means':u'Manner-Means',
#     u'motivation':u'Explanation',#TODO
#     u'nonvolitional-cause':u'Cause',
#     u'nonvolitional-result':u'Cause',
#     u'otherwise':u'Condition',
#     u'preparation':u'Background',#TODO
#     u'purpose':u'Enablement',
#     u'restatement':u'Summary',
#     u'restatement-mn':u'Summary',# multinuclear version: we do not need to keep this info at the end
#     u'sequence':u'Temporal',
#     u'solutionhood':u'Topic-Comment',#TODO
#     u'summary':u'Summary',
#     u'unconditional':u'Condition',#TODO
#     u'unless':u'Condition',#TODO
#     u'volitional-cause':u'Cause',
#     u'volitional-result':u'Cause',
#     # GERMAN
#     u'antithesis':u'Contrast',
#     u'background':u'Background',
#     u'cause':u'Cause',
#     u'circumstance':u'Background',
#     u'concession':u'Contrast',
#     u'condition':u'Condition',
#     u'conjunction':u'Joint',#TODO
#     u'contrast':u'Contrast',
#     u'disjunction':u'Joint',
#     u'e-elaboration':u'Elaboration',#Not a RST relation, can be mapped ?
#     u'elaboration':u'Elaboration',
#     u'enablement':u'Enablement',
#     u'evaluation':u'Evaluation',
#     u'evidence':u'Explanation',
#     u'interpretation':u'Evaluation',
#     u'joint':u'Joint',
#     u'justify':u'Explanation',#TODO
#     u'list':u'Joint',
#     u'means':u'Manner-Means',
#     u'motivation':u'Explanation',#TODO
#     u'otherwise':u'Condition',
#     u'preparation':u'Background',#TODO
#     u'purpose':u'Enablement',
#     u'reason':u'Explanation',#Map to Explanation?
#     u'restatement':u'Summary',
#     u'result':u'Cause',
#     u'sequence':u'Temporal',
#     u'solutionhood':u'Topic-Comment',#TODO
#     u'summary':u'Summary',
#     u'unless':u'Condition',#TODO
#     # ENGLISH
#     'analogy':'Comparison',
#     'antithesis':'Contrast',
#     'attribution':'Attribution',
#     'attribution-negative':'Attribution',
#     'background':'Background',
#     'cause':'Cause',
#     'cause-result':'Cause',
#     'circumstance':'Background',
#     'comment':'Evaluation',
#     'comment-topic':'Topic-Comment',
#     'comparison':'Comparison',
#     'concession':'Contrast',
#     'conclusion':'Evaluation',
#     'condition':'Condition',
#     'consequence':'Cause',
#     'contingency':'Condition',
#     'contrast':'Contrast',
#     'definition':'Elaboration',
#     'disjunction':'Joint',
#     'elaboration-additional':'Elaboration',
#     'elaboration-general-specific':'Elaboration',
#     'elaboration-object-attribute':'Elaboration',
#     'elaboration-part-whole':'Elaboration',
#     'elaboration-process-step':'Elaboration',
#     'elaboration-set-member':'Elaboration',
#     'enablement':'Enablement',
#     'evaluation':'Evaluation',
#     'evidence':'Explanation',
#     'example':'Elaboration',
#     'explanation-argumentative':'Explanation',
#     'hypothetical':'Condition',
#     'interpretation':'Evaluation',
#     'inverted-sequence':'Temporal',
#     'list':'Joint',
#     'manner':'Manner-Means',
#     'means':'Manner-Means',
#     'otherwise':'Condition',
#     'preference':'Comparison',
#     'problem-solution':'Topic-Comment',
#     'proportion':'Comparison',
#     'purpose':'Enablement',
#     'question-answer':'Topic-Comment',
#     'reason':'Explanation',
#     'restatement':'Summary',
#     'result':'Cause',
#     'rhetorical-question':'Topic-Comment',
#     'same-unit':'Same-unit',
#     'sequence':'Temporal',
#     'statement-response':'Topic-Comment',
#     'summary':'Summary',
#     'temporal-after':'Temporal',
#     'temporal-before':'Temporal',
#     'temporal-same-time':'Temporal',
#     'textual-organization':'Textual-organization',
#     'textualorganization':'Textual-organization',
#     'topic-comment':'Topic-Comment',
#     'topic-drift':'Topic-Change',
#     'topic-shift':'Topic-Change'
#
# }














# Dictionnaries with original labels as annotated in the data and possible corrections if needed (ie translation to English, corresction of errors)

basque_labels = {
    u'ahalbideratzea':u'enablement',
    u'alderantzizko-baldintza':u'unless',#not used in the rst dt
    u'antitesia':u'antithesis',
    u'arazo-soluzioa':u'solutionhood',#not used in the rst dt, solution-hood changed to 'solutionhood' used in the other corpora, map to problem-solution (<rst dt) ?
    u'aukera':u'otherwise',
    u'baldintza':u'condition',
    u'bateratzea':u'joint',#Join on the website but Joint is a most common name, note however that it is the group name for List and disjunction, not a relation is the original set, check the meaning of this relation
    u'birformulazioa':u'restatement',
    u'definitu-gabeko-erlazioa':u'reformulation',#not used in the rst dt
    u'disjuntzioa':u'disjunction',
    u'ebaluazioa':u'evaluation',
    u'ebidentzia':u'evidence',
    u'elaborazioa':u'elaboration',
    u'ez-baldintzatzailea':u'unconditional',#not used in the rst dt
    u'helburua':u'purpose',
    u'interpretazioa':u'interpretation',
    u'justifikazioa':u'justify',#not used in the rst dt
    u'kausa':u'cause',
    u'konjuntzioa':u'conjunction',#not used in the rst dt, replace by List? Joint?
    u'kontrastea':u'contrast',
    u'kontzesioa':u'concession',
    u'laburpena':u'summary',
    u'lista':u'list',
    u'metodoa':u'means',
    u'motibazioa':u'motivation',#not used in the rst dt
    u'ondorioa':u'result',
    u'prestatzea':u'preparation',#not used in the rst dt
    u'same-unit':u'same-unit',
    u'sekuentzia':u'sequence',
    u'testuingurua':u'background',
    u'zirkunstantzia':u'circumstance'
    }

brazilianCst_labels = {#The most similar to the RST
    u'antithesis':u'antithesis',
    u'attribution':u'attribution',
    u'background':u'background',
    u'circumstance':u'circumstance',
    u'comparison':u'comparison',
    u'concession':u'concession',
    u'conclusion':u'conclusion',
    u'condition':u'condition',
    u'contrast':u'contrast',
    u'elaboration':u'elaboration',
    u'enablement':u'enablement',
    u'evaluation':u'evaluation',
    u'evidence':u'evidence',
    u'explanation':u'explanation',
    u'interpretation':u'interpretation',
    u'joint':u'joint',
    u'justify':u'justify',
    u'list':u'list',
    u'means':u'means',
    u'motivation':u'motivation',
    u'non-volitional-cause':u'non-volitional-cause',# Not in the RST DT (bu in the theoretical framework)
    u'non-volitional-result':u'non-volitional-result',#Map these relations to cause and result? see the # of inst
    u'otherwise':u'otherwise',
    u'parenthetical':u'parenthetical',
    u'purpose':u'purpose',
    u'restatement':u'restatement',
    u'same-unit':u'same-unit',
    u'sequence':u'sequence',
    u'solutionhood':u'solutionhood',
    u'volitional-cause':u'volitional-cause',
    u'volitional-result':u'volitional-result'
}

brazilianSum_labels = {
    u'antithesis':u'antithesis',
    u'attribution':u'attribution',
    u'background':u'background',
    u'circumstance':u'circumstance',
    u'comparison':u'comparison',
    u'concession':u'concession',
    u'conclusion':u'conclusion',
    u'condition':u'condition',
    u'contrast':u'contrast',
    u'elaboration':u'elaboration',
    u'evaluation':u'evaluation',
    u'evidence':u'evidence',
    u'explanation':u'explanation',
    u'interpretation':u'interpretation',
    u'joint':u'joint',
    u'justify':u'justify',
    u'list':u'list',
    u'means':u'means',
    u'non-volitional-cause':u'non-volitional-cause',
    u'non-volitional-result':u'non-volitional-result',
    u'otherwise':u'otherwise',
    u'parenthetical':u'parenthetical',
    u'purpose':u'purpose',
    u'restatement':u'restatement',
    u'same-unit':u'same-unit',
    u'sequence':u'sequence',
    u'solutionhood':u'solutionhood',
    u'summary':u'summary',
    u'volitional-cause':u'volitional-cause'
        }

brazilianTCC_labels = {
    u'antithesis':u'antithesis',
    u'attribution':u'attribution',
    u'background':u'background',
    u'circumstance':u'circumstance',
    u'comparison':u'comparison',
    u'concession':u'concession',
    u'conclusion':u'conclusion',
    u'condition':u'condition',
    u'contrast':u'contrast',
    u'elaboration':u'elaboration',
    u'enablement':u'enablement',
    u'evaluation':u'evaluation',
    u'evidence':u'evidence',
    u'explanation':u'explanation',
    u'interpretation':u'interpretation',
    u'joint':u'joint',
    u'justify':u'justify',
    u'list':u'list',
    u'means':u'means',
    u'motivation':u'motivation',
    u'non-volitional-cause':u'non-volitional-cause',
    u'non-volitional-result':u'non-volitional-result',
    u'otherwise':u'otherwise',
    u'parenthetical':u'parenthetical',
    u'purpose':u'purpose',
    u'restatement':u'restatement',
    u'same-unit':u'same-unit',
    u'sequence':u'sequence',
    u'solutionhood':u'solutionhood',
    u'summary':u'summary',
    u'volitional-cause':u'volitional-cause',
    u'volitional-result':u'volitional-result'
        }


germanPcc_labels = {
    u'antithesis':u'antithesis',
    u'background':u'background',
    u'cause':u'cause',
    u'circumstance':u'circumstance',
    u'concession':u'concession',
    u'condition':u'condition',
    u'conjunction':u'conjunction',
    u'contrast':u'contrast',
    u'disjunction':u'disjunction',
    u'e-elaboration':u'entity-elaboration',#Not a RST relation, can be mapped ?
    u'elaboration':u'elaboration',
    u'enablement':u'enablement',
    u'evaluation':u'evaluation',
    u'evidence':u'evidence',
    u'interpretation':u'interpretation',
    u'joint':u'joint',
    u'justify':u'justify',
    u'list':u'list',
    u'means':u'means',
    u'motivation':u'motivation',
    u'otherwise':u'otherwise',
    u'preparation':u'preparation',
    u'purpose':u'purpose',
    u'reason':u'reason',#Map to Explanation?
    u'restatement':u'restatement',
    u'result':u'result',
    u'sequence':u'sequence',
    u'solutionhood':u'solutionhood',
    u'summary':u'summary',
    u'unless':u'unless'
}

multiLing_labels = {
    u'antithesis':u'antithesis',
    u'antítesis':u'antithesis',
    u'background':u'background',
    u'causa':u'cause',
    u'cause':u'cause',
    u'circumstance':u'circumstance',
    u'circunstancia':u'circumstance',
    u'concesión':u'concession',
    u'concession':u'concession',
    u'condición':u'condition',
    u'condition':u'condition',
    u'conjunction':u'conjunction',
    u'contrast':u'contrast',
    u'contraste':u'contrast',
    u'disjunction':u'disjunction',
    u'elaboración':u'elaboration',
    u'elaboration':u'elaboration',
    u'evaluación':u'evaluation',
    u'evaluation':u'evaluation',
    u'evidence':u'evidence',
    u'evidencia':u'evidence',
    u'fondo':u'background',
    u'interpretación':u'interpretation',
    u'interpretation':u'interpretation',
    u'justificación':u'justify',
    u'justify':u'justify',
    u'list':u'list',
    u'lista':u'list',
    u'means':u'means',
    u'medio':u'means',
    u'motivación':u'motivation',
    u'motivation':u'motivation',
    u'preparación':u'preparation',
    u'preparation':u'preparation',
    u'propósito':u'purpose',
    u'purpose':u'purpose',
    u'reformulación':u'restatement',
    u'restatement':u'restatement',
    u'result':u'result',
    u'resultado':u'result',
    u'resumen':u'summary',
    u'same-unit':u'same-unit',
    u'secuencia':u'sequence',
    u'sequence':u'sequence',
    u'solución':u'solutionhood',
    u'solutionhood':u'solutionhood',
    u'summary':u'summary',
    u'unless':u'unless'
}

spanish_labels = {
    u'alternativa':u'alternative',
    u'antítesis':u'antithesis',
    u'capacitación':u'enablement',
    u'causa':u'cause',
    u'circunstancia':u'circumstance',
    u'concesión':u'concession',
    u'condición':u'condition',
    u'condición-inversa':u'unconditional',#TODO
    u'conjunción':u'conjunction',
    u'contraste':u'contrast',
    u'disyunción':u'disjunction',
    u'elaboración':u'elaboration',
    u'evaluación':u'evaluation',
    u'evidencia':u'evidence',
    u'fondo':u'background',
    u'interpretación':u'interpretation',
    u'justificación':u'justify',
    u'lista':u'list',
    u'medio':u'means',
    u'motivación':u'motivation',
    u'preparación':u'preparation',
    u'propósito':u'purpose',
    u'reformulación':u'restatement',
    u'resultado':u'result',
    u'resumen':u'summary',
    u'same-unit':u'same-unit',
    u'secuencia':u'sequence',
    u'solución':u'solutionhood',
    u'unión':u'union',#never seen this relation!
    u'unless':u'unless'
}





# RST DT usual mapping
rstdt_mapping18 = {
    'analogy':'Comparison',
    'antithesis':'Contrast',
    'attribution':'Attribution',
    'attribution-negative':'Attribution',
    'background':'Background',
    'cause':'Cause',
    'cause-result':'Cause',
    'circumstance':'Background',
    'comment':'Evaluation',
    'comment-topic':'Topic-Comment',
    'comparison':'Comparison',
    'concession':'Contrast',
    'conclusion':'Evaluation',
    'condition':'Condition',
    'consequence':'Cause',
    'contingency':'Condition',
    'contrast':'Contrast',
    'definition':'Elaboration',
    'disjunction':'Joint',
    'elaboration-additional':'Elaboration',
    'elaboration-general-specific':'Elaboration',
    'elaboration-object-attribute':'Elaboration',
    'elaboration-part-whole':'Elaboration',
    'elaboration-process-step':'Elaboration',
    'elaboration-set-member':'Elaboration',
    'enablement':'Enablement',
    'evaluation':'Evaluation',
    'evidence':'Explanation',
    'example':'Elaboration',
    'explanation-argumentative':'Explanation',
    'hypothetical':'Condition',
    'interpretation':'Evaluation',
    'inverted-sequence':'Temporal',
    'list':'Joint',
    'manner':'Manner-Means',
    'means':'Manner-Means',
    'otherwise':'Condition',
    'preference':'Comparison',
    'problem-solution':'Topic-Comment',
    'proportion':'Comparison',
    'purpose':'Enablement',
    'question-answer':'Topic-Comment',
    'reason':'Explanation',
    'restatement':'Summary',
    'result':'Cause',
    'rhetorical-question':'Topic-Comment',
    'same-unit':'Same-unit',
    'sequence':'Temporal',
    'statement-response':'Topic-Comment',
    'summary':'Summary',
    'temporal-after':'Temporal',
    'temporal-before':'Temporal',
    'temporal-same-time':'Temporal',
    'textual-organization':'Textual-organization',
    'textualorganization':'Textual-organization',
    'topic-comment':'Topic-Comment',
    'topic-drift':'Topic-Change',
    'topic-shift':'Topic-Change'
    }


chinese_labels ={
        u'总结':u'Generalization',#seems that it means "in summary"
        u'ALT':u'Alternative',#in class Conjunction
        u'ATT':u'Attribute',#in class background
        u'CASU':u'Cause',#erreur
        u'CAUS':u'Cause',#in class inference
        u'CONC':u'Concession',#in class comparison
        u'COND':u'Condition',#in class condition
        u'CONT':u'Contrast',#in class comparison
        u'COOR':u'Coordination',#coordinate, in class Conjunction
        u'EXPL':u'Explanation',#in class specification
        u'GENE':u'Generalization',#in class summary
        u'HYP':u'Hypothetical',#in class condition (choose condition rather?)
        u'LIST':u'List',#in class specification
        u'MAR':u'Marker',#error
        u'MARK':u'Marker',#in class background
        u'PROG':u'Progressive',#in class Conjunction
        u'PURP':u'Purpose',#in class inference
        u'RESU':u'Result',#in class inference
        u'SUCC':u'Succession',#in class Conjunction
        u'TEMP':u'Temporal',#in class Conjunction
        u'TOP':u'Topic',#in class background
}

chinese_nuclearity ={
        u'总结':u'NS',#seems that it means "in summary"
        u'ALT':u'NN',#in class Conjunction
        u'ATT':u'NS',#in class background TODO find nuc !!
        u'CASU':u'NS',#erreur
        u'CAUS':u'NS',#in class inference
        u'CONC':u'NS',#in class comparison
        u'COND':u'NS',#in class condition
        u'CONT':u'NS',#in class comparison
        u'COOR':u'NN',#coordinate, in class Conjunction
        u'EXPL':u'NS',#in class specification
        u'GENE':u'NS',#in class summary
        u'HYP':u'NS',#in class condition (choose condition rather?)
        u'LIST':u'NS',#in class specification
        u'MAR':u'NS',#error TODO
        u'MARK':u'NS',#in class background TODO
        u'PROG':u'NN',#in class Conjunction
        u'PURP':u'NS',#in class inference
        u'RESU':u'NS',#in class inference
        u'SUCC':u'NN',#in class Conjunction
        u'TEMP':u'NN',#in class Conjunction
        u'TOP':u'NS',#in class background TODO
}



english_instr = {
    u'act:constraint':u'act:constraint',
    u'act:criterion':u'act:criterion',
    u'act:goal':u'act:goal',
    u'act:preparation':u'act:preparation',
    u'act:reason':u'act:reason',
    u'act:side-effect':u'act:side-effect',
    u'after:before':u'after:before',
    u'before:after':u'before:after',
    u'cause:effect':u'cause:effect',
    u'circumstance:situation':u'circumstance:situation',
    u'co-temp1:co-temp2':u'co-temp1:co-temp2',
    u'comparision':u'comparision',
    u'constraint:act':u'constraint:act',
    u'contrast1:contrast2':u'contrast1:contrast2',
    u'criterion:act':u'criterion:act',
    u'criterion:wrong-act':u'criterion:wrong-act',
    u'disjunction':u'disjunction',
    u'effect:cause':u'effect:cause',
    u'general:specific':u'general:specific',
    u'goal:act':u'goal:act',
    u'indeterminate':u'indeterminate',
    u'joint':u'joint',
    u'object:attribute':u'object:attribute',
    u'obstacle:situation':u'obstacle:situation',
    u'preparation:act':u'preparation:act',
    u'prescribe-act:wrong-act':u'prescribe-act:wrong-act',
    u'reason:act':u'reason:act',
    u'same-unit':u'same-unit',
    u'set:member':u'set:member',
    u'situation:circumstance':u'situation:circumstance',
    u'situation:obstacle':u'situation:obstacle',
    u'specific:general':u'specific:general',
    u'step1:step2':u'step1:step2',
    u'textualorganization':u'textualorganization',
    u'wrong-act:prescribe-act':u'wrong-act:prescribe-act'
}


# conjunction coordinate, alternative, temporal, progressive, succession
#
# comparison contrast, concession
#
# inference cause, result, purpose
#
# condition hypothetical, condition
#
# specification explanation, list
#
# summary generalization
#
# background topic, attribute, marker
#







dutch_labels = {
        u'antithesis':u'antithesis',
        u'background':u'background',
        u'circumstance':u'circumstance',
        u'concession':u'concession',
        u'condition':u'condition',
        u'conjunction':u'conjunction',
        u'contrast':u'contrast',
        u'disjunction':u'disjunction',
        u'elaboration':u'elaboration',
        u'enablement':u'enablement',
        u'evaluation':u'evaluation',
        u'evidence':u'evidence',
        u'interpretation':u'interpretation',
        u'joint':u'joint',
        u'justify':u'justify',
        u'list':u'list',
        u'means':u'means',
        u'motivation':u'motivation',
        u'nonvolitional-cause':u'non-volitional-cause',
        u'nonvolitional-result':u'non-volitional-result',
        u'otherwise':u'otherwise',
        u'preparation':u'preparation',
        u'purpose':u'purpose',
        u'restatement':u'restatement',
        u'restatement-mn':u'restatement',# multinuclear version: we do not need to keep this info at the end
        u'sequence':u'sequence',
        u'solutionhood':u'solutionhood',
        u'summary':u'summary',
        u'unconditional':u'unconditional',
        u'unless':u'unless',
        u'volitional-cause':u'volitional-cause',
        u'volitional-result':u'volitional-result'
        }


gum_labels = {}
