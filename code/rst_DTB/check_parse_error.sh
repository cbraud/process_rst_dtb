#!/bin/bash



# Exit immediately if a command exits with a non-zero status.
set -e


NORMALIZER=trance-master/progs/trance_treebank
MAKEGRAM=trance-master/progs/trance_grammar
LEARNER=trance-master/progs/trance_learn
EVAL=trance-master/progs/trance_parse


# INPUT DATA
##CORPORA=data/corpora/discourse/rst_scheme/
CORPORA=check_error/data/


# OUTPUT NORMALIZED FILES
NORMALIZED=check_error/normalized/
# OUTPUT MODELS+GRAMMARS
MODELS=check_error/models/
# OUTPUT PREDICTIONS
PREDS=check_error/predictions/


# for embeddings
BRAZILIAN_CST=discoursecph/multilingual/data/brazilian-cst/constituent_format/

#cd $CORPORA
#CORPORA=/Users/chloe/data/experiments/eacl-16-multiling-rst/brazilian-cst/constituent_format
for f in ${CORPORA}/*.brackets
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name

  # BRAZILIAN CST
    
    cat $f | ${NORMALIZER} --output ${NORMALIZED}tree.norm.brackets --normalize --remove-none --remove-cycle --debug

    ${MAKEGRAM} --input ${NORMALIZED}tree.norm.brackets --output ${MODELS}/braz-cst_grammar --cutoff 2 --debug --signature English



    ${LEARNER} --input ${NORMALIZED}tree.norm.brackets --test ${NORMALIZED}tree.norm.brackets --output ${MODELS}braz-cst_model --grammar ${MODELS}/braz-cst_grammar --unary 0 --beam 32 --kbest 128 --randomize --learn all:opt=adadec,violation=max,margin-all=true,batch=4,iteration=1,eta=1e-2,gamma=0.9,epsilon=1,lambda=1e-5 --mix-select --averaging --debug=2 --word-embedding ${BRAZILIAN_CST}embeddings_edus_svd-50.txt --embedding 50


done


# 
# 
# 
# ${EVAL} --grammar ${MODELS}/braz-cst_grammar --model ${MODELS}braz-cst_model --unary 0 --signature English --word-embedding ${BRAZILIAN_CST}embeddings_edus_svd-50.txt --embedding 50 --input ${BRAZILIAN_CST}test.en.edus --output ${PREDS}braz-cst_predictions



