#!/bin/bash



# Exit immediately if a command exits with a non-zero status.
set -e


CORPORA=data/corpora/discourse/rst_scheme/

BASQUE_RSTDT=${CORPORA}basque/basque_rstdt/
BRAZILIAN_CST=${CORPORA}brazilian/CSTNews5.0/
ENGLISH_RSTDT=${CORPORA}english/rst_discourse_treebank/data/RSTtrees-WSJ-main-1.0/
#ENGLISH_RSTDT_TEST=${CORPORA}english/rst_discourse_treebank/data/RSTtrees-WSJ-main-1.0/TEST/ # do both in python code

GERMAN_PCC=${CORPORA}german/potsdam-commentary-corpus-2.0.0/
ML_RSTDT=${CORPORA}multilingualRST/multiRST/
SPANISH_RSTDT_TRAIN=${CORPORA}spanish/Spanish-RSTDT/TRAIN/
SPANISH_RSTDT_TEST=${CORPORA}spanish/Spanish-RSTDT/TEST/
CHINESE_RSTDT=${CORPORA}chinese/Chinese_discourse_corpus-4747.txt


#ENGLISH_GUM=${CORPORA}english/GUM/
ENGLISH_GUM=expe_data/gum/



OUT=data/experiments/eacl-16-multiling-rst/
OUT_BASQUE_RSTDT=${OUT}basque-rstdt/original-basque-rstdt/
OUT_BRAZILIAN_CST=${OUT}brazilian-cst/original-brazilian-cst/
OUT_ENGLISH_RSTDT=${OUT}english-rstdt/
#OUT_ENGLISH_RSTDT_TEST=${OUT}english/eng-rstdt/test/
OUT_ENGLISH_GUM=${OUT}/english-gum-rstdt/
OUT_GERMAN_PCC=${OUT}german-pcc/original-german-pcc/
OUT_ML_RSTDT=${OUT}basq_span_engl-rstdt/
OUT_SPANISH_RSTDT_TRAIN=${OUT}spanish-rstdt/train/
OUT_SPANISH_RSTDT_TEST=${OUT}spanish-rstdt/test/
OUT_CHINESE_RSTDT=${OUT}chinese-rstdt/

# LABEL_MAPPING=data/corpora/discourse/rst_scheme/relation_mapping/
# ENGLISH_RSTDT_MAPPING=${LABEL_MAPPING}rstdt_18groups.txt


# echo BASQUE
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${BASQUE_RSTDT} --outpath ${OUT_BASQUE_RSTDT} --mapping basque_labels
# 
# echo BRAZILIAN CST
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${BRAZILIAN_CST} --outpath ${OUT_BRAZILIAN_CST} --mapping brazilianCst_labels
# 
# echo GERMAN PCC
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${GERMAN_PCC} --outpath ${OUT_GERMAN_PCC} --mapping germanPcc_labels
# 
# 
# echo MULTILING
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${ML_RSTDT} --outpath ${OUT_ML_RSTDT} --mapping multiLing_labels
# 
# #PB to define nuclearity of some nodes, TO CHECK
echo GUM
python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${ENGLISH_GUM} --outpath ${OUT_ENGLISH_GUM} --mapping gum_labels
# 
# echo SPANISH TRAIN
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${SPANISH_RSTDT_TRAIN} --outpath ${OUT_SPANISH_RSTDT_TRAIN} --mapping spanish_labels
# 
# echo SPANISH TEST
# python navi/discourse/code/rst_DTB/read_rs3.py --treebank ${SPANISH_RSTDT_TEST} --outpath ${OUT_SPANISH_RSTDT_TEST} --mapping spanish_labels
# 

# echo CHINESE
# python navi/discourse/code/rst_DTB/read_chinese.py --treebank ${CHINESE_RSTDT} --outpath ${OUT_CHINESE_RSTDT} --mapping chinese_labels

# python navi/discourse/code/rst_DTB/read_dis.py --treebank ${ENGLISH_RSTDT} --outpath ${OUT_ENGLISH_RSTDT} --mapping ${ENGLISH_RSTDT_MAPPING}


