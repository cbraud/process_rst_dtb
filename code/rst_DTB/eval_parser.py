#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Evaluation of the results of a discourse parser:
    - Input: pred trees and gold trees (both in the brackets/mrg format
    - Output: span, nuclearity and relation scores
'''



from __future__ import print_function
import argparse, os, sys, shutil, codecs, numpy
from nltk import Tree


def main( ):
    parser = argparse.ArgumentParser(
            description='Predictions (and gold).')
    parser.add_argument('--preds',
            dest='preds',
            action='store',
            help='Prediction file.')
    parser.add_argument('--golds',
            dest='golds',
            action='store',
            help='Gold file.')
    parser.add_argument('--draw',
            dest='draw',
            action='store_true',
            help='If True, draw the gold and pred trees.')
    args = parser.parse_args()

    evaluation( vars(args)['preds'], vars(args)['golds'], draw=vars(args)['draw'] )


def evaluation( preds, golds, draw=False ):
    try:
        predTrees = readPred( preds )
        goldTrees = readGold( golds )
        if len( goldTrees ) != len( predTrees ) :
            sys.exit( "Non equal number of pred and gold trees." )
        computeScore( goldTrees, predTrees, draw=draw )
    except IOError:
        print( "OoOFile not found:", preds )



def computeScore( goldTrees, predTrees, draw=False ):
    # -- Compute scores per document + global scores
#     print( "\nCompute scores..", file=sys.stderr )
    met = Metrics(levels=['span','nuclearity','relation'])
    for i in range( len( goldTrees ) ):
#         print( "Doc", i, file=sys.stderr )
        goldTree = goldTrees[i]
        predTree = predTrees[i]

        goldleaves = goldTree.leaves()
        predleaves = predTree.leaves()

        if draw:
            Tree.draw( goldTree )
            Tree.draw( predTree )

#         print( '-'*30, goldTree )
        bracketsGold = bracketForm( goldTree, goldleaves )
#         print( "ok brackets gold", file=sys.stderr )
        bracketsPred = bracketForm( predTree, predleaves )
#         print( "brackets pred", bracketsPred )
#         print( "ok brackets pred", file=sys.stderr )
        met.eval(bracketsGold, bracketsPred)
#         print( "ok eval", file=sys.stderr )
    met.report()

#     # State of the art scores for comparison
#     print( "-"*40, file=sys.stderr )
#     print( "HILDA:\t\t83.00\t68.40\t54.80", file=sys.stderr )
#     print( "TSP 1-1:\t82.47\t68.43\t55.73", file=sys.stderr )
#     print( "TSP SW:\t\t82.74\t68.40\t55.71", file=sys.stderr )
#     print( "gCRF:\t\t85.7\t71.0\t58.2", file=sys.stderr )
#     print( "DPLP concat:\t82.08\t71.13\t61.63", file=sys.stderr )
#     print( "DPLP gen:\t81.60\t70.95\t61.75", file=sys.stderr )


def readPred( preds ):
    predTrees = []
    with codecs.open( preds, encoding='utf8' ) as f:
        lines = f.readlines()
        for i,l in enumerate( lines ):
            l = l.strip()
            tree = l
#             id, tree, _, sc = l.split('|||')
            tree = Tree.fromstring( tree.strip() )
            children = [c for c in tree]
            #print( tree.label(), [c.label() for c in children] )
            if len( children ) == 1:
#                 Tree.draw(tree)
                tree = children[0]
            elif len( children ) == 2 and tree.label() != 'ROOT':
                tree = tree
#                 print( "OoO Tree without ROOT label", file=sys.stderr )
# #                 print( "Root has only one child" )
# #                 Tree.draw(tree)
#             elif len( children ) == 2 and children[0].label() == 'EDU' and children[0].leaves()[0] == "-BEGIN-":
# #                 Tree.draw(tree)
#                 tree = children[1]
# #                 print( "Root has 2 children, the 1st is the dummy node" )
# #                 Tree.draw(tree)
            else:#keep the tree?
                Tree.draw(tree)
                sys.exit( "Tree with neither 1 child od 2 children the 1st being the dummy EDU" )


            #Tree.draw(tree)
            #print( tree )
            #predTrees.append( [c for c in Tree.fromstring( tree.strip() )][0] )#should be a unary tree take the firt child
            predTrees.append( tree )
    return predTrees

def readGold( golds ):
    goldTrees = []
    with codecs.open( golds, encoding='utf8' ) as f:
        lines = f.readlines()
        for i,l in enumerate( lines ):
            l = l.strip()
            tree = Tree.fromstring( l )
#             print( '\n', '='*30 )
#             print( '\n', [c for c in tree], '\n' )
#             tree = [c for c in tree]
            #print( [c for c in tree] )
            #goldTrees.append( [c for c in ][0] )
            goldTrees.append( tree )


    return goldTrees








# ----------------------------------------------------------------------------------
# EVALUATION (Yangfeng evaluation code + need to transform the trees into a bracketed format)
# ----------------------------------------------------------------------------------
def bracketForm( tree, leaves ):
    '''
    Transform to the bracketed form used by Yangfeng code
    - relation between X and Y-Z
        - if NS: X-X span ; Y-Z rel
        - if NN: X-X rel  ; Y-Z rel
    return list of (span, nuclearity, relation)
    '''
    brackets = []
    nodeList = [tree]
    # depth-first traversal of tree
    while nodeList != []:
        node = nodeList.pop()
        if isinstance(node,Tree):
            label = node.label()
            nuc = label[0:2]
            relation = label[2:]
            #print( nuc )
            #nuc = label.split('-')[0]
#             relation = '-'.join( label.split('-')[1:] )
            children = [c for c in node]
#             print( label, len(children) )
            if len( children ) == 2:
#                 print( "arg1", children[0].label(), "arg2", children[1].label() )
                arg1, arg2 = getArgumentSpan( children[0], children[1], leaves, tree )
#                 print( "arg1", arg1, "arg2", arg2 )

                if nuc.lower() == "ns":
                    brackets.append( (arg1, "nucleus", "span") )
                    brackets.append( (arg2, "satellite", relation) )
                elif nuc.lower() == "sn":
                    brackets.append( (arg1, "satellite", relation) )
                    brackets.append( (arg2, "nucleus", "span") )
                elif nuc.lower() == "nn":
                    brackets.append( (arg1, "nucleus", relation) )
                    brackets.append( (arg2, "nucleus", relation) )
                else:
                    sys.exit( "nuclearity value unknown: "+nuc )
                nodeList.extend( children )
#             else:
#                 print( tree.label(), [c.label() for c in tree] )
# #                 Tree.draw( tree )
            if not len( children ) in [0,1,2]:
                sys.exit("Evaluation: non binary tree" )
        else:
            sys.exit("node is not instance tree??")
    return brackets



def getArgumentSpan( left, right, leaves, tree ):
    arg1span = getArgSpan( left, leaves, tree )
    arg2span = getArgSpan( right, leaves, tree )
    return arg1span, arg2span

def getArgSpan( node, leaves, tree ):
    if node.label().lower() == 'edu':
        if node.leaves()[0] == "-BEGIN-":
            return (-1,-1)
        index = getSpanEdu( node.leaves()[0], leaves )
        return( index, index )
    else:
        try:
            left = [c for c in node][0]
            right = [c for c in node][1]
        except:
            print( node.label(), [c.label() for c in node] )
            Tree.draw( tree )
        arg1, arg2 = None, None
        if left.label().lower() == 'edu':
            arg1 = getSpanEdu( left.leaves()[0], leaves )
        else:
            arg1 = getSpanCdu( left.leaves()[0], leaves )
        if right.label().lower() == 'edu':
            arg2 = getSpanEdu( right.leaves()[0], leaves )
        else:
            arg2 = getSpanCdu( right.leaves()[-1], leaves )
        return( arg1, arg2 )

def getSpanEdu( nodeLeaves, leaves ):
    for i, l in enumerate( leaves ):
        if nodeLeaves == l:
            return i

def getSpanCdu( nodeLeaves, leaves ):
    for i, l in enumerate( leaves ):
        if nodeLeaves == l:
            return i



# def oldgetArgumentSpan( left, right ):
#     ''' Span format for the arguments '''
#     arg1_leaves = getLeaves( left )
#     arg2_leaves = getLeaves( right )
#     return _getArgumentSpan( arg1_leaves ), _getArgumentSpan( arg2_leaves )
#
# def _getArgumentSpan( leaves ):
#     if len( leaves ) == 1:
#         return (leaves[0], leaves[0])
#     else:
#         return (leaves[0], leaves[-1])
#
# def getLeaves( tree ):
#     ''' Return the leaves (ie EDUs) covered by a relation '''
#     leaves = set()
#     children = [c for c in tree]
#     if len( children ) == 0:
#         return [int(tree.label())]
#     while len(children) != 0:
#         child = children.pop(0)
#
#         if len([c for c in child]) == 0:
#             leaves.add( child.label() )
#         else:
#             children.extend( [c for c in child] )
#     return sorted( [int(l) for l in sorted( list(leaves), lambda x,y:cmp(x,y) )] )



class Performance(object):
    def __init__(self, percision, recall):
        self.percision = percision
        self.recall = recall

class Metrics(object):
    def __init__(self, levels=['span','nuclearity','relation']):
        """ Initialization

        :type levels: list of string
        :param levels: evaluation levels, the possible values are only
                       'span','nuclearity','relation'
        """
        self.levels = levels
        self.span_perf = Performance([], [])
        self.nuc_perf = Performance([], [])
        self.rela_perf = Performance([], [])

    # CHLOE modified take bracket directly instead of trees
    def eval(self, goldbrackets, predbrackets):
        """ Evaluation performance on one pair of RST trees

        :type goldtree: RSTTree class
        :param goldtree: gold RST tree

        :type predtree: RSTTree class
        :param predtree: RST tree from the parsing algorithm
        """
        #goldbrackets = goldtree.bracketing()
        #predbrackets = predtree.bracketing()
        for level in self.levels:
            if level == 'span':
                self._eval(goldbrackets, predbrackets, idx=1)
            elif level == 'nuclearity':
                self._eval(goldbrackets, predbrackets, idx=2)
            elif level == 'relation':
                self._eval(goldbrackets, predbrackets, idx=3)
            else:
                raise ValueError("Unrecognized evaluation level: {}".format(level))

    def _eval(self, goldbrackets, predbrackets, idx):
        """ Evaluation on each discourse span
        """
        goldspan = [item[:idx] for item in goldbrackets]
        predspan = [item[:idx] for item in predbrackets]

        allspan = [span for span in goldspan if span in predspan]
        p, r = 0.0, 0.0
        for span in allspan:
            if span in goldspan:
                p += 1.0
            if span in predspan:
                r += 1.0
        p /= len(goldspan)

        r /= len(predspan)
        if idx == 1:
            self.span_perf.percision.append(p)
            self.span_perf.recall.append(r)
        elif idx == 2:
            self.nuc_perf.percision.append(p)
            self.nuc_perf.recall.append(r)
        elif idx == 3:
            #print( "prec", p, "rec", r )
            self.rela_perf.percision.append(p)
            self.rela_perf.recall.append(r)

    def report(self):
        """ Compute the F1 score for different evaluation levels
            and print it out
        """
        global_scores = {}
        for level in self.levels:
            if 'span' == level:
                p = numpy.array(self.span_perf.percision).mean()
                r = numpy.array(self.span_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
#                 print( 'F1 score on span level is {0:.4f}'.format(f1), file=sys.stderr )
                global_scores['span'] = f1
            elif 'nuclearity' == level:
                p = numpy.array(self.nuc_perf.percision).mean()
                r = numpy.array(self.nuc_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
#                 print( 'F1 score on nuclearity level is {0:.4f}'.format(f1), file=sys.stderr )
                global_scores['nuc'] = f1
            elif 'relation' == level:
                p = numpy.array(self.rela_perf.percision).mean()
                r = numpy.array(self.rela_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
#                 print( 'F1 score on relation level is {0:.4f}'.format(f1), file=sys.stderr )
                global_scores['rel'] = f1
                if f1 > 0.6:
                    print( "-------> Better than SOA ?!" )
            else:
                raise ValueError("Unrecognized evaluation level")
#         print( "\nOur system:\t"+"\t".join( [str( round( global_scores[l]*100, 2 ) ) for l in ['span', 'nuc', 'rel']] ), file=sys.stderr )
        print( "\t".join( [str( round( global_scores[l]*100, 2 ) ) for l in ['span', 'nuc', 'rel']] ) )











if __name__ == '__main__':
    main()



