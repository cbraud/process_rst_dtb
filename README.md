# Preprocessing data for use with the discourse parser

## Test data

In order to simply use an existing model other new data, we need to:
- 
- Generate  a file with the features arranged in column, called *raw* file. It'll be used to make predictions based on the model

        # for test generate only raw
        c=test
        folder=data/
        # feature / column names
        echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
        python3 process_pdtb.py raw ${data}constituent_format/${c}.brackets ${parse}/${c}.parse >> ${folder}/${c}.raw



*scripts/preprocess_data/* contains the code used to produce the files used by the parser. Note that first, the data have to be read with the code in ../preprocess_rst/, in order to produce the .dmrg and .edus files.

- *preprocess_data.sh*: general script, for now only deal with the final conversion to .tbk and .raw files used by the parser TODO finish it!
     bash path_to/preprocess_data.sh path_to_inpath/data-sample/en/ path_to_outpath/data-sample/en/
- *convert2dmrg-edus.py*: add EDU text to the dmrg files, and output the train/dev/test files (i.e. constituent_format/)
- *convert2dconll.py*: find the head of each relation, and output a dconll file for each train/dev/test set (i.e. dependency_format)
- *addEdu2conll.py*: add the index of each EDU to the files containing the dependency parse of each document (.conll files), thus match the tokens in the parse with those in the EDUs (i.e. parse-ud_edu/)
- *addTranslation.py*: add a column with the translation of the token, or NONE. TODO add to data_sample
- *process_pdtb.py*: build the feature files used by the discourse parser

TODO: add code to parse with UD? (just done using models available on the website, parsing the raw text of each document)

